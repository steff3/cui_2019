import { BasePersistenceHandler, TableType } from "./BasePersistenceHandler"
import { Player, TeamType } from '../Models/player'
import { Game, GameState, GameResult } from '../Models/game'
import { User } from '../Models/user'
import { PlayerPersistenceHandling } from "./PlayerPersistenceHandling"
import { GamePersistenceHandling } from "./GamePersistenceHandling"


export class GamePersistenceHandler extends BasePersistenceHandler implements GamePersistenceHandling {


    playerPersistenceHandler: PlayerPersistenceHandling

    constructor(playerHandler: PlayerPersistenceHandling) {
        super()
        this.playerPersistenceHandler = playerHandler
    }

    createNewGame(player: Player): Promise<number> {
        const timestamp = new Date()
        const data: any = []
        data.push(player.id)
        data.push(timestamp)
        data.push(timestamp)
        
        let newGameId: number = -1

        const client = this.connectClient();
        return new Promise<number>( res => {
            return this.createGameTable(client)
                .then( () => {
                    return this.insertGame(client, data)
                })
                .then(res => {
                    newGameId = res.rows[0].id
                })
                .catch(e => {
                    console.error(e.stack)
                })
                .finally(() => {
                    this.disconnectClient(client)
                    res(newGameId)
                })
        })
    } 

    addPlayer(id: number, player: Player, allowsSingle: boolean = false): Promise<boolean> {
        let game: Game
        let successfully: boolean = true
        const client = this.connectClient();
        return new Promise<boolean>( res => {
            return this.createGameTable(client)
                .then( () => {
                    return this.selectGame(client, id)
                })
                .then(res => {
                    if (res.rowCount > 0) {
                        return this.gameFromData(res.rows[0])
                    }
                })
                .then(res => {
                    if (res.players.length < 4) {
                        if (res.canAddPlayer(player) && (allowsSingle || 
                            (res.players.filter(element => element.userId == player.userId)).length == 0)) {
                            return this.updateGame(client, id, `player_${ res.players.length + 1 }_id`, [player.id])
                        } else {
                            successfully = false
                        }
                    }
                })
                .catch(e => {
                    console.error(e.stack)
                    successfully = false
                })
                .finally(() => {
                    this.disconnectClient(client)
                    res(successfully)
                })
        })
    } 

    setResult(id: number, goals: GameResult): Promise<boolean> {
        let successfully: boolean = true
        if (!goals.isComplete()) {
            return new Promise<boolean>( res => {
                res(false)
            })
        }

        const resultString = `${goals.goalsForTeam(TeamType.Blue)}:${goals.goalsForTeam(TeamType.Red)}`

        const client = this.connectClient();
        return new Promise<boolean>(res => {
            return this.createGameTable(client)
                .then( () => {
                    return this.updateGame(client, id, `result`, [resultString])
                })
                .catch(e => {
                    console.error(e.stack)
                    successfully = false
                })
                .finally(() => {
                    this.disconnectClient(client)
                    res(successfully)
                })
        })
    }

    retrieveGames(state: GameState = GameState.All): Promise<Game[]> {
        let games: Game[] = []  
        const client = this.connectClient();
        return new Promise<Game[]>(res => {
            return this.createGameTable(client)
                .then( () => {
                    switch (state) {
                        case GameState.All:
                            return this.selectGames(client)
                            break
                        case GameState.Pending:
                            return this.selectPendingGames(client)
                            break
                        case GameState.Filled:
                            return this.selectFilledGames(client)
                            break
                        case GameState.Learnable:
                            return this.selectLearnableGames(client)
                            break
                    }
                    
                })
                .then(res => {
                    if (res.rowCount > 0) {
                        let retrievers = new Array()
                        for (let game of res.rows) {
                            retrievers.push(this.gameFromData(game))
                        }

                        return Promise.all(retrievers)
                    }
                })
                .then(res => {
                    if (res) {
                        games = res
                    }
                })
                .catch(e => {
                    console.error(e.stack)
                })
                .finally(() => {
                    this.disconnectClient(client)
                    res(games)
                })
        })
    }

    retrieveGame(id: number): Promise<Game> {
        let game: Game 
        const client = this.connectClient();
        return new Promise<Game>(res => {
            return this.createGameTable(client)
                .then( () => {
                    return this.selectGame(client, id)
                })
                .then(res => {
                    if (res.rowCount > 0) {
                        return this.gameFromData(res.rows[0])
                    }
                })
                .then(res => {
                    game = res
                })
                .finally(() => {
                    this.disconnectClient(client)
                    res(game)
                })
        })
    }

    retrieveGamesWithUser(user: User, state: GameState = GameState.All): Promise<Game[]> {
        let games: Game[] = []
        const client = this.connectClient();
        return new Promise<Game[]>(res => {
            return this.createGameTable(client)
                .then( () => {
                    switch (state) {
                        case GameState.All:
                            return this.selectGamesWithUser(client, user.id)
                            break
                        case GameState.Pending:
                            return this.selectPendingGamesWithUser(client, user.id)
                            break
                        case GameState.Filled:
                            return this.selectFilledGamesWithUser(client, user.id)
                            break
                        case GameState.Learnable:
                            return this.selectLearnableGamesWithUser(client, user.id)
                            break
                    }
                    
                })
                .then(res => {
                    console.log(`found ${ res.rowCount } for ${ user.playerName }`)
                    if (res.rowCount > 0) {
                        let retrievers = new Array()
                        for (let game of res.rows) {
                            retrievers.push(this.gameFromData(game))
                        }

                        return Promise.all(retrievers)
                    }
                })
                .then(res => {
                    if (res) {
                        games = res
                    }
                })
                .catch(e => {
                    console.error(e.stack)
                })
                .finally(() => {
                    this.disconnectClient(client)
                    res(games)
                })
        })
    }

    retrieveGamesForUser(user: User): Promise<Game[]> {
        let games: Game[]  
        const client = this.connectClient();
        return new Promise<Game[]>(res => {
            return this.createGameTable(client)
                .then( () => {
                    return this.selectPendingGamesWithoutUser(client, user.id)
                })
                .then(res => {
                    console.log(`found ${ res.rowCount } for ${ user.playerName }`)
                    if (res.rowCount > 0) {
                        let retrievers = new Array()
                        for (let game of res.rows) {
                            retrievers.push(this.gameFromData(game))
                        }

                        return Promise.all(retrievers)
                    }
                })
                .then(res => {
                    if (res) {
                        games = res
                    }
                })
                .catch(e => {
                    console.error(e.stack)
                })
                .finally(() => {
                    this.disconnectClient(client)
                    res(games)
                })
        })
    }

    deleteExistingGame(id: number): Promise<boolean> {
        let successfully: boolean = true

        const client = this.connectClient()
        return new Promise<boolean>(res => {
            return this.createGameTable(client)
                .then(() => {
                    return this.deleteGame(client, id)
                })
                .catch( e => {
                    console.error(e.stack)
                    successfully = false
                }).finally(() => {
                    this.disconnectClient(client)
                    res(successfully)
                })
        })
    }


    ///
    ///     DATABASE OPERATIONS
    ///

    private createGameTable(client: any): Promise<any> {
        //  
        const queryText = `CREATE TABLE IF NOT EXISTS ${ TableType.Game} (
            id SERIAL PRIMARY KEY,
            player_1_id INT REFERENCES ${ TableType.Player } (id) ON DELETE RESTRICT,
            player_2_id INT REFERENCES ${ TableType.Player } (id) ON DELETE RESTRICT,
            player_3_id INT REFERENCES ${ TableType.Player } (id) ON DELETE RESTRICT,
            player_4_id INT REFERENCES ${ TableType.Player } (id) ON DELETE RESTRICT,
            created_at TIMESTAMPTZ DEFAULT NOW(),
            last_changed TIMESTAMPTZ DEFAULT NOW(),
            result VARCHAR (4));`

        return client.query(queryText)
    }

    private insertGame(client: any, data: any): Promise<any> {
        const query = {
            text: `INSERT INTO ${ TableType.Game } (
                player_1_id,
                created_at,
                last_changed)
                VALUES($1, $2, $3)
                RETURNING id`,
            values: data
        }

        return client.query(query)
    }

    private updateGame(client: any, id: number, field: string, data: any): Promise<any> {
        const query = {
            text: `UPDATE ${ TableType.Game }
                SET ${ field } = $1,
                    last_changed = NOW()
                WHERE id = ${ id }
                RETURNING id`,
            values: data
        }

        return client.query(query)
    }

    private selectGame(client: any, id: number): Promise<any> {
        return super.selectById(client, TableType.Game, id)
    }

    private selectPendingGames(client: any): Promise<any> {
        const query = `SELECT *
                        FROM ${ TableType.Game }
                        WHERE player_4_id IS NULL 
                        OR player_3_id IS NULL 
                        OR player_2_id IS NULL 
                        OR player_1_id IS NULL`

        return client.query(query)
    }

    private selectFilledGames(client: any): Promise<any> {
        const query = `SELECT *
                        FROM ${ TableType.Game }
                        WHERE player_4_id NOT NULL 
                        AND player_3_id NOT NULL 
                        AND player_2_id NOT NULL 
                        AND player_1_id NOT NULL`

        return client.query(query)
    }

    private selectLearnableGames(client: any): Promise<any> {
        const query = `SELECT *
                        FROM ${ TableType.Game }
                        WHERE player_4_id NOT NULL 
                        AND player_3_id NOT NULL 
                        AND player_2_id NOT NULL 
                        AND player_1_id NOT NULL
                        AND result NOT NULL`

        return client.query(query)
    }

    private selectGamesWithUser(client: any, userId: number): Promise<any> {
        const query = `SELECT *
                        FROM ${ TableType.Game }
                        LEFT JOIN ${ TableType.Player } 
                        ON ${ TableType.Player }.id = ${ TableType.Game }.player_1_id
                        OR ${ TableType.Player }.id = ${ TableType.Game }.player_2_id
                        OR ${ TableType.Player }.id = ${ TableType.Game }.player_3_id
                        OR ${ TableType.Player }.id = ${ TableType.Game }.player_4_id
                        Where ${ TableType.Player }.user_id = ${ userId }`

        return client.query(query)
    }

    private selectFilledGamesWithUser(client: any, userId: number): Promise<any> {
        const query = `SELECT *
                        FROM ${ TableType.Game }
                        LEFT JOIN ${ TableType.Player } 
                        ON ${ TableType.Player }.id = ${ TableType.Game }.player_1_id
                        OR ${ TableType.Player }.id = ${ TableType.Game }.player_2_id
                        OR ${ TableType.Player }.id = ${ TableType.Game }.player_3_id
                        OR ${ TableType.Player }.id = ${ TableType.Game }.player_4_id
                        Where ${ TableType.Player }.user_id = ${ userId } 
                        AND player_4_id NOT NULL 
                        AND player_3_id NOT NULL 
                        AND player_2_id NOT NULL 
                        AND player_1_id NOT NULL`

        return client.query(query)
    }

    private selectPendingGamesWithUser(client: any, userId: number): Promise<any> {
        const query = `SELECT *
                        FROM ${ TableType.Game }
                        LEFT JOIN ${ TableType.Player } 
                        ON ${ TableType.Player }.id = ${ TableType.Game }.player_1_id
                        OR ${ TableType.Player }.id = ${ TableType.Game }.player_2_id
                        OR ${ TableType.Player }.id = ${ TableType.Game }.player_3_id
                        OR ${ TableType.Player }.id = ${ TableType.Game }.player_4_id
                        Where ${ TableType.Player }.user_id = ${ userId } 
                        AND (player_4_id IS NULL 
                            OR player_3_id IS NULL 
                            OR player_2_id IS NULL 
                            OR player_1_id IS NULL)`

        return client.query(query)
    }

    private selectLearnableGamesWithUser(client: any, userId: number): Promise<any> {
        const query = `SELECT *
                        FROM ${ TableType.Game }
                        LEFT JOIN ${ TableType.Player } 
                        ON ${ TableType.Player }.id = ${ TableType.Game }.player_1_id
                        OR ${ TableType.Player }.id = ${ TableType.Game }.player_2_id
                        OR ${ TableType.Player }.id = ${ TableType.Game }.player_3_id
                        OR ${ TableType.Player }.id = ${ TableType.Game }.player_4_id
                        Where ${ TableType.Player }.user_id = ${ userId } 
                        AND player_4_id NOT NULL 
                        AND player_3_id NOT NULL 
                        AND player_2_id NOT NULL 
                        AND player_1_id NOT NULL
                        AND result NOT NULL`

        return client.query(query)
    }

    private selectPendingGamesWithoutUser(client: any, userId: number): Promise<any> {
        const query = `SELECT *
                        FROM ${ TableType.Game }
                        LEFT JOIN ${ TableType.Player } 
                        ON ${ TableType.Player }.id = ${ TableType.Game }.player_1_id
                        OR ${ TableType.Player }.id = ${ TableType.Game }.player_2_id
                        OR ${ TableType.Player }.id = ${ TableType.Game }.player_3_id
                        OR ${ TableType.Player }.id = ${ TableType.Game }.player_4_id
                        Where ${ TableType.Player }.user_id != ${ userId } 
                        AND (player_4_id IS NULL 
                            OR player_3_id IS NULL 
                            OR player_2_id IS NULL 
                            OR player_1_id IS NULL)
                        LIMIT 1`

        return client.query(query)
    }


    private selectGames(client: any): Promise<any> {
        return super.selectAllItem(client, TableType.Game)
    }

    private deleteGame(client: any, id: number): Promise<any> {
        return this.deleteItem(client, id, TableType.Game)
    }

    //
    //  CONVERT TO DATATYPE
    //

    gameFromData(data:any): Promise<Game> {
        let game = new Game()
        game.id = data.id
        game.createdAt = new Date(data.created_at)
        game.lastChanged = new Date(data.last_changed)

        if (data.result) {
            const result = new GameResult()
            const resultString:string = data.result
            const resultValues: string[] = resultString.split(':')
            result.setNumberOfGoals(TeamType.Blue, +resultValues[0])
            result.setNumberOfGoals(TeamType.Red, +resultValues[1])
            game.result = result
        }

        let playerIds: number[] = []
        playerIds.push(data.player_1_id)
        if (data.player_2_id) {
            playerIds.push(data.player_2_id)
        }
        if (data.player_3_id) {
            playerIds.push(data.player_3_id)
        }
        if (data.player_4_id) {
            playerIds.push(data.player_4_id)
        }

        let retrievers = new Array()
        for (let id of playerIds) {
            retrievers.push(this.playerPersistenceHandler.playerForId(id))
        }
        // playerIds.forEach(element => {
        //     retrievers.push(this.playerPersistenceHandler.playerForId(element))
        // })

        return new Promise<Game>(res => {
            return Promise.all(retrievers).
            then((includedPlayers: Player[]) => {
                game.players = includedPlayers
            })
            .finally(() => {
                res(game)
            })
        })
    }


}