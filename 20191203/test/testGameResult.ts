import { describe } from 'mocha'
import * as chai from 'chai'
import * as chaiAsPromised from 'chai-as-promised'
import * as uuidv4 from 'uuid/v4'
const { expect } = chai

chai.use(chaiAsPromised)


import { GameResult } from '../src/Models/game'
import { TeamType } from '../src/Models/player'

describe ('Test GameResult Class', function() {
    it ('Create GameResult', function() {
        const result = new GameResult()

        expect(result.isComplete()).to.be.false
        expect(result.goalsForTeam(TeamType.Blue)).to.be.equal(0)
        expect(result.goalsForTeam(TeamType.Red)).to.be.equal(0)
    })

    it ('Add Goal GameResult for BLUE', function() {
        const result = new GameResult()
        result.addGoalForTeam(TeamType.Blue)

        expect(result.goalsForTeam(TeamType.Blue)).to.be.equal(1)
        expect(result.goalsForTeam(TeamType.Red)).to.be.equal(0)
    })

    it ('Add Goal GameResult For RED', function() {
        const result = new GameResult()
        result.addGoalForTeam(TeamType.Blue)
        result.addGoalForTeam(TeamType.Blue)
        result.addGoalForTeam(TeamType.Red)
        result.addGoalForTeam(TeamType.Blue)
        result.addGoalForTeam(TeamType.Red)

        expect(result.goalsForTeam(TeamType.Blue)).to.be.equal(3)
        expect(result.goalsForTeam(TeamType.Red)).to.be.equal(2)
    })

    it ('Set Goals GameResult', function() {
        const result = new GameResult()
        result.setNumberOfGoals(TeamType.Red, 10)
        result.setNumberOfGoals(TeamType.Blue, 7)

        expect(result.goalsForTeam(TeamType.Blue)).to.be.equal(7)
        expect(result.goalsForTeam(TeamType.Red)).to.be.equal(10)
    })

    it ('Check Complete false: < 10', function() {
        const result = new GameResult()
        result.addGoalForTeam(TeamType.Blue)
        result.addGoalForTeam(TeamType.Blue)
        result.addGoalForTeam(TeamType.Red)
        result.addGoalForTeam(TeamType.Blue)
        result.addGoalForTeam(TeamType.Red)

        expect(result.isComplete()).to.be.false
    })

    it ('Check Complete false: Same Number Of Goals', function() {
        const result = new GameResult()
        result.setNumberOfGoals(TeamType.Red, 10)
        result.setNumberOfGoals(TeamType.Blue, 10)

        expect(result.isComplete()).to.be.false
    })

    it ('Check Complete true', function() {
        const result = new GameResult()
        result.setNumberOfGoals(TeamType.Red, 10)
        result.setNumberOfGoals(TeamType.Blue, 7)

        expect(result.isComplete()).to.be.true
    })
})