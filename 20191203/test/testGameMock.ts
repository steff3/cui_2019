import { describe } from 'mocha'
import * as chai from 'chai'
import * as chaiAsPromised from 'chai-as-promised'
import * as uuidv4 from 'uuid/v4'
const { expect } = chai

chai.use(chaiAsPromised)


import { PlayerPersistenceHandling } from '../src/Persistence/PlayerPersistenceHandling'
import { UserPersistenceHandling } from '../src/Persistence/UserPersistenceHandling'
import { GamePersistenceHandling } from '../src/Persistence/GamePersistenceHandling'
import { GameMockHandler } from '../src/Persistence/Mock/GameMockHandler'
import { PlayerMockHandler } from '../src/Persistence/Mock/PlayerMockHandler'
import { UserMockHandler } from '../src/Persistence/Mock/UserMockHandler'
import { PositionType, TeamType } from '../src/Models/player'
import { User } from '../src/Models/user'
import { Player } from '../src/Models/player'
import { Game, GameState, GameResult } from '../src/Models/game'

let playerHandler: PlayerPersistenceHandling
let userHandler: UserPersistenceHandling
let handler: GameMockHandler
let users: User[] = []
let players: Player[] = []
let gameIds: number[] = []

describe ('Create New Game In Database', function() {
    before('Create Users and Players', async function() {
        const pHandler = new PlayerMockHandler()
        userHandler = new UserMockHandler()
        handler = new GameMockHandler(pHandler)
        playerHandler = pHandler

        await addPlayer('Mario', 'Maier', 'SuperMario', uuidv4(), PositionType.Attack)
        await addPlayer('Dieter', 'Bauer', 'Didi', uuidv4(), PositionType.Defence)
        await addPlayer('Sara', 'Neuhaus', 'Sasa', uuidv4(), PositionType.Attack)
        await addPlayer('Lena', 'Lukatsch', 'Leni', uuidv4(), PositionType.Defence)
        await addPlayer('Bubu', 'Loreny', 'Bubu', uuidv4(), PositionType.Attack)

        await addFurtherPlayer(users[0], PositionType.Attack)
        await addFurtherPlayer(users[1], PositionType.Defence)
        await addFurtherPlayer(users[2], PositionType.Defence)
        await addFurtherPlayer(users[3], PositionType.Attack)
        await addFurtherPlayer(users[4], PositionType.Attack)

        // check to refuse player twice in game
        await addFurtherPlayer(users[0], PositionType.Defence)
    })

    after('Delete Players and Users', async function() {
        // const success = await userHandler.deleteExistingUser(user.id)
        // console.log(`user successfully delete? ${success}`)
        for (let user of users) {
            await cleanUser(user)
        }
    })

    it ('Create New Game', async function() {
        gameIds = []
        const gameId = await handler.createNewGame(players[0])
        gameIds.push(gameId)

        expect(gameId).to.be.a('number').that.is.greaterThan(0)
    })

    it ('Retrieve Game By Id', async function() {
        gameIds = []
        console.dir(players[0], {depth:null})
        const gameId = await handler.createNewGame(players[0])
        console.log(`created game with id ${ gameId }`)
        gameIds.push(gameId)

        const game = await handler.retrieveGame(gameId)
        console.dir(game, {depth:null})
    })

    it ('Add Second Player to Game', async function() {
        gameIds = []
        const gameId = await handler.createNewGame(players[0])
        gameIds.push(gameId)
        const success = await handler.addPlayer(gameId, players[1])

        const game = await handler.retrieveGame(gameId)

        let members = []
        game.players.forEach(element => {
            members.push(element.position)
            members.push(element.name)
            members.push(element.userId)
        })

        expect(success).to.be.true
        expect(game.players).to.be.an('array')
        expect(game.players.length).to.be.equal(2)
        expect(members).to.include(players[0].position)
        expect(members).to.include(players[1].position)
        expect(members).to.include(players[0].name)
        expect(members).to.include(players[1].name)
        expect(members).to.include(players[0].userId)
        expect(members).to.include(players[1].userId)
        console.dir(game, {depth:null})
    })

    it ('Have Four Players in Game', async function() {
        gameIds = []
        const gameId = await handler.createNewGame(players[0])
        gameIds.push(gameId)

        let success = await handler.addPlayer(gameId, players[1])
        expect(success).to.be.true
        success = await handler.addPlayer(gameId, players[2])
        expect(success).to.be.true
        success = await handler.addPlayer(gameId, players[3])
        expect(success).to.be.true

        const game = await handler.retrieveGame(gameId)

        let members = []
        game.players.forEach(element => {
            members.push(element.position)
            members.push(element.name)
            members.push(element.userId)
        })

        expect(game.players).to.be.an('array')
        expect(game.players.length).to.be.equal(4)
        expect(members).to.include(players[0].position)
        expect(members).to.include(players[1].position)
        expect(members).to.include(players[2].position)
        expect(members).to.include(players[3].position)
        expect(members).to.include(players[0].name)
        expect(members).to.include(players[1].name)
        expect(members).to.include(players[2].name)
        expect(members).to.include(players[3].name)
        expect(members).to.include(players[0].userId)
        expect(members).to.include(players[1].userId)
        expect(members).to.include(players[2].userId)
        expect(members).to.include(players[3].userId)
        console.dir(game, {depth:null})
    })

    it ('Verify Rejection of 3rd Attack in Game', async function() {
        gameIds = []
        const gameId = await handler.createNewGame(players[0])
        gameIds.push(gameId)

        let success = await handler.addPlayer(gameId, players[1])
        expect(success).to.be.true
        success = await handler.addPlayer(gameId, players[2])
        expect(success).to.be.true
        success = await handler.addPlayer(gameId, players[4])
        expect(success).to.be.false

        const game = await handler.retrieveGame(gameId)

        let members = []
        game.players.forEach(element => {
            members.push(element.position)
            members.push(element.name)
            members.push(element.userId)
        })

        expect(game.players).to.be.an('array')
        expect(game.players.length).to.be.equal(3)
        expect(members).to.include(players[0].position)
        expect(members).to.include(players[1].position)
        expect(members).to.include(players[2].position)
        expect(members).to.include(players[0].name)
        expect(members).to.include(players[1].name)
        expect(members).to.include(players[2].name)
        expect(members).to.include(players[0].userId)
        expect(members).to.include(players[1].userId)
        expect(members).to.include(players[2].userId)
        console.dir(game, {depth:null})
    })

    it ('Reject Second Player from User in Game', async function() {
        gameIds = []
        const gameId = await handler.createNewGame(players[0])
        gameIds.push(gameId)

        let success = await handler.addPlayer(gameId, players[1])
        expect(success).to.be.true
        success = await handler.addPlayer(gameId, players[2])
        expect(success).to.be.true

        
        let game = await handler.retrieveGame(gameId)
        expect(game.players).to.be.an('array')
        expect(game.players.length).to.be.equal(3)

        // make sure we can add the position of the player to the game
        success = game.canAddPlayer(players[10])
        expect(success).to.be.true
        // make sure that we try to add a player from a user who 
        // is already registered for the game
        expect(players[10].userId).to.be.equal(players[0].userId)
        success = await handler.addPlayer(gameId, players[10])
        expect(success).to.be.false

        game = await handler.retrieveGame(gameId)

        let members = []
        game.players.forEach(element => {
            members.push(element.position)
            members.push(element.name)
            members.push(element.userId)
        })

        expect(game.players).to.be.an('array')
        expect(game.players.length).to.be.equal(3)
        expect(members).to.include(players[0].position)
        expect(members).to.include(players[1].position)
        expect(members).to.include(players[2].position)
        expect(members).to.include(players[0].name)
        expect(members).to.include(players[1].name)
        expect(members).to.include(players[2].name)
        expect(members).to.include(players[0].userId)
        expect(members).to.include(players[1].userId)
        expect(members).to.include(players[2].userId)
        console.dir(game, {depth:null})
    })

    it ('Retrieve All Games', async function() {
        gameIds = []
        let gameId = await handler.createNewGame(players[0])
        gameIds.push(gameId)
        let success = await handler.addPlayer(gameId, players[1])
        expect(success).to.be.true

        gameId = await handler.createNewGame(players[2])
        gameIds.push(gameId)
        success = await handler.addPlayer(gameId, players[3])
        expect(success).to.be.true

        const games = await handler.retrieveGames()
        expect(games).to.be.an('array')
        expect(games.length).to.be.equal(2)
        console.dir(games[0], {depth:null})
        console.dir(games[1], {depth:null})
    })

    it ('Retrieve Pending New Game', async function() {
        gameIds = []
        const gameId = await handler.createNewGame(players[0])
        gameIds.push(gameId)

        const games = await handler.retrieveGames(GameState.Pending)
        expect(games).to.be.an('array')
        expect(games.length).to.be.equal(1)
        console.dir(games[0], {depth:null})
    })

    it ('Retrieve Pending Game', async function() {
        gameIds = []
        const gameId = await handler.createNewGame(players[0])
        gameIds.push(gameId)

        const success = await handler.addPlayer(gameId, players[1])
        expect(success).to.be.true

        const games = await handler.retrieveGames(GameState.Pending)
        expect(games).to.be.an('array')
        expect(games.length).to.be.equal(1)
        console.dir(games[0], {depth:null})
    })

    it ('Retrieve Pending Games', async function() {
        gameIds = []
        let gameId = await handler.createNewGame(players[0])
        gameIds.push(gameId)
        let success = await handler.addPlayer(gameId, players[1])
        expect(success).to.be.true

        gameId = await handler.createNewGame(players[2])
        gameIds.push(gameId)
        success = await handler.addPlayer(gameId, players[3])
        expect(success).to.be.true

        const games = await handler.retrieveGames(GameState.Pending)
        expect(games).to.be.an('array')
        expect(games.length).to.be.equal(2)
        console.dir(games[0], {depth:null})
    })

    it ('Retrieve Pending Game, Not Filled Game', async function() {
        gameIds = []
        let gameId = await handler.createNewGame(players[0])
        gameIds.push(gameId)

        let success = await handler.addPlayer(gameId, players[1])
        expect(success).to.be.true
        success = await handler.addPlayer(gameId, players[2])
        expect(success).to.be.true
        success = await handler.addPlayer(gameId, players[3])
        expect(success).to.be.true

        gameId = await handler.createNewGame(players[4])
        gameIds.push(gameId)

        let games = await handler.retrieveGames()
        expect(games).to.be.an('array')
        expect(games.length).to.be.equal(2)

        games = await handler.retrieveGames(GameState.Pending)
        expect(games).to.be.an('array')
        expect(games.length).to.be.equal(1)
        console.dir(games[0], {depth:null})
    })

    it ('Retrieve Game WITH User(1)', async function() {
        gameIds = []
        const gameId = await handler.createNewGame(players[0])
        gameIds.push(gameId)

        const games = await handler.retrieveGamesWithUser(users[0])
        expect(games).to.be.an('array')
        expect(games.length).to.be.equal(1)
        console.dir(games[0], {depth:null})
    })

    it ('Retrieve Game WITH User(2)', async function() {
        gameIds = []
        const gameId = await handler.createNewGame(players[0])
        gameIds.push(gameId)

        const success = await handler.addPlayer(gameId, players[1])
        expect(success).to.be.true

        const games = await handler.retrieveGamesWithUser(users[1])
        expect(games).to.be.an('array')
        expect(games.length).to.be.equal(1)
        console.dir(games[0], {depth:null})
    })

    it ('Retrieve Games WITH User(1)', async function() {
        gameIds = []
        let gameId = await handler.createNewGame(players[0])
        gameIds.push(gameId)
        let success = await handler.addPlayer(gameId, players[1])
        expect(success).to.be.true

        // second player for user1
        const player2Id = await playerHandler.createNewPlayer(users[0], PositionType.Defence)
        const player2 = await playerHandler.playerForId(player2Id)

        gameId = await handler.createNewGame(player2)
        //gameIds.push(gameId)

        const games = await handler.retrieveGamesWithUser(users[0])
        expect(games).to.be.an('array')
        expect(games.length).to.be.equal(2)
        console.dir(games, {depth:null})

        success = await handler.deleteExistingGame(gameId)
        expect(success).to.be.true

        success = await playerHandler.deleteExistingPlayer(player2Id)
        expect(success).to.be.true
    })

    it ('Retrieve Games WITH User(2)', async function() {
        gameIds = []
        let gameId = await handler.createNewGame(players[0])
        gameIds.push(gameId)
        let success = await handler.addPlayer(gameId, players[1])
        expect(success).to.be.true

        // second player for user1
        const player2Id = await playerHandler.createNewPlayer(users[0], PositionType.Defence)
        const player2 = await playerHandler.playerForId(player2Id)

        gameId = await handler.createNewGame(player2)
        //gameIds.push(gameId)

        const games = await handler.retrieveGamesWithUser(users[1])
        expect(games).to.be.an('array')
        expect(games.length).to.be.equal(1)
        console.dir(games, {depth:null})

        success = await handler.deleteExistingGame(gameId)
        expect(success).to.be.true

        success = await playerHandler.deleteExistingPlayer(player2Id)
        expect(success).to.be.true
    })

    it ('Retrieve NO Games WITH User(3)', async function() {
        gameIds = []
        let gameId = await handler.createNewGame(players[0])
        gameIds.push(gameId)
        let success = await handler.addPlayer(gameId, players[1])
        expect(success).to.be.true

        // second player for user1
        const player2Id = await playerHandler.createNewPlayer(users[0], PositionType.Defence)
        const player2 = await playerHandler.playerForId(player2Id)

        gameId = await handler.createNewGame(player2)
        //gameIds.push(gameId)

        const games = await handler.retrieveGamesWithUser(users[2])
        //expect(games).to.be.undefined
        expect(games).to.be.an('array')
        expect(games).to.be.empty

        success = await handler.deleteExistingGame(gameId)
        expect(success).to.be.true

        success = await playerHandler.deleteExistingPlayer(player2Id)
        expect(success).to.be.true
    })

    it ('Set Result for Game', async function() {
        //
        // PREPARE
        //
        gameIds = []
        const gameId = await handler.createNewGame(players[0])
        gameIds.push(gameId)

        let success = await handler.addPlayer(gameId, players[1])
        expect(success).to.be.true
        success = await handler.addPlayer(gameId, players[2])
        expect(success).to.be.true
        success = await handler.addPlayer(gameId, players[3])
        expect(success).to.be.true

        //
        // SET MAIN CONDITIONS
        //
        const result = new GameResult()
        result.setNumberOfGoals(TeamType.Blue, 8)
        result.setNumberOfGoals(TeamType.Red, 10)
        expect(result.isComplete()).to.be.true
        success = await handler.setResult(gameId, result)

        //
        // VERIFY
        //
        expect(success).to.be.true

        const game = await handler.retrieveGame(gameId)
        expect(game.result.isComplete()).to.be.true
        expect(game.result.goalsForTeam(TeamType.Blue)).to.be.equal(8)
        expect(game.result.goalsForTeam(TeamType.Red)).to.be.equal(10)
        console.dir(game, {depth:null})
    })

    it ('Retrieve Games FOR User(0)', async function() {
        //
        // PREPARE
        //
        gameIds = []
        let gameId = await handler.createNewGame(players[0])
        gameIds.push(gameId)
        let success = await handler.addPlayer(gameId, players[1])
        expect(success).to.be.true
        success = await handler.addPlayer(gameId, players[2])
        expect(success).to.be.true
        success = await handler.addPlayer(gameId, players[3])
        expect(success).to.be.true
        const result = new GameResult()
        result.setNumberOfGoals(TeamType.Blue, 8)
        result.setNumberOfGoals(TeamType.Red, 10)
        expect(result.isComplete()).to.be.true
        success = await handler.setResult(gameId, result)
        expect(success).to.be.true

        gameId = await handler.createNewGame(players[1])
        gameIds.push(gameId)
        success = await handler.addPlayer(gameId, players[2])
        expect(success).to.be.true
        success = await handler.addPlayer(gameId, players[3])
        expect(success).to.be.true

        let allGames = await handler.retrieveGames()
        expect(allGames).to.be.an('array')
        expect(allGames.length).to.be.equal(2)

        //
        // SET MAIN CONDITIONS
        //
        const playableGames = await handler.retrieveGamesForUser(users[0])
        
        //
        // VERIFY
        //
        expect(playableGames).to.be.an('array')
        expect(playableGames.length).to.be.equal(1)
        //expect(playableGames[0].result.isComplete()).to.be.false
        console.dir(playableGames, {depth:null})
        allGames = await handler.retrieveGames()
        expect(allGames).to.be.an('array')
        expect(allGames.length).to.be.equal(2)
    })

    afterEach('Delete Current Game', async function() {
        for (let id of gameIds) {
             await cleanGame(id)
        }
    })
    
} )

async function cleanGame(id: number) {
    const success = await handler.deleteExistingGame(id)
    console.log(`game with id ${id} successfully deleted? ${success}`)
}

async function addPlayer(firstName: string, lastName: string, playerName: string, actionId: string, position: PositionType) {
    const userId = await userHandler.createNewUser(firstName, lastName, playerName, actionId)
    const user = await userHandler.retrieveUser(userId)
    console.dir(user, {depth:null})
    users.push(user)

    const playerId = await playerHandler.createNewPlayer(user, position)
    const player = await playerHandler.playerForId(playerId)
    console.dir(player, {depth:null})
    players.push(player)
}

async function addFurtherPlayer(user: User, position: PositionType) {
    const playerId = await playerHandler.createNewPlayer(user, position)
    const player = await playerHandler.playerForId(playerId)
    console.dir(player, {depth:null})
    players.push(player)
}

async function cleanUser(user: User) {
    const players = await playerHandler.playersOfUser(user)

    let success = false
    for (let player of players) {
        success = await playerHandler.deleteExistingPlayer(player.id)
        console.log(`player ${player.name} successfully deleted? ${success}`)
    }
    
    success = await userHandler.deleteExistingUser(user.id)
    console.log(`user ${user.firstName} successfully delete? ${success}`)
}