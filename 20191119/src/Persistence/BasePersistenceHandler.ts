import { Client } from 'pg'

export enum TableType {
    User = 'user_data',
    Player = 'player_data',
    Game = 'game_data'
}

export class BasePersistenceHandler {

    protected connectClient(): any {
        const client = new Client({
            user: 'postgres',
            host: '0.0.0.0',
            database: 'agentdb',
            password: 'postgres',
            port: 5432
        })

        client.connect()

        return client
    }


    protected disconnectClient(client: any): Promise<any> {
        return client.end()
    }

    protected updateItem(client: any, id: number, field: string, data: any, table: string): Promise<any> {
        const query = {
            text: `UPDATE ${ table } (
                SET ${ field } = $1
                WHERE id = ${ id }
                RETURNING id`,
            values: data
        }

        return client.query(query)
    }

    protected deleteItem(client: any, id: number, table: string): Promise<any> {
        const query = `DELETE FROM ${ table }
                        WHERE id = ${ id }`

        return client.query(query)
    }

    
}