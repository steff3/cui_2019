import { BasePersistenceHandler } from './BasePersistenceHandler'
import { UserPersistenceHandling } from './UserPersistenceHandling'
import { User } from '../Models/user'
import { PositionType } from '../Models/player'

export class UserPersistenceHandler extends BasePersistenceHandler implements UserPersistenceHandling {
    private userTable: string = 'user_data'

    getUserTable(): string {
        return this.userTable
    }
    
    createNewUser(firstName: string, lastName: string, playerName: string, actionId: string): Promise<number> {
        const data: string[] = []
        data.push(firstName)
        data.push(lastName)
        data.push(playerName)
        data.push(actionId)

        let newUserId: number = -1

        const client = this.connectClient();
        return new Promise<number>( res => {
            return this.createUserTable(client)
                .then( () => {
                    return this.insertUser(client, data)
                })
                .then(res => {
                    newUserId = res.rows[0].id
                })
                .catch(e => {
                    console.error(e.stack)
                })
                .finally(() => {
                    this.disconnectClient(client)
                    res(newUserId)
                })
        })
    }    
    
    deleteExistingUser(id: number): Promise<boolean> {
        let successfully: boolean = true

        const client = this.connectClient()
        return new Promise<boolean>(res => {
            return this.createUserTable(client)
                .then(() => {
                    return this.deleteUser(client, id)
                })
                .catch( e => {
                    console.error(e.stack)
                    successfully = false
                }).finally(() => {
                    this.disconnectClient(client)
                    res(successfully)
                })
        })
    }

    retrieveUser(id: number): Promise<User> {
        let retrievedUser: User = null

        const client = this.connectClient()
        return new Promise<User>( res => {
            return this.createUserTable(client)
                .then(() => {
                    return this.selectUser(client, id)
                })
                .then(res => {
                    if (res.rowCount > 0) {
                        retrievedUser = this.userFromData(res.rows[0])
                    }
                })
                .catch(e => {
                    console.error(e.stack)
                })
                .finally(() => {
                    this.disconnectClient(client)
                    res(retrievedUser)
                })
        })
    }

    retrieveUserByActionId(id: string): Promise<User> {
        let retrievedUser: User = null

        const client = this.connectClient()
        return new Promise<User>( res => {
            return this.createUserTable(client)
                .then(() => {
                    return this.selectUserByActionId(client, id)
                })
                .then(res => {
                    if (res.rowCount > 0) {
                        retrievedUser = this.userFromData(res.rows[0])
                    }
                })
                .catch(e => {
                    console.error(e.stack)
                })
                .finally(() => {
                    this.disconnectClient(client)
                    res(retrievedUser)
                })
        })
    }

    retrieveAllUsers(): Promise<User[]> {
        let retrievedUsers: User[] = []

        const client = this.connectClient()
        return new Promise<User[]>( res => {
            return this.createUserTable(client)
                .then(() => {
                    return this.selectUsers(client)
                })
                .then(res => {
                    if (res.rowCount > 0) {
                        res.rows.forEach(element => {
                            retrievedUsers.push(this.userFromData(element))
                        });
                    }
                })
                .catch(e => {
                    console.error(e.stack)
                })
                .finally(() => {
                    this.disconnectClient(client)
                    res(retrievedUsers)
                })
        })
    }

    updatePlayerName(id: number, name: String): Promise<boolean> {
        const data: any[] = []
        data.push(name)

        let successfully: boolean = true

        const client = this.connectClient()
        return new Promise<boolean>( res => {
            return this.createUserTable(client)
                .then(() => {
                    return this.updateUser(client, id, 'player_name', data)
                })
                .catch(e => {
                    successfully = false
                    console.error(e.stack)
                })
                .finally(() => {
                    this.disconnectClient(client)
                    res(successfully)
                })
        })
    }

    updatePreferredPosition(id: number, position: PositionType): Promise<boolean> {
        const data: any[] = []
        if (position == PositionType.Attack) {
            data.push('ATTACK')
        } else {
            data.push('DEFENSE')
        }
        

        let successfully: boolean = true

        const client = this.connectClient()
        return new Promise<boolean>( res => {
            return this.createUserTable(client)
                .then(() => {
                    return this.updateUser(client, id, 'preferred_position', data)
                })
                .catch(e => {
                    successfully = false
                    console.error(e.stack)
                })
                .finally(() => {
                    this.disconnectClient(client)
                    res(successfully)
                })
        })
    }

    /// 
    ///     Write to Postgresql
    ///
    private createUserTable(client: any): Promise<any> {
        const queryText = `CREATE TABLE IF NOT EXISTS ${ this.userTable } (
            id SERIAL PRIMARY KEY,
            first_name VARCHAR (50) NOT NULL,
            last_name VARCHAR (50) NOT NULL,
            player_name VARCHAR (50) NOT NULL,
            action_id VARCHAR (40) NOT NULL,
            preferred_position VARCHAR (10));`

        return client.query(queryText)
    }

    private insertUser(client: any, data: string[]): Promise<any> {
        const query = {
            text: `INSERT INTO ${ this.userTable } (
                first_name,
                last_name,
                player_name,
                action_id)
                VALUES($1, $2, $3, $4)
                RETURNING id`,
            values: data
        }

        return client.query(query)
    }

    private updateUser(client: any, id: number, field: string, data: any): Promise<any> {
        return this.updateItem(client, id, field, data, this.userTable)
        // const query = {
        //     text: `UPDATE ${ this.userTable } (
        //         SET ${ field } = $1
        //         WHERE id = ${ id }
        //         RETURNING id`,
        //     values: data
        // }

        // return client.query(query)
    }

    private selectUser(client: any, id: number): Promise<any> {
        const query = `SELECT *
                        FROM ${ this.userTable}
                        WHERE id = ${ id }`

        return client.query(query)
    }

    private selectUserByActionId(client: any, id: string): Promise<any> {
        const query = `SELECT *
                        FROM ${ this.userTable}
                        WHERE action_id = ${ id }`

        return client.query(query)
    }

    private selectUsers(client: any): Promise<any> {
        const query = `SELECT *
                        FROM ${ this.userTable}`

        return client.query(query)
    }

    private deleteUser(client: any, id: number): Promise<any> {
        return this.deleteItem(client, id, this.userTable)
        // const query = `DELETE FROM ${ this.userTable}
        //                 WHERE id = ${ id }`

        // return client.query(query)
    }


    ///
    /// Data Converters
    ///

    private userFromData(data:any): User {
        const user = new User()
        user.id = data.id
        user.firstName = data.first_name
        user.lastName = data.last_name
        user.playerName = data.player_name
        user.actionId = data.action_id
        if (data.preferred_position && data.preferred_position.length > 0) {
            if (data.preferred_position === 'ATTACK') {
                user.preferredPosition = PositionType.Attack
            } else {
                user.preferredPosition = PositionType.Defence
            }
        }

        return user
    }
}