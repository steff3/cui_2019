import { describe } from 'mocha'
import * as chai from 'chai'
import * as chaiAsPromised from 'chai-as-promised'
import * as uuidv4 from 'uuid/v4'
const { expect } = chai
chai.use(chaiAsPromised)


import  { UserPersistenceHandler } from '../src/Persistence/UserPersistenceHandler'

let currentId: number = 0
let actionId: string = ''

describe('Generate User In Database', function () {
    context('User Persistence Context', function () {
        const handler = new UserPersistenceHandler()

        it ('Create New User', async function() {
            actionId = uuidv4()

            const userId = await handler.createNewUser('Mario', 'Maier', 'SuperMario', actionId)

            expect(userId).to.be.a('number').that.is.greaterThan(0)
            currentId = userId

        })
    })
} )

describe('Retrieve User From Database', function () {
    context('User Persistence Context', function () {
        const handler = new UserPersistenceHandler()

        it ('Retreive User', async function() {
            
            const user = await handler.retrieveUser(currentId)

            expect(user).to.have.property('firstName', 'Mario')
            expect(user).to.have.property('lastName', 'Maier')
            expect(user).to.have.property('playerName', 'SuperMario')
            expect(user).to.have.property('actionId', actionId)

        })
    })
} )

describe('Retrieve Users From Database', function () {
    context('User Persistence Context', function () {
        const handler = new UserPersistenceHandler()

        it ('Retreive ALL Users', async function() {
            
            const users = await handler.retrieveAllUsers()

            expect(users.length).to.be.equal(1)
            expect(users[0]).to.have.property('playerName', 'SuperMario')
        })
    })
} )

describe('Delete User From Database', function () {
    context('User Persistence Context', function () {
        const handler = new UserPersistenceHandler()

        it ('Delete Specific User', async function() {
            
            const success = await handler.deleteExistingUser(currentId)

            expect(success).to.be.true
        })
    })
} )

describe('Retrieve Users From Database', function () {
    context('User Persistence Context', function () {
        const handler = new UserPersistenceHandler()

        it ('Retreive ALL Users', async function() {
            
            const users = await handler.retrieveAllUsers()

            expect(users.length).to.be.equal(0)
        })
    })
} )