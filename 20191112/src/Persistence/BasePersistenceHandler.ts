import { Client } from 'pg'

export class BasePersistenceHandler {


    protected connectClient(): any {
        const client = new Client({
            user: 'postgres',
            host: '0.0.0.0',
            database: 'agentdb',
            password: 'postgres',
            port: 5432
        })

        client.connect()

        return client
    }


    protected disconnectClient(client: any): Promise<any> {
        return client.end()
    }

    
}