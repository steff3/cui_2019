import { describe } from 'mocha'
import * as chai from 'chai'
import * as chaiAsPromised from 'chai-as-promised'
import * as uuidv4 from 'uuid/v4'
const { expect } = chai

chai.use(chaiAsPromised)

import { GameResult } from '../src/Models/game'
import { TeamType } from '../src/Models/player'
import { SearchGameIntent } from '../src/Intents/CompositeIntents/SearchGamesIntent'
import { KickerContext } from '../src/Context/KickerContext'
import { UserRequest } from '../src/Context/UserRequest'
import { CompositeIntentState } from '../src/Intents/CompositeIntents/ICompositeIntent'

let compositeIntent: SearchGameIntent
let context: KickerContext

describe ('test Search Game Intent', function() {
    before('Create Context', async function() {
        context = new KickerContext(true)
        await context.setup()
        await context.getUserForActionId(null)
        compositeIntent = new SearchGameIntent(context, context)
    })

    this.beforeEach('Create Search Game Intent', function() {
        compositeIntent = new SearchGameIntent(context, context)
    })

    it ('test canHandle GetGamesIntent', async function() {
        const request = new UserRequest('GetMatchesIntent', {}, 'Gibt es verfügbare Spiele?')

        const success = compositeIntent.canHandle(request.intent)

        expect(success).to.be.true
    })

    it ('test handle GetGamesIntent', async function() {
        const request = new UserRequest('GetMatchesIntent', {}, 'Gibt es verfügbare Spiele?')

        compositeIntent.canHandle(request.intent)

        const success = await compositeIntent.handle(request.parameters)
        

        expect(success).to.be.true
        expect(compositeIntent.nextInquiry()).to.be.a('string')
        expect(compositeIntent.nextInquiry()).to.be.equal('Ich habe 3 Spiele gefunden. Auf welcher Position möchtest du gerne spielen?')
    })

    it ('test handle GetGamesIntent Extracting Position', async function() {
        const request = new UserRequest('GetMatchesIntent', {position: 'ATTACK'}, 'Sturm?')
        
        compositeIntent.canHandle(request.intent)

        const success = await compositeIntent.handle(request.parameters)
        

        expect(success).to.be.true
        expect(compositeIntent.nextInquiry()).to.be.a('string')
        expect(compositeIntent.nextInquiry()).to.be.equal('Ich habe 2 Spiele gefunden.')
    })

    it ('test handle GetGamesIntent 2 Requests', async function() {
        let request = new UserRequest('GetMatchesIntent', {}, 'Gibt es verfügbare Spiele?')

        compositeIntent.canHandle(request.intent)

        await compositeIntent.handle(request.parameters)
        
        request = new UserRequest('PositionIntent', {position: 'ATTACK'}, 'Sturm')

        let success = compositeIntent.canHandle(request.intent)
        expect(success).to.be.true

        success = await compositeIntent.handle(request.parameters)

        expect(success).to.be.true
        expect(compositeIntent.nextInquiry()).to.be.a('string')
        expect(compositeIntent.nextInquiry()).to.be.equal('Du spielst im Sturm.')
    })
})
