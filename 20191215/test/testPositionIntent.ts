import { describe } from 'mocha'
import * as chai from 'chai'
import * as chaiAsPromised from 'chai-as-promised'
import * as uuidv4 from 'uuid/v4'
const { expect } = chai
chai.use(chaiAsPromised)


import { PositionType, TeamType } from '../src/Models/player'
import { PositionIntent } from '../src/Intents/AtomicIntents/PositionIntent'
import { Player } from '../src/Models/player'

let player: Player
let positionIntent: PositionIntent
let nlpPositionItent: any

describe ('Test Atomic Position Intent', function() {
    before('Create Player', function() {
        player = new Player()
        nlpPositionItent = {
            intent: 'Position Intent',
            parameters: {
                position: 'ATTACK'
            }
        }
        positionIntent = new PositionIntent(0, null, null, player, 'position')
    })

    it ('Trigger CanHandle', function() {
        const success = positionIntent.canHandle(nlpPositionItent.intent)

        expect(success).to.be.true
    })

    it ('Test Handle', function() {
        // Precondition
        expect(player.position).to.be.undefined

        // Test Setup
        const success = positionIntent.handle(nlpPositionItent.parameters)

        // Test Validation
        expect(success).to.be.true
        expect(player).to.have.property('position', PositionType.Attack)
    })

    it ('Test Confirm', function() {
        const confirmation = positionIntent.confirmation()

        expect(confirmation).to.be.equal('Du spielst im Sturm.')
    })
})