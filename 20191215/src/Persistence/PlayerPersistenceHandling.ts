import { Player, PositionType, TeamType } from '../Models/player'
import { User } from '../Models/user'

export interface PlayerPersistenceHandling {
    createNewPlayer(user: User, position: PositionType, team?: TeamType): Promise<number>

    updateExistingPlayer(id: number, position?: PositionType, team?: TeamType): Promise<boolean>

    playerForId(id: number): Promise<Player>
    playersOfUser(user: User): Promise<Player[]>
    positionsOfUser(user: User):Promise<PositionType[]>

    deleteExistingPlayer(id: number): Promise<boolean>
}