import { Game, GameState } from '../Models/game'
import { Player } from '../Models/player'
import { User } from '../Models/user'
 
export interface GamePersistenceHandling {
    createNewGame(player: Player): Promise<number>

    addPlayer(id: number, player: Player, allowsSingle: boolean): Promise<boolean> 

    retrieveGames(state: GameState): Promise<Game[]>
    retrieveGame(id: number): Promise<Game> 

    retrieveGamesWithUser(user: User, state: GameState): Promise<Game[]>
    retrieveGamesForUser(user: User): Promise<Game[]>

    deleteExistingGame(id: number): Promise<boolean> 
} 