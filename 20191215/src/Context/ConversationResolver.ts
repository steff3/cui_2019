import { UserRequest } from "./UserRequest";
import { ICompositeIntent, CompositeIntentState } from "../Intents/CompositeIntents/ICompositeIntent";
import { SearchGameIntent } from "../Intents/CompositeIntents/SearchGamesIntent"
import { IntentPersistenceHandler, PlayerNameProvider } from "./KickerContext"



export class ConversationResolver {
    private sessionId: string

    private openConversationHandlers: ICompositeIntent[] = []
    private conversationHandlers: ICompositeIntent[]
    private currentConversation: ICompositeIntent

    constructor(persistenceHandler: IntentPersistenceHandler, nameProvider: PlayerNameProvider) {
        this.conversationHandlers = ResolveHelper.getConversations(persistenceHandler, nameProvider)
    }

    async handleConversationalTurn(request: UserRequest): Promise<string> {
        let matchedHandler: ICompositeIntent = null

        let answerString: string

        if (this.currentConversation && this.currentConversation.canHandle(request.intent)) {
            // console.log('found for current')
            matchedHandler = this.currentConversation
        }
        
        if ((!matchedHandler) && this.openConversationHandlers.length > 0) {
            matchedHandler = this.openConversationHandlers.find(e => e.canHandle(request.intent))
        }

        if ((!matchedHandler) && this.conversationHandlers.length > 0) {
            matchedHandler = this.conversationHandlers.find(e => e.canHandle(request.intent))
        }

        if (matchedHandler) {
            let success = await matchedHandler.handle(request.parameters)
            // console.log('success on matched handler', success)
            if (success) {
                // console.log('state on matched handler', matchedHandler.getState())
                if (matchedHandler.getState() != CompositeIntentState.OutOfScope) {
                    answerString = matchedHandler.nextInquiry()
                } else {
                    answerString = 'Ok, darüber muss ich nachdenken (2)'
                }
                this.currentConversation = matchedHandler
            }
        } else {
            // console.log("Completely Off")
            answerString = 'Ok, darüber muss ich nachdenken (3)'
        }

        return new Promise<string>(res => {
            res(answerString)
        })
    }
}

abstract class ResolveHelper {
    static getConversations(persistenceHandler: IntentPersistenceHandler, nameProvider: PlayerNameProvider): ICompositeIntent[] {
        let handlers: ICompositeIntent[] = []

        handlers.push(this.provideSearchGamesIntent(persistenceHandler, nameProvider))

        //console.log(`(3) ${handlers.length}`)
        return handlers
    }

    static provideSearchGamesIntent(persistenceHandler: IntentPersistenceHandler, nameProvider: PlayerNameProvider): SearchGameIntent {
        return new SearchGameIntent(persistenceHandler, nameProvider)
    }
}