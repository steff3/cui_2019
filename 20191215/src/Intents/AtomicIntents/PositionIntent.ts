import { IAtomicIntent, AtomicIntentState } from './IAtomicIntent'
import { BaseAtomicSlotIntent } from './BaseAtomicSlotIntent'
import { PositionType } from '../../Models/player'
import { ListenerAssigner } from '../ListenerIntents/ListenerCapableIntent'

export class PositionIntent extends BaseAtomicSlotIntent implements IAtomicIntent {
    readonly index: number
    readonly inquiry: string
    readonly keyword: string

    private position: PositionType
    private listenerAssigner: ListenerAssigner
    private action: (position: PositionType) => boolean

    constructor(index: number, action: (position: PositionType) => boolean, listenerAssigner: ListenerAssigner, target: any, key: string, inquiry?: string) {
        super(key, target)
        this.index = index
        this.listenerAssigner = listenerAssigner
        this.action = action

        if (inquiry) {
            this.inquiry = inquiry
        } else {
            this.inquiry = 'Auf welcher Position möchtest du gerne spielen?'
        }
        
        this.keyword = 'Position'
    }

    descriptionForPositionType(): string {
        switch (this.position) {
            case PositionType.Attack:
                return 'im Sturm'
            case PositionType.Defence:
                return 'in der Abwehr'
        }
    }

    //
    //      INTERFACE IINTENT
    //
    canHandle(nlpIntent: string): boolean {
        if (nlpIntent === 'PositionIntent') {
            return true
        }
        return false
    }

    handle(parameters: any): Promise<boolean> {
        let success = false

        if (parameters.position) {
            if (parameters.position === 'ATTACK') {
                this.position = PositionType.Attack
            } else if (parameters.position === 'DEFENSE') {
                this.position = PositionType.Defence
            }
        }
        
        if (this.position != undefined) {
            this.assignmentTarget[this.keyPath] = this.position
            success = true
            this.state = AtomicIntentState.Resolved

            success = this.action(this.position)
        }

        return new Promise<boolean>(res => {
            res(success)
        })
    }

    getState(): AtomicIntentState {
        return this.state
    }

    //
    //      INTERFACE IATOMICSLOTINTENT
    //
    extractSlot(parameters: any) {
        const positionString = parameters.position

        if (positionString) {
            if (positionString === 'ATTACK') {
                this.position = PositionType.Attack
            } else if (positionString === 'DEFENSE') {
                this.position = PositionType.Defence
            }
        }

        if (this.position != undefined) {
            this.assignmentTarget[this.keyPath] = this.position
            this.action(this.position)
            this.state = AtomicIntentState.Resolved
        }
    }

    //
    //      INTERFACE IATOMICINTENT
    //
    confirmation(): string {
        return `Du spielst ${ this.descriptionForPositionType() }.`
    }
}