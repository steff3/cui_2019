import { IAtomicIntent } from './IAtomicIntent'

export interface IAtomicSlotIntent {
    readonly keyPath: string
    readonly assignmentTarget: any
    
    extractSlot(parameters: any)
}