import { IAtomicSlotIntent } from './IAtomicSlotIntent'
import { AtomicIntentState } from './IAtomicIntent'

export abstract class BaseAtomicSlotIntent implements IAtomicSlotIntent {
    readonly keyPath: string
    readonly assignmentTarget: any

    protected state: AtomicIntentState

    constructor(keyPath: string, assignmentTarget: any) {
        this.keyPath = keyPath
        this.assignmentTarget = assignmentTarget
        this.state = AtomicIntentState.Open
    }

    extractSlot(parameters: any) {
        this.assignmentTarget[this.keyPath] = parameters[this.keyPath]
    }
 
    //  Kind of a generic implementation of the canHandle-function 
    //  for slot-based AtomicIntents
    handle(parameters: any): Promise<boolean> {
        let success = false

        if (parameters[this.keyPath]) {
            this.assignmentTarget[this.keyPath] = parameters[this.keyPath]
            this.state = AtomicIntentState.Resolved
            success = true
        }

        return new Promise<boolean>(res => {
            res(success)
        })
    }
}