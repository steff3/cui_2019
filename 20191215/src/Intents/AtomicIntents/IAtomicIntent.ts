import { IIntent } from '../IIntent'

export enum AtomicIntentState {
    Open,
    Conflicting,
    AwaitingConfirmation,
    Resolved
}

export interface IAtomicIntent extends IIntent {
    readonly index: number

    readonly inquiry: string
    readonly keyword: string

    getState(): AtomicIntentState
    
    confirmation(): string
}
