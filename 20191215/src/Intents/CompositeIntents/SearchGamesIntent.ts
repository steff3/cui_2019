import { ICompositeIntent, CompositeIntentState } from './ICompositeIntent'
import { IAtomicIntent, AtomicIntentState } from '../AtomicIntents/IAtomicIntent'
import { IAtomicSlotIntent } from '../AtomicIntents/IAtomicSlotIntent'
import { BaseAtomicSlotIntent } from '../AtomicIntents/BaseAtomicSlotIntent'
import { IListenerIntent, ListenerType } from '../ListenerIntents/BaseIntentListener'
import { ListenerCapableIntent } from '../ListenerIntents/ListenerCapableIntent'
import { IntentPersistenceHandler, PlayerNameProvider } from '../../Context/KickerContext'
import { PositionIntent } from '../AtomicIntents/PositionIntent'
import { Game } from '../../Models/game'
import { Player, PositionType } from '../../Models/player'
import { UserConfirmationResultConsumer } from '../ListenerIntents/UserConfirmationIntent'


export class SearchGameIntent extends ListenerCapableIntent implements ICompositeIntent, UserConfirmationResultConsumer {
    private assitantIntent: string = 'GetMatchesIntent'
    private intents: IAtomicIntent[]
    private availableGames: Game[]

    currentIntent: IAtomicIntent
    currentListener: IListenerIntent
    private state: CompositeIntentState


    private persistenceHandler: IntentPersistenceHandler
    private nameProvider: PlayerNameProvider

    private player: Player

    constructor(persistenceHandler: IntentPersistenceHandler, nameProvider: PlayerNameProvider) {
        super()
        this.persistenceHandler = persistenceHandler
        this.state = CompositeIntentState.OutOfScope
        this.nameProvider = nameProvider

        this.player = new Player()

        this.intents = []
        const action = (position: PositionType): boolean => {
            let success = false
            if (this.availableGames) {
                //console.log(`number of games before filter :: ${this.availableGames.length}`)
                this.availableGames = this.availableGames.filter(e => e.canAddPlayerOnPosition(position))
                //console.log(`number of games after filter :: ${this.availableGames.length}`)
                success = true
            }
            return success
        }
        this.addListener(ListenerType.UserConfirmation)
        this.intents.push(new PositionIntent(0, action, this, this.player, 'position'))
    }

    //
    //      Implementing UserConfirmationResultConsumer
    //
    async consume(confirmed: boolean): Promise<boolean> {
        let success = false
        switch (confirmed) {
            case true:
                if (this.state == CompositeIntentState.Resolved) {
                    // this is fine .... I will book
                    success = await this.persistenceHandler.addToGame(this.availableGames[0].id, this.player.position)
                    if (success) {
                        this.state = CompositeIntentState.Confirmed
                    }
                } else if (this.state == CompositeIntentState.Refused) {
                    // switch to New Game Intent
                    success = true
                }
            case false: 
                if (this.state == CompositeIntentState.Resolved) {
                    // ok, ask if user wants to start her own game
                    this.state = CompositeIntentState.Refused
                    success = true
                } else if (this.state == CompositeIntentState.Refused) {
                    // ok, seems I cannot really do something for you right now
                    success = true
                }
                
        }

        return new Promise<boolean>(res => {
            res(success)
        })
    }

    //
    //      Implementing ICompositeIntent
    //
    canHandle(nlpIntent: string): boolean {
        this.currentIntent = null
        this.currentListener = null
        let success: boolean = false
        if (nlpIntent === this.assitantIntent) {
            success = true
            this.state = CompositeIntentState.Inspecting
        } else {
            for (let listener of this.listeners) {
                if (listener.canHandle(nlpIntent)) {
                    this.currentListener = listener
                    break
                }
            }
            if (this.currentListener == null) {
                for (let intent of this.intents) {
                    if (intent.canHandle(nlpIntent)) {
                        this.currentIntent = intent
                        break
                    }
                }
            }
            

            if (this.currentListener || this.currentIntent) {
                if (this.currentIntent) {
                    this.state = CompositeIntentState.InScope
                }
                success = true
            }
        }
        
        return success
    }

    async handle(parameters: any): Promise<boolean> {
        let success = false
        if (this.state != CompositeIntentState.OutOfScope) {
            if (this.state == CompositeIntentState.Inspecting) {
                this.availableGames = await this.findAvailableGames()

                if (this.availableGames.length == 0) {
                    this.state = CompositeIntentState.Conflicting
                } else {
                    this.state = CompositeIntentState.InScope
                }
                success = true
            } else {
                if (this.currentListener) {
                    success = await this.currentListener.handle(parameters)
                }
                if (this.currentIntent) {
                    success = await this.currentIntent.handle(parameters)
                }
            }

            //
            //  Extract additional slot parameters
            //
            const others = this.intents.filter(e => ((e instanceof BaseAtomicSlotIntent) && (e.getState() == AtomicIntentState.Open))) as unknown as IAtomicSlotIntent[]

            for (let intent of others) {
                intent.extractSlot(parameters)
            }
        }

        return new Promise<boolean>(res => {
            res(success)
        })
    }

    getState(): CompositeIntentState {
        if (this.state == CompositeIntentState.OutOfScope 
            || this.state == CompositeIntentState.Conflicting 
            || this.state == CompositeIntentState.Confirmed
            || this.state == CompositeIntentState.Refused) {
            return this.state
        } else {
            const conflicting = this.intents.find(e => e.getState() == AtomicIntentState.Conflicting)

            if (conflicting) {
                return CompositeIntentState.Conflicting
            }

            const open = this.intents.find(e => e.getState() == AtomicIntentState.Open)
            if (open) {
                return CompositeIntentState.InScope
            }

            this.state = CompositeIntentState.Resolved
            return this.state
        }
    }

    nextInquiry(): string {
        if (this.state == CompositeIntentState.OutOfScope) {
            return ""
        } else if (this.state == CompositeIntentState.Confirmed) {
            this.state = CompositeIntentState.OutOfScope
            return "Super. Ich habe dich für ein Spiel eingetragen. Ich gebe dir Bescheid, wenn es beginnt."
        } else if (this.state == CompositeIntentState.Refused) {
            //this.state = CompositeIntentState.OutOfScope
            return "Ok. Möchtest Du stattdessen lieber ein neues Spiel starten?"
        }

        if (this.state == CompositeIntentState.Resolved) {
            let message = this.gameCountStatement()
            this.assignListener(ListenerType.UserConfirmation, this)
            return `${ message } Soll ich dich in eines der Spiele eintragen?`
        } else if (this.currentIntent) {
            let confirmation = this.currentIntent.confirmation()

            let nextIntent = this.intents.find(e => e.getState() == AtomicIntentState.Open)
            if (nextIntent) {
                return `${ confirmation } ${nextIntent.inquiry}`
            } else {
                return confirmation
            }
        } else {
            if (this.state == CompositeIntentState.Conflicting) {
                return 'Leider sind im Moment keine Spiele verfügbar! Willst Du ein neues Spiel anlegen?'
            } else if (this.state == CompositeIntentState.InScope) {

                let confirmMessage = this.gameCountStatement()
                const open = this.intents.find(e => e.getState() == AtomicIntentState.Open)
                
                if (open) {
                    return `${ confirmMessage } ${ open.inquiry }`
                } else {
                    return confirmMessage
                }
                
            }
        }
        
    }

    //
    //      Internal Operations
    //
    async findAvailableGames(): Promise<Game[]> {
        return this.persistenceHandler.availableGamesForUser()
    }

    gameCountStatement(): string {
        let message: string 
        if (this.availableGames.length == 0) {
            message = 'Ich konnte leider keine Spiele für dich finden.'
        } else if (this.availableGames.length == 1) {
            message = 'Ich habe ein Spiel gefunden.'
        } else {
            message = `Ich habe ${ this.availableGames.length } Spiele gefunden.`
        }
        return message
    }
}