import { IIntent } from '../IIntent'

export enum CompositeIntentState {
    OutOfScope, //
    InScope,
    Inspecting,
    Conflicting,
    Resolved,
    Confirmed,
    Refused
}

export interface ICompositeIntent extends IIntent {
    getState(): CompositeIntentState
    nextInquiry(): string
}