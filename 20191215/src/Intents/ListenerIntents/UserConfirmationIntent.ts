import { ListenerType, BaseIntentListener } from './BaseIntentListener'

export interface UserConfirmationResultConsumer {
    consume(confirmed: boolean): Promise<boolean>
}

export class UserConfirmationIntent extends BaseIntentListener<UserConfirmationResultConsumer> {
    constructor() {
        super(ListenerType.UserConfirmation)
    }

    async handle(parameters: any): Promise<boolean> {
        return this.consumer.consume(parameters.granted)
    }

    canHandle(nlpIntent: string): boolean {
        return nlpIntent === 'UserConfirmationIntent' && (this.consumer != null || this.consumer != undefined)
    }
}
