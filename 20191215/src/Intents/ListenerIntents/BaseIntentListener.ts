import { IIntent } from "../IIntent"

export enum ListenerType {
    UserConfirmation,
    TouchInteraction
}

export interface IListenerIntent extends IIntent {
    isListenerOfType(type: ListenerType): boolean
}

export abstract class BaseIntentListener<T> implements IListenerIntent {
    private type: ListenerType

    protected consumer: T

    constructor(type: ListenerType) {
        this.type = type
    }

    setConsumer(consumer: T) {
        this.consumer = consumer
    }

    isListenerOfType(type: ListenerType): boolean {
        return this.type === type
    }

    canHandle(nlpIntent: string): boolean {
        throw new Error("Method not implemented.");
    }

    handle(parameters: any): Promise<boolean> {
        throw new Error("Method not implemented.");
    }
}

