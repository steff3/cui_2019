import { ListenerType, BaseIntentListener } from './BaseIntentListener'


export class TouchInteractionSelection {
    readonly index: number
    readonly value: string

    constructor(index: number, value: string) {
        this.index = index
        this.value = value
    }
}

export interface TouchInteractionResultConsumer {
    consume(selection: TouchInteractionSelection): Promise<boolean>
}

export class TouchInteractionIntent extends BaseIntentListener<TouchInteractionResultConsumer> {
    constructor() {
        super(ListenerType.TouchInteraction)
    }

    async handle(parameters: any): Promise<boolean> {
        const selectedOption = new TouchInteractionSelection(parameters.OPTION, parameters.text)

        return this.consumer.consume(selectedOption)
    }

    canHandle(nlpIntent: string): boolean {
        return nlpIntent === 'TouchInteractionIntent' && (this.consumer != null || this.consumer != undefined)
    }

}