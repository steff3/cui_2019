import { User } from '../Models/user'
import { PositionType } from '../Models/player'

export interface UserPersistenceHandling {
    createNewUser(firstName: string, lastName: string, playerName: string, actionId: string): Promise<number>

    deleteExistingUser(id: number): Promise<boolean>

    retrieveUser(id: number): Promise<User>
    retrieveUserByActionId(id: string): Promise<User>
    retrieveAllUsers():Promise<User[]>

    updatePlayerName(id: number, name: String): Promise<boolean>
    updatePreferredPosition(id: number, position: PositionType): Promise<boolean>
}

