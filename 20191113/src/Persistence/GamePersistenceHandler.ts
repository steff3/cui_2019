import { BasePersistenceHandler, TableType } from "./BasePersistenceHandler"



export class GamePersistenceHandler extends BasePersistenceHandler {
    private gameTable: string = 'game_data'

    ///
    ///     DATABASE OPERATIONS
    ///

    private createGameTable(client: any): Promise<any> {
        const queryText = `CREATE TABLE IF NOT EXISTS ${ this.gameTable } (
            id SERIAL PRIMARY KEY,
            player_1_id INT REFERENCES ${ TableType.Player } (id) ON DELETE RESTRICT,
            player_2_id INT REFERENCES ${ TableType.Player } (id) ON DELETE RESTRICT,
            player_3_id INT REFERENCES ${ TableType.Player } (id) ON DELETE RESTRICT,
            player_4_id INT REFERENCES ${ TableType.Player } (id) ON DELETE RESTRICT);`

        return client.query(queryText)
    }

}