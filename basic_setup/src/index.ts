

import * as express from 'express'
import * as bodyParser from 'body-parser'

import { dialogflow, Permission} from 'actions-on-google'


const app = dialogflow({debug: true})



app.fallback((conv) => {
    const intent = conv.intent

    conv.close('Ok, thanks!');
})

const expressApp = express().use(bodyParser.json())

expressApp.post('/fulfillment', app)

expressApp.listen(3000)