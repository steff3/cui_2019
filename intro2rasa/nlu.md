## intent:CreateNewGameIntent
- start a new game
- create a new game
- start a new match
- create a new match
- create new game
- create new match
- create new
- new match

## intent:PositionIntent
- [Attack](position)
- [attack](position)
- [Defense](position)
- [defense](position)
- I want to play on position [attack](position)
- I want to play on position [defense](position)

## intent:ThanksIntent
- Thanks
- Great
- thanks
- great

## intent:WelcomeIntent
- hello
- Hello
- Hey there
- hey there

## intent:JoinGameIntent
- join a game
