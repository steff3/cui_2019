## Story from conversation with f0279b9e-9e14-4822-916c-9d16bc6df998 on January 13th 2020

* WelcomeIntent
    - utter_greet
* CreateNewGameIntent
    - utter_ask_position
* PositionIntent{"position":"Attack"}
    - utter_confirm
* ThanksIntent
    - utter_thanks

## New Game Story

* WelcomeIntent
    - utter_greet
* CreateNewGameIntent
    - utter_ask_position
* PositionIntent
    - utter_confirm
* ThanksIntent
    - utter_thanks

## Story from conversation with d1ee263b-e016-4101-a5c0-50a5a2b1a185 on January 14th 2020

* WelcomeIntent
    - utter_greet
* CreateNewGameIntent
    - utter_ask_position
* PositionIntent{"position":"defense"}
    - slot{"position":"defense"}
    - utter_confirm
    - slot{"position":"defense"}
* ThanksIntent
    - utter_thanks

## Join Existing Game Story

* WelcomeIntent
    - utter_greet
* JoinGameIntent
    - utter_searchGame
    - utter_ask_join
* CreateNewGameIntent
    - utter_confirm
