

import * as express from 'express'
import * as bodyParser from 'body-parser'
import * as uuidv4 from 'uuid'

import { dialogflow, Permission} from 'actions-on-google'
import { ContextHandling, KickerContext } from './Context/KickerContext'
import { User } from './Models/user'
import { Game } from './Models/game'
import { Player, PositionType } from './Models/player'

let isUserKnown: boolean = false

const app = dialogflow({debug: true})

const context: ContextHandling = new KickerContext()
let currentUser: User
let retrievedGames: Game[]


app.middleware(conv =>  {
    //isUserKnown = ('userId' in conv.user.storage)
    
    
    
})

app.intent('Default Welcome Intent', conv => {
    if (isUserKnown) {
        conv.ask('Hallo. Wie geht es Dir heute?')
    } else {
        console.log(conv.intent)
        conv.ask('Hallo. Wer bist Du denn?')
        conv.ask(new Permission({
            context: "Wie ist Dein Name?",
            permissions: ["NAME"]
        }))
    }
})

app.intent('GetMatchesIntent', (conv) => {
    // Request to GamePersistenceHandler
   return context.createUser("Steffen", "Blümm", "Steff")
        .then(res => {
            return context.retrieverUserForId(res)
        })
        .then(res => {
                currentUser = res
                return context.getAvailableGame(currentUser)
            })
            .then(res => {
                retrievedGames = res
                const answer = `Es gibt im Moment ${res.length} Spiele bei denen Du mitspielen kannst. 
                Hast Du eine bevorzugte Position, auf der du spielen möchtest?`

                return conv.ask(answer)
            }) 
})

app.intent('PositionIntent', (conv) => {
    // Request to GamePersistenceHandler
    const { Position } = conv.parameters
    let available = false
    if (Position) {
        let playingPositon: PositionType
        if (Position === 'Defense') {
            playingPositon = PositionType.Defence
        } else {
            playingPositon = PositionType.Attack
        }
        if (retrievedGames && retrievedGames.length > 0) {
            const thisPlayer = new Player()
            thisPlayer.position = playingPositon
            
            for(let game of retrievedGames) {  
                if (game.canAddPlayer(thisPlayer)) {
                    available = true
                    break
                }

            }
        }
    }

    if (available) {
        conv.ask("Du kannst einem der beiden Spiele beitreten. Soll ich dich hinzufügen?")
    } else {
        conv.ask(`keines der Spiele benötigt eine Spieler auf Position ${Position}`)
    }

})

app.intent('NegativeConfirmationIntent', (conv) => {
    // Request to GamePersistenceHandler
    conv.ask("")
})

app.intent('PositiveConfirmationIntent', (conv) => {
    // Request to GamePersistenceHandler
    conv.ask("")
})

app.intent('UserPermissionIntent', (conv, params, confirmationGranted) => {
    console.log(params)
    const {name} = conv.user
    console.log('user\'s name' + name.given + ', ' + name.family)

    if (confirmationGranted) {
        conv.close('Hallo ' + name.given)
        const newUserId = uuidv4()
        conv.user.storage['userId'] = newUserId
    } else {
        conv.close('Das ist aber Schade!')
    }
})


app.fallback((conv) => {
    const intent = conv.intent

    conv.close('Ok, thanks!');
})

const expressApp = express().use(bodyParser.json())

expressApp.post('/fulfillments', app)

expressApp.listen(3000)