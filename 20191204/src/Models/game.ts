import {Player, PositionType, TeamType, PlayingPosition} from './player'

export enum GameState {
    All,
    Pending,
    Filled,
    Learnable
}

export class GameResult {
    private goalMap: Map<TeamType, number>

    constructor() {
        this.goalMap = new Map<TeamType, number>()
        this.goalMap.set(TeamType.Blue, 0)
        this.goalMap.set(TeamType.Red, 0)
    }

    setNumberOfGoals(team: TeamType, achievedGoals: number): boolean {
        if ((team == TeamType.Red || team == TeamType.Blue) && achievedGoals >= 0) {
            this.goalMap.set(team, achievedGoals)
            return true
        }
        return false
    }

    goalsForTeam(team: TeamType): number {
        if (!(team == TeamType.Red || team == TeamType.Blue)) {
            // thinking about extending this enum .... taking precautions
            return -1
        }
        return this.goalMap.get(team)
    }

    addGoalForTeam(team: TeamType): boolean {
        if (!(team == TeamType.Red || team == TeamType.Blue)) {
            // thinking about extending this enum .... taking precautions
            return false
        }

        if (this.goalMap.get(team)) {
            let numOfGoals = this.goalMap.get(team)
            this.goalMap.set(team, numOfGoals + 1)
        } else {
            this.goalMap.set(team, 1)
        }
    }

    isComplete(): boolean {
        if (this.goalMap.size != 2) {
            return false
        }

        if (this.goalMap.get(TeamType.Blue) == this.goalMap.get(TeamType.Red)) {
            return false
        }

        if (!(this.goalMap.get(TeamType.Blue) == 10 || this.goalMap.get(TeamType.Red) == 10)) {
            return false
        }

        return true
    }
}

export class Game {
    id: number
    players: Player[]
    result: GameResult
    createdAt: Date
    lastChanged: Date

    // constructor(player: Player) {
    //     this.players = new Array(4)
    //     this.players.push(player)
    //     this.time = new Date()
    // }

    isComplete(): boolean {
        if (this.players.length == 4) {
            return true
        } else {
            return false
        }
    }

    canAddPlayer(player: Player): boolean {
        if (this.players.length < 4) {
            if (this.players.filter(e => (e.position == player.position)).length < 2) {
                return true
            }
        }
        return false
    }
}


/*
isComplete(): boolean {
        if (this.players.length == 4) {
            return true
        } else {
            return false
        }
    }

    canAddPlayer(player: Player): boolean {
        if (this.players.length < 4) {
            if (this.players.find(e => (e.position == player.position)) == null) {
                return true
            }
        }
        return false
    }

    getOpenPositions(): PlayingPosition[] {
        if (this.players.length == 4) {
            return []
        } else {
            let possiblePositions = [new PlayingPosition(PositionType.Attack, TeamType.Blue), 
                                    new PlayingPosition(PositionType.Defence, TeamType.Blue), 
                                    new PlayingPosition(PositionType.Attack, TeamType.Red), 
                                    new PlayingPosition(PositionType.Defence, TeamType.Red)]

            for (const player of this.players) {
                const index = possiblePositions.indexOf(player.position)
                if (index > -1) {
                    possiblePositions.splice(index, 1);
                }
            }

            return possiblePositions
        }
    }
    
    addPlayer(player: Player) {
        if (this.canAddPlayer(player)) {
            this.players.push(player)
        }
    }

    removePlayer(player: Player) {
        const index = this.players.indexOf(player, 0);
        if (index > -1) {
            this.players.splice(index, 1);
        }
    }
}
*/