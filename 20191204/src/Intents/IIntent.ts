export interface IIntent {
    canHandle(nlpIntent: string): boolean
    handle(parameters: any): boolean
    isHandled(): boolean
}