import { IIntent } from '../IIntent'

export interface IAtomicIntent extends IIntent {
    readonly index: number

    readonly inquiry: string
    readonly keyword: string
    
    confirmation(): string
}