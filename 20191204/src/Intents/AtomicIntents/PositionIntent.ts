import { IAtomicSlotIntent } from './IAtomicSlotIntent'
import { PositionType } from '../../Models/player'

export class PositionIntent implements IAtomicSlotIntent {
    readonly index: number
    readonly inquiry: string
    readonly keyword: string

    private position: PositionType
    private assignmentKey: string
    private assignmentObject: any

    constructor(index: number, object: any, key: string, inquiry?: string) {
        this.index = index
        this.assignmentKey = key
        this.assignmentObject = object
        //this.assignmentObject[this.assignmentKey] = this.position

        if (inquiry) {
            this.inquiry = inquiry
        } else {
            this.inquiry = 'Auf welcher Position möchtest du spielen?'
        }
        
        this.keyword = 'Position'
    }

    descriptionForPositionType(): string {
        switch (this.position) {
            case PositionType.Attack:
                return 'im Sturm'
            case PositionType.Defence:
                return 'in der Abwehr'
        }
    }

    //
    //      INTERFACE IINTENT
    //
    canHandle(nlpIntent: string): boolean {
        if (nlpIntent === 'Position Intent') {
            return true
        }
        return false
    }

    handle(parameters: any): boolean {
        let success = false

        if (parameters.position) {
            if (parameters.position === 'ATTACK') {
                this.position = PositionType.Attack
            } else if (parameters.position === 'DEFENSE') {
                this.position = PositionType.Defence
            }
        }
        
        if (this.position != undefined) {
            this.assignmentObject[this.assignmentKey] = this.position
            success = true
        }

        return success
    }

    isHandled(): boolean {
        return false
    }

    //
    //      INTERFACE IATOMICSLOTINTENT
    //
    extractSlot(parameters: any) {
        const {positionString} = parameters.position

        if (positionString) {
            if (positionString === 'ATTACK') {
                this.position = PositionType.Attack
            } else if (positionString === 'DEFENSE') {
                this.position = PositionType.Defence
            }
        }

        if (this.position != undefined) {
            this.assignmentObject[this.assignmentKey] = this.position
        }
        
    }

    //
    //      INTERFACE IATOMICINTENT
    //
    confirmation(): string {
        return `Du spielst ${ this.descriptionForPositionType() }.`
    }
}