import { ICompositeIntent } from './ICompositeIntent'
import { IAtomicIntent } from '../AtomicIntents/IAtomicIntent'
import { IAtomicSlotIntent } from '../AtomicIntents/IAtomicSlotIntent'


export class SearchGameIntents implements ICompositeIntent {
    private intents: IAtomicIntent[]

    currentIntent: IAtomicIntent

    canHandle(nlpIntent: string): boolean {
        if (nlpIntent === 'TransactionIntent') {
            for (let intent of this.intents) {
                
            }
        }
        for (let intent of this.intents) {
            if (intent.canHandle(nlpIntent)) {
                this.currentIntent = intent
                return true
            }
        }
        return false
    }
    handle(parameters: any): boolean {
        let success = false
        success = this.currentIntent.handle(parameters)

        return success
    }

    isHandled(): boolean {
        let isHandled = true
        for (let intent of this.intents) {
            if (!intent.isHandled) {
                isHandled = false
            }
        }
        return isHandled
    }

    nextInquiry(): string {
        let confirmation = this.currentIntent.confirmation()
        let currentIndex = this.currentIntent.index

        let nextIntent = this.intents.find(e => e.index == currentIndex + 1)

        return `${ confirmation } ${nextIntent.inquiry}`
    }
}