import { UserPersistenceHandling } from '../Persistence/UserPersistenceHandling'
import { PlayerPersistenceHandling } from '../Persistence/PlayerPersistenceHandling'
import { GamePersistenceHandling } from '../Persistence/GamePersistenceHandling'
import { GameMockHandler } from '../Persistence/Mock/GameMockHandler'
import { UserMockHandler } from '../Persistence/Mock/UserMockHandler'
import { PlayerMockHandler } from '../Persistence/Mock/PlayerMockHandler'
import * as uuidv4 from 'uuid'

import { Game } from '../Models/game'
import { User } from '../Models/user'
import { Player, PositionType } from '../Models/player'

export interface ContextHandling {
    getAvailableGame(user: User): Promise<Game[]>

    createUser(firstName: string, lastName: string, playerHandler: string): Promise<number>
    retrieverUserForId(id: number): Promise<User>
}

export class KickerContext implements ContextHandling {
    protected userHandler: UserPersistenceHandling
    protected playerHandler: PlayerPersistenceHandling
    protected gameHandler: GamePersistenceHandling

    helper = new ContextHelper()

    constructor() {
        this.userHandler = new UserMockHandler()
        const pHandler = new PlayerMockHandler()
        this.gameHandler = new GameMockHandler(pHandler)
        this.playerHandler = pHandler

        this.setup()
    }

    async setup() {
        await this.helper.setUpUsers(this.userHandler)
        await this.helper.setUpPlayer(this.playerHandler)
        await this.helper.setUpGames(this.gameHandler)
    }

    getAvailableGame(user: User): Promise<Game[]> {
        return this.gameHandler.retrieveGamesForUser(user)
    }

    retrieverUserForId(id: number): Promise<User> {
        return this.userHandler.retrieveUser(id)
    }

    createUser(firstName: string, lastName: string, playerHandler: string): Promise<number> {
        return this.createUserInMock(firstName, lastName, playerHandler)
    }



    async createUserInMock(firstName: string, lastName: string, playerName: string): Promise<number> {
        const actionId = uuidv4()
        return  this.userHandler.createNewUser(firstName, lastName, playerName, actionId)
    }
}



class ContextHelper {
    private userHandler: UserPersistenceHandling
    private playerHandler: PlayerPersistenceHandling
    private gameHandler: GamePersistenceHandling

    private users: User[] = []

    async setUpUsers(hander: UserPersistenceHandling) {
        this.userHandler = hander
        await this.createUser('Mario', 'Maier', 'SuperMario')
        await this.createUser('Dieter', 'Bauer', 'Didi')
        await this.createUser('Sara', 'Neuhaus', 'Sasa')
        await this.createUser('Lena', 'Lukatsch', 'Leni')
        await this.createUser('Bubu', 'Loreny', 'Bubu')

        console.dir(this.users, {depth:null})
    }

    async createUser(firstName: string, lastName: string, playerName: string) {
        const actionId = uuidv4()
        const userId = await this.userHandler.createNewUser(firstName, lastName, playerName, actionId)
        const user = await this.userHandler.retrieveUser(userId)

        this.users.push(user)
    }

    async setUpPlayer(hander: PlayerPersistenceHandling) {
        this.playerHandler = hander


    }

    async setUpGames(handler: GamePersistenceHandling) {
        this.gameHandler = handler

        // create player for user0
        let playerId = await this.playerHandler.createNewPlayer(this.users[0], PositionType.Attack)
        let player = await this.playerHandler.playerForId(playerId)
        const game1id = await this.gameHandler.createNewGame(player)

        // add user3 to game
        playerId = await this.playerHandler.createNewPlayer(this.users[3], PositionType.Defence)
        player = await this.playerHandler.playerForId(playerId)
        await this.gameHandler.addPlayer(game1id, player, false)

        // create player for user0
        playerId = await this.playerHandler.createNewPlayer(this.users[2], PositionType.Attack)
        player = await this.playerHandler.playerForId(playerId)
        const game2id = await this.gameHandler.createNewGame(player)

        // add user3 to game
        playerId = await this.playerHandler.createNewPlayer(this.users[1], PositionType.Defence)
        player = await this.playerHandler.playerForId(playerId)
        await this.gameHandler.addPlayer(game2id, player, false)
    }

    async createPlayer(user: User, position: PositionType) {
        const playerId = await this.playerHandler.createNewPlayer(user, position)

    }
}