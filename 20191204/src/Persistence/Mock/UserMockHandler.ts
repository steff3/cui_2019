import { UserPersistenceHandling } from '../UserPersistenceHandling'
import { User } from '../../Models/user'
import { PositionType } from '../../Models/player'

export class UserMockHandler implements UserPersistenceHandling {
    private users: User[] = []
    private numOfDeleted = 0

    createNewUser(firstName: string, lastName: string, playerName: string, actionId: string): Promise<number> {
        const newUser = new User()
        newUser.id = (this.users.length + this.numOfDeleted + 1)
        newUser.firstName = firstName
        newUser.lastName = lastName
        newUser.playerName = playerName
        newUser.actionId = actionId
        this.users.push(newUser)

        return new Promise<number>(res => res(newUser.id))
    }

    deleteExistingUser(id: number): Promise<boolean> {
        let success = false

        const index = this.users.findIndex(e => e.id == id)

        if (index >= 0) {
            const deletedUsers = this.users.splice(index, 1)
            if (deletedUsers.length == 1 && deletedUsers[0].id == id) {
                this.numOfDeleted++
                success = true
            }
        }

        return new Promise<boolean>(res => res(success))
    }

    retrieveUser(id: number): Promise<User> {
        let retrievedUser: User

        const index = this.users.findIndex(e => e.id == id)
        console.log(`index is ${ index } :: id is ${ id }`)

        if (index >= 0) {
            retrievedUser = this.copyOfUser(index)
        }

        return new Promise<User>(res => res(retrievedUser))
    }

    retrieveUserByActionId(id: string): Promise<User> {
        let retrievedUser: User

        const index = this.users.findIndex(e => e.actionId == id)
        if (index >= 0) {
            retrievedUser = this.copyOfUser(index)
        }

        return new Promise<User>(res => res(retrievedUser))
    }

    retrieveAllUsers():Promise<User[]> {
        const retrievedUsers: User[] = []

        for (let user of this.users) {
            retrievedUsers.push(this.copyOfUser(user.id))
        }

        return new Promise<User[]>(res => res(retrievedUsers))
    }

    updatePlayerName(id: number, name: string): Promise<boolean> {
        let success = false

        if (this.users.length > id) {
            this.users[id].playerName = name
        }

        return new Promise<boolean>(res => res(success))
    }

    updatePreferredPosition(id: number, position: PositionType): Promise<boolean> {
        let success = false

        if (this.users.length > id) {
            this.users[id].preferredPosition = position
        }

        return new Promise<boolean>(res => res(success))
    }


    copyOfUser(index: number): User {
        const newUser = new User()

        newUser.firstName = this.users[index].firstName
        newUser.lastName = this.users[index].lastName
        newUser.playerName = this.users[index].playerName
        newUser.actionId = this.users[index].actionId
        newUser.id = this.users[index].id

        if (this.users[index].preferredPosition) {
            newUser.preferredPosition = this.users[index].preferredPosition
        }

        return newUser
    }
}