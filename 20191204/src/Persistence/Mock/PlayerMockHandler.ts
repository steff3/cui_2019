import { PlayerPersistenceHandling } from '../PlayerPersistenceHandling'
import { Player, PositionType, TeamType } from '../../Models/player'
import { User } from '../../Models/user'

export class PlayerMockHandler implements PlayerPersistenceHandling {
    private players: Player[] = []
    private numOfDeleted: number = 0

    createNewPlayer(user: User, position: PositionType, team?: TeamType): Promise<number> {
        const newPlayer = new Player()
        newPlayer.id = this.players.length + this.numOfDeleted + 1
        newPlayer.name = user.playerName
        newPlayer.position = position
        if (team) {
            newPlayer.team = team
        }
        newPlayer.userId = user.id
        this.players.push(newPlayer)

        return new Promise<number>(res => res(newPlayer.id))
    }

    updateExistingPlayer(id: number, position?: PositionType, team?: TeamType): Promise<boolean> {
        let success = false

        if (this.players.length + this.numOfDeleted > id) {
            const index = this.players.findIndex(e => e.id == id)

            if (index >= 0) {
                const thatPlayer = this.players[index]
            
                if (position) {
                    thatPlayer.position = position
                }

                if (team) {
                    thatPlayer.team = team
                }
                success = true
            }
        }

        return new Promise<boolean>(res => res(success))
    }

    playerForId(id: number): Promise<Player> {
        let retrievedPlayer: Player

        const index = this.players.findIndex(e => e.id == id)

        if (index >= 0) {
            retrievedPlayer = this.copyOfPlayer(index)
        }

        return new Promise<Player>(res => res(retrievedPlayer))
    }

    playersOfUser(user: User): Promise<Player[]> {
        let retrievedPlayers: Player[] = []

        const filteredPlayers = this.players.filter(e => e.userId == user.id)
        for (let player of filteredPlayers) {
            const index = this.players.findIndex(e => e.id == player.id)
            retrievedPlayers.push(this.copyOfPlayer(index))
        }

        return new Promise<Player[]>(res => res(retrievedPlayers))
    }

    positionsOfUser(user: User): Promise<PositionType[]> {
        let retrievedPositions: PositionType[] = []

        const filteredPlayers = this.players.filter(e => e.userId == user.id)
        for (let player of filteredPlayers) {
            retrievedPositions.push(player.position)
        }

        return new Promise<PositionType[]>(res => res(retrievedPositions))
    }

    deleteExistingPlayer(id: number): Promise<boolean> {
        let success = false

        const index = this.players.findIndex(e => e.id == id)

        if (index >= 0) {
            const deletedPlayers = this.players.splice(index, 1)
            if (deletedPlayers.length == 1 && deletedPlayers[0].id == id) {
                this.numOfDeleted++
                success = true
            }
        }

        return new Promise<boolean>(res => res(success))
    }

    copyOfPlayer(index: number): Player {
        const copiedPlayer = new Player()

        copiedPlayer.id = this.players[index].id
        copiedPlayer.name = this.players[index].name
        copiedPlayer.position = this.players[index].position
        copiedPlayer.userId = this.players[index].userId

        if (this.players[index].team) {
            copiedPlayer.team = this.players[index].team
        }

        return copiedPlayer
    }

    private copyOfPlayerWithId(index: number): Player {
        const copiedPlayer = new Player()

        copiedPlayer.id = this.players[index].id
        copiedPlayer.name = this.players[index].name
        copiedPlayer.position = this.players[index].position
        copiedPlayer.userId = this.players[index].userId

        if (this.players[index].team) {
            copiedPlayer.team = this.players[index].team
        }

        return copiedPlayer
    }
}