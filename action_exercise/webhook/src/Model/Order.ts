import { Present } from './Present'

export class Order {
    dateOrdered: Date
    dateDelivery: Date

    item: Present
}