import { Product } from './Product'

export class Present {
    product: Product
    greeting: string

    constructor(product: Product, greeting: string) {
        this.product = product
        this.greeting = greeting
    }
}