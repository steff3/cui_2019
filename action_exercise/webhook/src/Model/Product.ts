

export class Product {
    name: string
    price: number
    rating: number

    constructor(name: string, price: number, rating: number) {
        this.name = name
        this.price = price
        this.rating = rating
    }
}