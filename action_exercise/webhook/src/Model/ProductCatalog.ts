import { Product } from "./Product";


export class ProductCatalog {
    readonly items: Product[]

    constructor() {
        this.items = []

        this.items.push(new Product('Valentin-Ring Deluxe', 99.95, 5))
        this.items.push(new Product('Valentin-Halskette', 69.95, 5))
        this.items.push(new Product('Valentin-Ring', 49.95, 5))
        this.items.push(new Product('Valentin-Armreif', 39.95, 5))
        this.items.push(new Product('Valentin-Pralinen', 29.95, 5))
    }
}