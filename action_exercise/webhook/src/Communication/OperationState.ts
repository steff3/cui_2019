

export enum OperationState {
    Normal,
    CanConnect,
    CanModifyPresent,
    CanRetreiveDeliveryInfo,
    DemandSatisfaction
}