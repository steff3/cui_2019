import * as express from 'express'
import * as bodyParser from 'body-parser'
import * as uuidv4 from 'uuid'

import { dialogflow, Permission} from 'actions-on-google'
import { Suggestions, Confirmation, SimpleResponse, List, DateTime } from 'actions-on-google/dist/service/actionssdk'

import { UserRequest } from './Communication/UserRequest'
import { AssistantResponse, AssistantResponseType } from './Communication/AssistantResponse'
import { OperationState } from './Communication/OperationState'
import { ProductCatalog } from './Model/ProductCatalog'

let isUserKnown: boolean = true


const app = dialogflow({debug: true})

// >>>> fill in these fields accordingly
const user = {
    givenName: '<firstName_from_account>',
    familyName: '<lastName_from_account>',
    id: '<generated UUID>'
}
const users = [user]

let permissionForData: string

const catalog = new ProductCatalog()

let currentUser: any
let currentState: OperationState



app.middleware(conv =>  {
    isUserKnown = ('userId' in conv.user.storage)
    currentState = OperationState.Normal

    if (isUserKnown) {
        currentUser = users.find(e => e.id === conv.user.storage.userId)
    }
    
    const date = conv.user.last.seen
    if (date) {
        console.log(`last seen : ${ date }`)
        const now = new Date()
        if (now.getTime() - date.getTime() > (1000 * 60 * 60 * 24 * 7)) {
            console.log('older than a week')
        } else if (now.getTime() - date.getTime() < (1000 * 60 * 5)) {
            //currentState = OperationState.CanConnect
            console.log('within the last five minute')
        } else if (now.getTime() - date.getTime() < (1000 * 60 * 60)) {
            console.log('within the last hour')
        } else {
            console.log('within last week')
        }
    }

    
})

app.intent('Default Welcome Intent', conv => {
    if (isUserKnown) {
        conv.ask(`Hallo, ${currentUser.givenName}. Wie kann ich Dir heute weiterhelfen?`)
        switch (currentState) {
            case OperationState.Normal:
                conv.ask(new Suggestions(['Geschenk aussuchen']))
                break
            default: 
                break
        }
        
    } else {
        //console.log(conv.intent)
        permissionForData = 'NAME'
        conv.ask('Hallo. Willkommen beim Valentin-Assistant! Es ist einfacher für mich dir behilflich zu sein, wenn ich Deinen Namen weiss.')
        conv.ask(new Permission({
            context: "Wie ist Dein Name?",
            permissions: ['NAME']
        }))
    }
})

app.intent('FindPresentIntent', conv => {
    conv.ask(`Wir haben gerade ${catalog.items.length} Top-Geschenke! Wähle eines aus der Liste aus!`)
    conv.ask(new List({
        title: 'Unsere Top-Geschenke',
        items:{
            '1': {
                title: catalog.items[0].name,
                description: `Bewertung: ${catalog.items[0].rating} Stars`,
                synonyms: [catalog.items[0].name, '1']
            },
            '2': {
                title: catalog.items[1].name,
                description: `Bewertung: ${catalog.items[1].rating} Stars`,
                synonyms: [catalog.items[1].name, '2']
            },
            '3': {
                title: catalog.items[2].name,
                description: `Bewertung: ${catalog.items[2].rating} Stars`,
                synonyms: [catalog.items[2].name, '3']
            },
            '7': {
                title: 'weitere',
                synonyms: ['andere Geschenke', '7']
            }
        }
    }))
})

app.intent('TouchInteractionIntent', conv => {
    const userSelection = conv.contexts.input['actions_intent_option'].parameters

    console.dir(userSelection, [{depth:null}])
    //conv.ask(`${userSelection.text} - sehr gute Wahl!`)
    const selectedIndex = +userSelection.OPTION
    console.log('index selected by user', selectedIndex)
    if (selectedIndex < 7) {
        console.log('selected product')
        conv.ask(new Confirmation(`${userSelection.text} - sehr gute Wahl! Möchtest du dein Geschenk jetzt bestellen?`))
    } else {
        console.log('selected others')
        conv.ask(`Hier die weiteren Produkte:`)
        conv.ask(new List({
            title: 'Weitere Top-Geschenke',
            items:{
                '4': {
                    title: catalog.items[3].name,
                    description: `Bewertung: ${catalog.items[0].rating} Stars`,
                    synonyms: [catalog.items[0].name, '4']
                },
                '5': {
                    title: catalog.items[4].name,
                    description: `Bewertung: ${catalog.items[1].rating} Stars`,
                    synonyms: [catalog.items[1].name, '5']
                }
            }
        }))
    }
    
})


app.intent('UserConfirmationIntent', (conv, params, confirmationGranted) => {
    // Request to GamePersistenceHandler
    if (confirmationGranted) {
         
        const options = {
            prompts: {
                initial: 'Wann soll die Lieferung denn erfolgen?',
                date: 'An welchem Tag Tag?',
                time: 'Um wieviel Uhr?'
            } 
        }
        conv.ask(new DateTime(options))
    }
   
})

app.intent('UserDateTimeIntent', (conv, params, confirmationGranted) => {
    if (confirmationGranted) {
        const timeInfo = conv.contexts.input['actions_intent_datetime'].parameters
        console.dir(timeInfo, [{depth:null}])
        //conv.ask(`ok, wir liefern ${timeInfo.text}`)
        permissionForData = 'DEVICE_COARSE_LOCATION'
        conv.ask(new Permission({
            context: `ok, wir liefern ${timeInfo.text}! ${currentUser.givenName}, wohin sollen wir es senden? Wie ist deine Adress`,
            permissions: ["DEVICE_COARSE_LOCATION"]
        }))
    }
})

app.intent('RepromptIntent', conv => {
    console.log('Reprompt .....')
    const repromptCount = parseInt(conv.arguments.get('REPROMPT_COUNT'))
  if (repromptCount === 0) {
    conv.ask(`What was that?`);
  } else if (repromptCount === 1) {
    conv.ask(`Sorry I didn't catch that. Could you repeat yourself?`)
  } else if (conv.arguments.get('IS_FINAL_REPROMPT')) {
    conv.close(`Okay let's try this again later.`)
  }
})

app.intent('UserPermissionIntent', (conv, params, confirmationGranted) => {
    console.log(params)

    switch (permissionForData) {
        case 'NAME':
            if (isUserKnown) {
                return
            }
            if (confirmationGranted) {
                const {name} = conv.user
                console.log('user\'s name' + name.given + ', ' + name.family)
                conv.close('Hallo ' + name.given)
                const newUserId = uuidv4()
                conv.user.storage['userId'] = newUserId
            } else {
                conv.close('Das ist aber Schade!')
            }
            break
        case 'DEVICE_COARSE_LOCATION':
            break
    }
    
})


app.fallback((conv) => {
    const intent = conv.intent

    conv.close('Ok, thanks!');
})

app.catch((conv, e) => {
    console.log('CATCH .....')
    conv.close('Ok, catched!');
})

const expressApp = express().use(bodyParser.json())

expressApp.post('/fulfillments', app)

expressApp.listen(3000)


// Helper methods

function userRequestFromConv(conv: any, parameters: any = conv.parameters): UserRequest {
    const request = new UserRequest(conv.intent, parameters, conv.input.raw)

    return request
}

function prepareResponse(response: AssistantResponse, conv: any) {
    console.dir(response, {depth:null})
    if (response.isInquiry) {
        if (conv.surface.capabilities.has('actions.capability.SCREEN_OUTPUT')) {
            console.log('has screen output')
            conv.ask(new SimpleResponse({
                speech: response.phrase,
                text: 'Dies wird angezeigt'
            }))
        } else {
            conv.ask(response.phrase)
        }
        switch (response.type) {
            case AssistantResponseType.Suggestions:
                if (conv.screen) {
                    conv.ask(new Suggestions(response.options))
                }
                break
            case AssistantResponseType.Confirmation:
                conv.ask(new Confirmation(response.options[0]))
                break
            default:
                break
        }
    } else {
        conv.close(response.phrase)
    }
}