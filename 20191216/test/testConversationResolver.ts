import { describe } from 'mocha'
import * as chai from 'chai'
import * as chaiAsPromised from 'chai-as-promised'
import * as uuidv4 from 'uuid/v4'
const { expect } = chai

chai.use(chaiAsPromised)

import { ConversationResolver } from '../src/Context/ConversationResolver'
import { KickerContext } from '../src/Context/KickerContext'
import { UserRequest } from '../src/Context/UserRequest'
import { AssistantResponseType } from '../src/Context/AssistantResponse'

let resolver: ConversationResolver
let context: KickerContext

describe.only ('test Conversation Resolver', function() {
    before('Create Context', async function() {
        
    })

    this.beforeEach('Create Search Game Intent', async function() {
        context = new KickerContext(true)
        await context.setup()
        await context.getUserForActionId(null)
        resolver = new ConversationResolver(context, context)
    })

    it ('test handleConversationalTurn Available Games', async function() {
        const request = new UserRequest('GetMatchesIntent', {}, 'Gibt es verfügbare Spiele?')

        const answer = await resolver.handleConversationalTurn(request)

        expect(answer).to.be.an('object')
        expect(answer.isInquiry).to.be.true
        expect(answer.type).to.be.equal(AssistantResponseType.Suggestions)
        expect(answer.options).to.be.eql(['Sturm', 'Abwehr'])
        expect(answer.phrase).to.be.a('string')
        expect(answer.phrase).to.be.equal('Ich habe 3 Spiele gefunden. Auf welcher Position möchtest du gerne spielen?')
    })

    it ('test handleConversationalTurn Choose Position', async function() {
        let request = new UserRequest('GetMatchesIntent', {}, 'Gibt es verfügbare Spiele?')

        let answer = await resolver.handleConversationalTurn(request)
        
        request = new UserRequest('PositionIntent', {position: 'Attack'}, 'Sturm')

        answer = await resolver.handleConversationalTurn(request)

        expect(answer).to.be.an('object')
        expect(answer.isInquiry).to.be.true
        expect(answer.type).to.be.equal(AssistantResponseType.Confirmation)
        expect(answer.phrase).to.be.a('string')
        expect(answer.phrase).to.be.equal('Ich habe 2 Spiele gefunden.')
        expect(answer.options).to.be.eql(['Soll ich dich in eines der Spiele eintragen?'])
    })

    it ('test handleConversationalTurn Add To Game', async function() {
        const gamesBefore = await context.availableGamesOfUser()
        let request = new UserRequest('GetMatchesIntent', {}, 'Gibt es verfügbare Spiele?')

        let answer = await resolver.handleConversationalTurn(request)
        
        request = new UserRequest('PositionIntent', {position: 'Attack'}, 'Sturm')

        answer = await resolver.handleConversationalTurn(request)

        request = new UserRequest('UserConfirmationIntent', {granted: true}, 'Ja')

        answer = await resolver.handleConversationalTurn(request)

        const games = await context.availableGamesOfUser()
        
        expect(answer).to.be.an('object')
        expect(answer.isInquiry).to.be.false
        expect(answer.type).to.be.equal(AssistantResponseType.Plain)
        expect(answer.phrase).to.be.a('string')
        expect(answer.phrase).to.be.equal('Super. Ich habe dich für ein Spiel eingetragen. Ich gebe dir Bescheid, wenn es beginnt.')
        expect(gamesBefore.length).to.be.equal(0)
        expect(games.length).to.be.equal(1)
    })

    it ('test handleConversationalTurn Refused Add To Game', async function() {
        const gamesBefore = await context.availableGamesOfUser()
        let request = new UserRequest('GetMatchesIntent', {}, 'Gibt es verfügbare Spiele?')

        let answer = await resolver.handleConversationalTurn(request)
        
        request = new UserRequest('PositionIntent', {position: 'Attack'}, 'Sturm')

        answer = await resolver.handleConversationalTurn(request)

        request = new UserRequest('UserConfirmationIntent', {granted: false}, 'Nein')

        answer = await resolver.handleConversationalTurn(request)

        const games = await context.availableGamesOfUser()
        
        expect(answer).to.be.an('object')
        expect(answer.isInquiry).to.be.true
        expect(answer.type).to.be.equal(AssistantResponseType.Confirmation)
        expect(answer.phrase).to.be.a('string')
        expect(answer.phrase).to.be.equal('Ok.')
        expect(answer.options).to.be.eql(['Möchtest Du stattdessen lieber ein neues Spiel starten?'])
        expect(gamesBefore.length).to.be.equal(0)
        expect(games.length).to.be.equal(0)
    })

    it ('test handleConversationalTurn Switch to StartNewMatchIntent', async function() {
        const gamesBefore = await context.availableGamesOfUser()
        let request = new UserRequest('GetMatchesIntent', {}, 'Gibt es verfügbare Spiele?')

        let answer = await resolver.handleConversationalTurn(request)
        
        request = new UserRequest('PositionIntent', {position: 'Attack'}, 'Sturm')

        answer = await resolver.handleConversationalTurn(request)

        request = new UserRequest('UserConfirmationIntent', {granted: false}, 'Nein')

        answer = await resolver.handleConversationalTurn(request)

        const games = await context.availableGamesOfUser()

        request = new UserRequest('UserConfirmationIntent', {granted: true}, 'Ja')

        answer = await resolver.handleConversationalTurn(request)
        
        expect(answer).to.be.an('object')
        expect(answer.isInquiry).to.be.true
        expect(answer.type).to.be.equal(AssistantResponseType.Suggestions)
        expect(answer.phrase).to.be.a('string')
        expect(answer.phrase).to.be.equal('Auf welcher Position möchtest du gerne spielen?')
        expect(answer.options).to.be.eql(['Sturm', 'Abwehr'])
        expect(gamesBefore.length).to.be.equal(0)
        expect(games.length).to.be.equal(0)
    })
})