import { describe } from 'mocha'
import * as chai from 'chai'
import * as chaiAsPromised from 'chai-as-promised'
import * as uuidv4 from 'uuid/v4'
const { expect } = chai
chai.use(chaiAsPromised)


import { UserPersistenceHandling } from '../src/Persistence/UserPersistenceHandling'
import { UserMockHandler } from '../src/Persistence/Mock/UserMockHandler'

let currentId: number = 0
let actionId: string = ''
let handler: UserPersistenceHandling


describe ('Test User In Mock', function () {
    before('Create Handler', function() {
        handler = new UserMockHandler()
    })
    
    it ('Create New User', async function() {
        actionId = uuidv4()

        const userId = await handler.createNewUser('Mario', 'Maier', 'SuperMario', actionId)

        expect(userId).to.be.a('number').that.is.greaterThan(0)
        currentId = userId

    })

    it ('Retrieve New User', async function() {
        const user = await handler.retrieveUser(currentId)

        expect(user).to.have.property('firstName', 'Mario')
        expect(user).to.have.property('lastName', 'Maier')
        expect(user).to.have.property('playerName', 'SuperMario')
        expect(user).to.have.property('actionId', actionId)

    })
} )