import { UserPersistenceHandling } from '../Persistence/UserPersistenceHandling'
import { PlayerPersistenceHandling } from '../Persistence/PlayerPersistenceHandling'
import { GamePersistenceHandling } from '../Persistence/GamePersistenceHandling'
import { GameMockHandler } from '../Persistence/Mock/GameMockHandler'
import { UserMockHandler } from '../Persistence/Mock/UserMockHandler'
import { PlayerMockHandler } from '../Persistence/Mock/PlayerMockHandler'
import * as uuidv4 from 'uuid'

import { Game, GameState } from '../Models/game'
import { User } from '../Models/user'
import { Player, PositionType } from '../Models/player'
import { UserRequest } from './UserRequest'
import { AssistantResponse } from './AssistantResponse'
import { ConversationResolver } from './ConversationResolver'

export interface ContextHandling {
    //createUser(firstName: string, lastName: string, playerHandler: string): Promise<number>
    retrieverUserForActiomId(id: string): Promise<boolean>

    handleUserRequest(request: UserRequest): Promise<AssistantResponse>
}

export interface IntentPersistenceHandler {
    availableGamesForUser(): Promise<Game[]>
    addToGame(gameId: number, position: PositionType): Promise<boolean>
    availableGamesOfUser(): Promise<Game[]>
}

export interface PlayerNameProvider {
    playerName(): string
}

export class KickerContext implements ContextHandling, IntentPersistenceHandler, PlayerNameProvider {
    protected userHandler: UserPersistenceHandling
    protected playerHandler: PlayerPersistenceHandling
    protected gameHandler: GamePersistenceHandling
    private currentUser: User
    private resolver: ConversationResolver
    readonly currentUserActionId: string

    helper = new ContextHelper()

    constructor(isTest: boolean = false) {
        this.userHandler = new UserMockHandler()
        const pHandler = new PlayerMockHandler()
        this.gameHandler = new GameMockHandler(pHandler)
        this.playerHandler = pHandler

        this.resolver = new ConversationResolver(this, this)

        this.currentUserActionId = uuidv4()

        if (!isTest) {
            this.setup()
        }
    }

    async getUserForActionId(actionId: string): Promise<boolean> {
        //console.log('CALLING METHOD')
        if (actionId) {
            this.currentUser = await this.userHandler.retrieveUserByActionId(actionId)
        } else {
            this.currentUser = await this.userHandler.retrieveUserByActionId(this.currentUserActionId)
        }
        
        //console.dir(this.currentUser, {depth:null})
        return new Promise<boolean>(res => {
            res(this.currentUser != undefined || this.currentUser != null)
        })
    }

    async setup() {
        await this.helper.setUpUsers(this.userHandler)
        await this.helper.setUpPlayer(this.playerHandler)
        await this.helper.setUpGames(this.gameHandler)
        
        this.userHandler.createNewUser("Steffen", "Blümm", "Steff", this.currentUserActionId)
    }

    availableGamesForUser(): Promise<Game[]> {
        return this.gameHandler.retrieveGamesForUser(this.currentUser)
    }

    async retrieverUserForActiomId(id: string): Promise<boolean> {
        if (id) {
            this.currentUser = await this.userHandler.retrieveUserByActionId(id)
        } else {
            this.currentUser = await this.userHandler.retrieveUserByActionId(this.currentUserActionId)
        }
        

        return new Promise<boolean>(res => {
            res(this.currentUser != undefined && this.currentUser != null)
        })
    }

    async addToGame(gameId: number, position: PositionType): Promise<boolean> {
        const playerId = await this.playerHandler.createNewPlayer(this.currentUser, position)
        const player = await this.playerHandler.playerForId(playerId)
        return this.gameHandler.addPlayer(gameId, player, false)
    }

    async availableGamesOfUser(): Promise<Game[]> {
        return this.gameHandler.retrieveGamesWithUser(this.currentUser, GameState.Pending)
    }

    createUser(firstName: string, lastName: string, playerHandler: string): Promise<number> {
        return this.createUserInMock(firstName, lastName, playerHandler)
    }



    async createUserInMock(firstName: string, lastName: string, playerName: string): Promise<number> {
        const actionId = uuidv4()
        return  this.userHandler.createNewUser(firstName, lastName, playerName, actionId)
    }

    async handleUserRequest(request: UserRequest): Promise<AssistantResponse> {
        return this.resolver.handleConversationalTurn(request)
    }

    //
    //      PLAYER NAME PROVIDER
    //
    playerName(): string {
        if (this.currentUser) {
            return this.currentUser.playerName
        } else {
            return ''
        }
    }
}



class ContextHelper {
    private userHandler: UserPersistenceHandling
    private playerHandler: PlayerPersistenceHandling
    private gameHandler: GamePersistenceHandling

    private users: User[] = []

    async setUpUsers(hander: UserPersistenceHandling) {
        this.userHandler = hander
        await this.createUser('Mario', 'Maier', 'SuperMario')
        await this.createUser('Dieter', 'Bauer', 'Didi')
        await this.createUser('Sara', 'Neuhaus', 'Sasa')
        await this.createUser('Lena', 'Lukatsch', 'Leni')
        await this.createUser('Bubu', 'Loreny', 'Bubu')

        //console.dir(this.users, {depth:null})
    }

    async createUser(firstName: string, lastName: string, playerName: string) {
        const actionId = uuidv4()
        const userId = await this.userHandler.createNewUser(firstName, lastName, playerName, actionId)
        const user = await this.userHandler.retrieveUser(userId)

        this.users.push(user)
    }

    async setUpPlayer(hander: PlayerPersistenceHandling) {
        this.playerHandler = hander


    }

    async setUpGames(handler: GamePersistenceHandling) {
        this.gameHandler = handler

        // create player and game1 for user0
        let playerId = await this.playerHandler.createNewPlayer(this.users[0], PositionType.Attack)
        let player = await this.playerHandler.playerForId(playerId)
        const game1id = await this.gameHandler.createNewGame(player)

        // add user3 to game
        playerId = await this.playerHandler.createNewPlayer(this.users[3], PositionType.Defence)
        player = await this.playerHandler.playerForId(playerId)
        await this.gameHandler.addPlayer(game1id, player, false)

        // create player and game2 for user2
        playerId = await this.playerHandler.createNewPlayer(this.users[2], PositionType.Attack)
        player = await this.playerHandler.playerForId(playerId)
        const game2id = await this.gameHandler.createNewGame(player)

        // add user1 to game
        playerId = await this.playerHandler.createNewPlayer(this.users[1], PositionType.Defence)
        player = await this.playerHandler.playerForId(playerId)
        await this.gameHandler.addPlayer(game2id, player, false)

        // create player and game2 for user2
        playerId = await this.playerHandler.createNewPlayer(this.users[2], PositionType.Attack)
        player = await this.playerHandler.playerForId(playerId)
        const game3id = await this.gameHandler.createNewGame(player)

        // add user1 to game
        playerId = await this.playerHandler.createNewPlayer(this.users[1], PositionType.Attack)
        player = await this.playerHandler.playerForId(playerId)
        await this.gameHandler.addPlayer(game3id, player, false)

        // add user3 to game
        playerId = await this.playerHandler.createNewPlayer(this.users[3], PositionType.Defence)
        player = await this.playerHandler.playerForId(playerId)
        await this.gameHandler.addPlayer(game3id, player, false)
    }

    async createPlayer(user: User, position: PositionType) {
        const playerId = await this.playerHandler.createNewPlayer(user, position)

    }
}