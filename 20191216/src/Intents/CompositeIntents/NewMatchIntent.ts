import { ICompositeIntent, CompositeIntentState } from './ICompositeIntent'
import { IAtomicIntent, AtomicIntentState } from '../AtomicIntents/IAtomicIntent'
import { IAtomicSlotIntent } from '../AtomicIntents/IAtomicSlotIntent'
import { BaseAtomicSlotIntent } from '../AtomicIntents/BaseAtomicSlotIntent'
import { IListenerIntent, ListenerType } from '../ListenerIntents/BaseIntentListener'
import { ListenerCapableIntent } from '../ListenerIntents/ListenerCapableIntent'
import { IntentPersistenceHandler, PlayerNameProvider } from '../../Context/KickerContext'
import { PositionIntent } from '../AtomicIntents/PositionIntent'
import { Player, PositionType } from '../../Models/player'
import { UserConfirmationResultConsumer } from '../ListenerIntents/UserConfirmationIntent'
import { AssistantResponse, AssistantResponseType } from '../../Context/AssistantResponse'
import { UserIntentType, IntentSwitcher } from '../../Context/ConversationResolver'

export class NewMatchIntent extends ListenerCapableIntent implements ICompositeIntent, UserConfirmationResultConsumer {
    private assitantIntent: string = 'CreateMatchIntent'
    private intents: IAtomicIntent[]

    currentIntent: IAtomicIntent
    currentListener: IListenerIntent
    intentSwitcher: IntentSwitcher
    private state: CompositeIntentState

    private persistenceHandler: IntentPersistenceHandler
    private nameProvider: PlayerNameProvider

    private player: Player

    constructor(persistenceHandler: IntentPersistenceHandler, nameProvider: PlayerNameProvider) {
        super()
        this.persistenceHandler = persistenceHandler
        this.state = CompositeIntentState.OutOfScope
        this.nameProvider = nameProvider

        this.player = new Player()

        this.intents = []
        
        this.addListener(ListenerType.UserConfirmation)
        this.intents.push(new PositionIntent(0, null, this, this.player, 'position'))
    }


    //
    //      Implementing UserConfirmationResultConsumer
    //
    async consume(confirmed: boolean): Promise<boolean> {
        let success = false
        switch (confirmed) {
            case true:
                if (this.state == CompositeIntentState.Resolved) {
                    // this is fine .... I will book
                    //success = await this.persistenceHandler.addToGame(this.availableGames[0].id, this.player.position)
                    if (success) {
                        this.state = CompositeIntentState.Confirmed
                    }
                } else if (this.state == CompositeIntentState.Refused) {
                    // switch to New Game Intent
                    success = true
                }
            case false: 
                if (this.state == CompositeIntentState.Resolved) {
                    // ok, ask if user wants to start her own game
                    this.state = CompositeIntentState.Refused
                    success = true
                } else if (this.state == CompositeIntentState.Refused) {
                    // ok, seems I cannot really do something for you right now
                    success = true
                }
                
        }

        return new Promise<boolean>(res => {
            res(success)
        })
    }

    //
    //      Implementing IIntent
    //
    canHandle(nlpIntent: string): boolean {
        this.currentIntent = null
        this.currentListener = null
        let success: boolean = false
        if (nlpIntent === this.assitantIntent) {
            success = true
            this.state = CompositeIntentState.InScope
        } else {
            for (let listener of this.listeners) {
                if (listener.canHandle(nlpIntent)) {
                    this.currentListener = listener
                    break
                }
            }
            if (this.currentListener == null) {
                for (let intent of this.intents) {
                    if (intent.canHandle(nlpIntent)) {
                        this.currentIntent = intent
                        break
                    }
                }
            }
            

            if (this.currentListener || this.currentIntent) {
                if (this.currentIntent) {
                    this.state = CompositeIntentState.InScope
                }
                success = true
            }
        }
        
        return success
    }

    async handle(parameters: any): Promise<boolean> {
        let success = false
        if (this.state != CompositeIntentState.OutOfScope) {
            if (this.currentListener) {
                success = await this.currentListener.handle(parameters)
            }

            if (this.currentIntent) {
                success = await this.currentIntent.handle(parameters)
            }

            //
            //  Extract additional slot parameters
            //
            const others = this.intents.filter(e => ((e instanceof BaseAtomicSlotIntent) && (e.getState() == AtomicIntentState.Open))) as unknown as IAtomicSlotIntent[]

            for (let intent of others) {
                intent.extractSlot(parameters)
            }
        }

        return new Promise<boolean>(res => {
            res(success)
        })
    }


    //
    //      Implementing ICompositeIntent
    //
    canProceed(type: UserIntentType): boolean {
        let success = false
        if (type == UserIntentType.StartNewMatch) {
            success = true
            this.state = CompositeIntentState.InScope
        }

        return success
    }

    prepare(payload: any) {
        const others = this.intents.filter(e => ((e instanceof BaseAtomicSlotIntent) && (e.getState() == AtomicIntentState.Open))) as unknown as IAtomicSlotIntent[]

        for (let intent of others) {
            intent.extractSlot(payload)
        }
    }

    getState(): CompositeIntentState {
        if (this.state == CompositeIntentState.OutOfScope 
            || this.state == CompositeIntentState.Conflicting 
            || this.state == CompositeIntentState.Confirmed
            || this.state == CompositeIntentState.Refused) {
            return this.state
        } else {
            const conflicting = this.intents.find(e => e.getState() == AtomicIntentState.Conflicting)

            if (conflicting) {
                return CompositeIntentState.Conflicting
            }

            const open = this.intents.find(e => e.getState() == AtomicIntentState.Open)
            if (open) {
                return CompositeIntentState.InScope
            }

            this.state = CompositeIntentState.Resolved
            return this.state
        }
    }

    nextInquiry(): AssistantResponse {
        if (this.state == CompositeIntentState.OutOfScope) {
            return new AssistantResponse('')
        } else if (this.state == CompositeIntentState.Confirmed) {
            this.state = CompositeIntentState.OutOfScope
            const message = 'Super. Ich habe dich für ein Spiel eingetragen. Ich gebe dir Bescheid, wenn es beginnt.'
            return new AssistantResponse(message, AssistantResponseType.Plain, false)
        } else if (this.state == CompositeIntentState.Refused) {
            //this.state = CompositeIntentState.OutOfScope
            const message = 'Ok.'
            const nextInquiry = new AssistantResponse(message, AssistantResponseType.Confirmation)
            nextInquiry.options = ['Möchtest Du stattdessen lieber ein neues Spiel starten?']
            return nextInquiry
        }

        if (this.state == CompositeIntentState.Resolved) {
            let message = ''
            this.assignListener(ListenerType.UserConfirmation, this)
            const nextInquiry = new AssistantResponse(message, AssistantResponseType.Confirmation)
            nextInquiry.options = ['Soll ich dich in eines der Spiele eintragen?']
            return nextInquiry
        } else if (this.currentIntent) {
            let confirmation = this.currentIntent.confirmation()

            let nextIntent = this.intents.find(e => e.getState() == AtomicIntentState.Open)
            if (nextIntent) {
                const nextInquiry = nextIntent.inquiry()
                nextInquiry.prepend(confirmation)
                return  nextInquiry
            } else {
                return new AssistantResponse(confirmation)
            }
        } else {
            if (this.state == CompositeIntentState.Conflicting) {
                const nextInquiry = new AssistantResponse('Leider sind im Moment keine Spiele verfügbar!', AssistantResponseType.Confirmation)
                nextInquiry.options = ['Willst Du ein neues Spiel anlegen?']
                return nextInquiry
            } else if (this.state == CompositeIntentState.InScope) {

                const confirmMessage = ''
                const open = this.intents.find(e => e.getState() == AtomicIntentState.Open)
                
                if (open) {
                    const nextInquiry = open.inquiry()
                    nextInquiry.prepend(confirmMessage)
                    return nextInquiry
                } else {
                    return new AssistantResponse(confirmMessage, AssistantResponseType.Plain, false)
                }
                
            }
        }
        
    }
}