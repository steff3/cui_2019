import { IIntent } from '../IIntent'
import { AssistantResponse } from '../../Context/AssistantResponse'
import { UserIntentType, IntentSwitcher } from '../../Context/ConversationResolver'

export enum CompositeIntentState {
    OutOfScope, //
    InScope,
    Inspecting,
    Conflicting,
    Resolved,
    Confirmed,
    Refused
}

export interface ICompositeIntent extends IIntent {
    intentSwitcher: IntentSwitcher
    getState(): CompositeIntentState
    nextInquiry(): AssistantResponse

    canProceed(type: UserIntentType): boolean
    prepare(payload: any)
}