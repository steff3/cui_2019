import { IAtomicIntent, AtomicIntentState } from './IAtomicIntent'
import { BaseAtomicSlotIntent } from './BaseAtomicSlotIntent'
import { ListenerAssigner } from '../ListenerIntents/ListenerCapableIntent'
import { TeamType } from '../../Models/player'
import { AssistantResponse, AssistantResponseType } from '../../Context/AssistantResponse'

export class TeamIntent extends BaseAtomicSlotIntent implements IAtomicIntent {
    readonly index: number
    readonly inquiryMessage: string
    readonly keyword: string

    private team: TeamType
    private listenerAssigner: ListenerAssigner
    private action: (position: TeamType) => boolean

    constructor(index: number, action: (position: TeamType) => boolean, listenerAssigner: ListenerAssigner, target: any, key: string, inquiry?: string) {
        super(key, target)
        this.index = index
        this.listenerAssigner = listenerAssigner
        this.action = action

        if (inquiry) {
            this.inquiryMessage = inquiry
        } else {
            this.inquiryMessage = 'Auf welcher Position möchtest du gerne spielen?'
        }
        
        this.keyword = 'Position'
    }

    //
    //      INTERFACE IINTENT
    //
    canHandle(nlpIntent: string): boolean {
        if (nlpIntent === 'TeamIntent') {
            return true
        }
        return false
    }

    handle(parameters: any): Promise<boolean> {
        let success = false

        if (parameters.position) {
            if (parameters.position === 'BLUE') {
                this.team = TeamType.Blue
            } else if (parameters.position === 'RED') {
                this.team = TeamType.Red
            }
        }
        
        if (this.team != undefined) {
            this.assignmentTarget[this.keyPath] = this.team
            success = true
            this.state = AtomicIntentState.Resolved

            success = this.action(this.team)
        }

        return new Promise<boolean>(res => {
            res(success)
        })
    }

    getState(): AtomicIntentState {
        return this.state
    }

    //
    //      INTERFACE IATOMICSLOTINTENT
    //
    extractSlot(parameters: any) {
        const teamString = parameters.team

        if (teamString) {
            if (teamString === 'BLUE') {
                this.team = TeamType.Blue
            } else if (teamString === 'RED') {
                this.team = TeamType.Red
            }
        }

        if (this.team != undefined) {
            this.assignmentTarget[this.keyPath] = this.team
            this.action(this.team)
            this.state = AtomicIntentState.Resolved
        }
    }

    reset() {
        this.team = undefined
        super.reset()
    }

    //
    //      INTERFACE IATOMICINTENT
    //
    descriptionForTeamType(): string {
        switch (this.team) {
            case TeamType.Blue:
                return 'im blauen Team'
            case TeamType.Red:
                return 'im roten Team'
        }
    }

    confirmation(): string {
        return `Du spielst ${ this.descriptionForTeamType() }.`
    }

    inquiry(): AssistantResponse {
        const generatedInquiry = new AssistantResponse(this.inquiryMessage, AssistantResponseType.Suggestions)
        generatedInquiry.options = ['Blau', 'Rot']

        return generatedInquiry
    }
}