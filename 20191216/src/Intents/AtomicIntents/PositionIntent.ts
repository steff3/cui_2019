import { IAtomicIntent, AtomicIntentState } from './IAtomicIntent'
import { BaseAtomicSlotIntent } from './BaseAtomicSlotIntent'
import { PositionType } from '../../Models/player'
import { ListenerAssigner } from '../ListenerIntents/ListenerCapableIntent'
import { AssistantResponse, AssistantResponseType } from '../../Context/AssistantResponse'

export class PositionIntent extends BaseAtomicSlotIntent implements IAtomicIntent {
    readonly index: number
    readonly inquiryMessage: string
    readonly keyword: string

    private position: PositionType
    private listenerAssigner: ListenerAssigner
    private action: (position: PositionType) => boolean

    constructor(index: number, action: (position: PositionType) => boolean, listenerAssigner: ListenerAssigner, target: any, key: string, inquiry?: string) {
        super(key, target)
        this.index = index
        this.listenerAssigner = listenerAssigner
        this.action = action

        if (inquiry) {
            this.inquiryMessage = inquiry
        } else {
            this.inquiryMessage = 'Auf welcher Position möchtest du gerne spielen?'
        }
        
        this.keyword = 'Position'
    }

    descriptionForPositionType(): string {
        switch (this.position) {
            case PositionType.Attack:
                return 'im Sturm'
            case PositionType.Defence:
                return 'in der Abwehr'
        }
    }

    //
    //      INTERFACE IINTENT
    //
    canHandle(nlpIntent: string): boolean {
        if (nlpIntent === 'PositionIntent') {
            return true
        }
        return false
    }

    handle(parameters: any): Promise<boolean> {
        let success = false

        if (parameters.position) {
            if (parameters.position === 'Attack') {
                this.position = PositionType.Attack
            } else if (parameters.position === 'Defense') {
                this.position = PositionType.Defence
            }
        }
        
        if (this.position != undefined) {
            this.assignmentTarget[this.keyPath] = this.position
            success = true
            this.state = AtomicIntentState.Resolved
            if (this.action) {
                success = this.action(this.position)
            }
        }

        return new Promise<boolean>(res => {
            res(success)
        })
    }

    getState(): AtomicIntentState {
        return this.state
    }

    //
    //      INTERFACE IATOMICSLOTINTENT
    //
    extractSlot(parameters: any) {
        const positionString = parameters.position

        if (positionString) {
            if (positionString === 'Attack') {
                this.position = PositionType.Attack
            } else if (positionString === 'Defense') {
                this.position = PositionType.Defence
            }
        }

        if (this.position != undefined) {
            this.assignmentTarget[this.keyPath] = this.position
            if (this.action) {
                this.action(this.position)
            }
            this.state = AtomicIntentState.Resolved
        }
    }

    reset() {
        this.position = undefined
        super.reset()
    }

    //
    //      INTERFACE IATOMICINTENT
    //
    confirmation(): string {
        return `Du spielst ${ this.descriptionForPositionType() }.`
    }

    inquiry(): AssistantResponse {
        const generatedInquiry = new AssistantResponse(this.inquiryMessage, AssistantResponseType.Suggestions)
        generatedInquiry.options = ['Sturm', 'Abwehr']

        return generatedInquiry
    }
}