import { ListenerType, IListenerIntent } from './BaseIntentListener'
import { TouchInteractionIntent } from "./TouchInteractionIntent"
import { UserConfirmationIntent } from "./UserConfirmationIntent"

export interface ListenerAssigner {
    assignListener(type: ListenerType, consumer: any): boolean
}

export abstract class ListenerCapableIntent implements ListenerAssigner {
    protected listeners: IListenerIntent[]

    constructor() {
        this.listeners = []
    }

    protected addListener(type: ListenerType) {
        if (this.listeners.find(e => e.isListenerOfType(type)!= undefined)) {
            return
        }
        switch (type) {
            case ListenerType.TouchInteraction:
                this.listeners.push(new TouchInteractionIntent())
                break
            case ListenerType.UserConfirmation:
                this.listeners.push(new UserConfirmationIntent())
                break

        }
        
    }

    assignListener(type: ListenerType, consumer: any): boolean {
        let success = false
        const matechedListener = this.listeners.find(e => e.isListenerOfType(type))

        if (matechedListener) {
            switch (type) {
                case ListenerType.TouchInteraction:
                    (matechedListener as TouchInteractionIntent).setConsumer(consumer)
                    break
                case ListenerType.UserConfirmation:
                    (matechedListener as UserConfirmationIntent).setConsumer(consumer)
                    break
            }

            success = true
        }

        return success
    }
}