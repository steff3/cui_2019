import { BasePersistenceHandler, TableType } from './BasePersistenceHandler'
import { PlayerPersistenceHandling } from './PlayerPersistenceHandling'
import { Player, PositionType, TeamType } from '../Models/player'
import { User } from '../Models/user'


export class PlayerPersistenceHandler extends BasePersistenceHandler implements PlayerPersistenceHandling {

    createNewPlayer(user: User, position: PositionType, team?: TeamType): Promise<number> {
        const data: any = []
        data.push(user.id)
        data.push(user.playerName)
        if (position == PositionType.Attack) {
            data.push('ATTACK')
        } else {
            data.push('DEFENSE')
        }
        
        let newPlayerId: number = -1

        const client = this.connectClient();
        return new Promise<number>( res => {
            return this.createPlayerTable(client)
                .then( () => {
                    return this.insertPlayer(client, data)
                })
                .then(res => {
                    newPlayerId = res.rows[0].id
                })
                .catch(e => {
                    console.error(e.stack)
                })
                .finally(() => {
                    this.disconnectClient(client)
                    res(newPlayerId)
                })
        })
    }    
    
    updateExistingPlayer(id: number, position?: PositionType, team?: TeamType): Promise<boolean> {
        throw new Error("Method not implemented.");
    }

    playerForId(id: number): Promise<Player> {
        let retrievedPlayer: Player

        const client = this.connectClient();
        return new Promise<Player>( res => {
            return this.createPlayerTable(client)
                .then(() => {
                    return this.selectPlayer(client, id)
                })
                .then(res => {
                    if (res.rowCount > 0) {
                        retrievedPlayer = this.playerFromData(res.rows[0])
                    }
                })
                .catch(e => {
                    console.error(e.stack)
                })
                .finally(() => {
                    this.disconnectClient(client)
                    res(retrievedPlayer)
                })
        })
    }

    playersOfUser(user: User): Promise<Player[]> {
        let retrievedPlayers: Player[] = []

        const client = this.connectClient();
        return new Promise<Player[]>( res => {
            return this.createPlayerTable(client)
                .then(() => {
                    return this.selectPlayers(client, user.id)
                })
                .then(res => {
                    if (res.rowCount > 0) {
                        res.rows.forEach(element => {
                            retrievedPlayers.push(this.playerFromData(element))
                        });
                    }
                })
                .catch(e => {
                    console.error(e.stack)
                })
                .finally(() => {
                    this.disconnectClient(client)
                    res(retrievedPlayers)
                })
        })
    }

    positionsOfUser(user: User):Promise<PositionType[]> {
        let retrievedPositions: PositionType[] = []

        const client = this.connectClient();
        return new Promise<PositionType[]>( res => {
            return this.createPlayerTable(client)
                .then(() => {
                    return this.selectChosenPositions(client, user.id)
                })
                .then(res => {
                    if (res.rowCount > 0) {
                        res.rows.forEach(element => {
                            if (element.position === 'ATTACK') {
                                retrievedPositions.push(PositionType.Attack)
                            } else if (element.position === 'DEFENSE') {
                                retrievedPositions.push(PositionType.Defence)
                            }
                        });
                    }
                })
                .catch(e => {
                    console.error(e.stack)
                })
                .finally(() => {
                    this.disconnectClient(client)
                    res(retrievedPositions)
                })
        })
    }

    deleteExistingPlayer(id: number): Promise<boolean> {
        let successfully: boolean = true

        const client = this.connectClient()
        return new Promise<boolean>(res => {
            return this.createPlayerTable(client)
                .then(() => {
                    return this.deletePlayer(client, id)
                })
                .catch( e => {
                    console.error(e.stack)
                    successfully = false
                }).finally(() => {
                    this.disconnectClient(client)
                    res(successfully)
                })
        })
    }

    ///
    ///     DATABASE OPERATIONS
    ///

    private createPlayerTable(client: any): Promise<any> {
        const queryText = `CREATE TABLE IF NOT EXISTS ${ TableType.Player } (
            id SERIAL PRIMARY KEY,
            user_id INT REFERENCES ${ TableType.User } (id) ON DELETE SET NULL,
            name VARCHAR (50) NOT NULL,
            position VARCHAR (10),
            team VARCHAR (10));`

        return client.query(queryText)
    }

    private insertPlayer(client: any, data: any): Promise<any> {
        const query = {
            text: `INSERT INTO ${ TableType.Player } (
                user_id,
                name,
                position)
                VALUES($1, $2, $3)
                RETURNING id`,
            values: data
        }

        return client.query(query)
    }

    private selectPlayers(client: any, userId: number): Promise<any> {
        const query = `SELECT *
                        FROM ${ TableType.Player }
                        WHERE user_id = ${ userId }`

        return client.query(query)
    }

    private selectPlayer(client: any, id: number): Promise<any> {
        const query = `SELECT *
                        FROM ${ TableType.Player }
                        WHERE id = ${ id }`

        return client.query(query)
    }

    private updatePlayer(client: any, id: number, field: string, data: any): Promise<any> {
        return this.updateItem(client, id, field, data, TableType.Player)
    }

    private deletePlayer(client: any, id: number): Promise<any> {
        return this.deleteItem(client, id, TableType.Player)
    }

    private selectChosenPositions(client: any, userId: number): Promise<any> {
        const query = `SELECT position
                        FROM ${ TableType.Player }
                        WHERE user_id = ${ userId }`

        return client.query(query)
    }


    ///
    ///     DATA CONVERTERS
    ///

    private playerFromData(data: any): Player {
        let player = new Player()
        player.id = data.id
        player.userId = data.user_id
        player.name = data.name
        
        if (data.position) {
            if (data.position === 'ATTACK') {
                player.position = PositionType.Attack
            } else {
                player.position = PositionType.Defence
            }
        }

        return player
    }
}