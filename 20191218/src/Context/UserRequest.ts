


export class UserRequest {
    readonly intent: string
    readonly parameters: any
    readonly utterance: string

    constructor(intent: string, parameters: any, utterance: string) {
        this.intent = intent
        this.parameters = parameters
        this.utterance = utterance
    }
}