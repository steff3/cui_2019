

export enum AssistantResponseType {
    Plain,
    List,
    Confirmation,
    Suggestions
}

export class AssistantResponse {
    type: AssistantResponseType
    phrase: string
    isInquiry: boolean
    options: string[]

    constructor(phrase: string, type: AssistantResponseType = AssistantResponseType.Plain, isInquiry: boolean = true) {
        this.type = type
        this.phrase = phrase
        this.isInquiry = isInquiry
    }

    prepend(message: string) {
        if (message.length > 0) {
            this.phrase = `${ message } ${ this.phrase }`
        }
    }

    append(message: string) {
        if (message.length > 0) {
            this.phrase = `${ this.phrase } ${ message }`
        }
    }

    personalizePhrase(name: string, probability: number = 35) {
        if (probability == 0) {
            return
        } else if (probability == 100) {
            this.addName(name)
        } else {
            const rand = Math.floor(Math.random() * 100) + 0
            if (rand <= probability) {
                this.addName(name)
            }
        }
    }

    private addName(name: string) {
        this.phrase = `${ name }, ${ this.phrase.charAt(0).toLocaleLowerCase() }${ this.phrase.slice(1) }`
    }
}