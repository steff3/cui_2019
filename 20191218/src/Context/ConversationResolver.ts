import { UserRequest } from "./UserRequest"
import { AssistantResponse } from './AssistantResponse'
import { ICompositeIntent, CompositeIntentState } from "../Intents/CompositeIntents/ICompositeIntent"
import { SearchGameIntent } from "../Intents/CompositeIntents/SearchGamesIntent"
import { NewMatchIntent } from "../Intents/CompositeIntents/NewMatchIntent"
import { IntentPersistenceHandler, PlayerNameProvider } from "./KickerContext"


export enum UserIntentType {
    JoinExistingMatch,
    StartNewMatch
}

export interface IntentSwitcher {
    switchToIntent(type: UserIntentType, payload?: any): boolean
}


export class ConversationResolver implements IntentSwitcher {
    private sessionId: string

    private openConversationHandlers: ICompositeIntent[] = []
    private conversationHandlers: ICompositeIntent[]
    private currentConversation: ICompositeIntent

    constructor(persistenceHandler: IntentPersistenceHandler, nameProvider: PlayerNameProvider) {
        this.conversationHandlers = ResolveHelper.getConversations(persistenceHandler, nameProvider, this)
    }

    async handleConversationalTurn(request: UserRequest): Promise<AssistantResponse> {
        let matchedHandler: ICompositeIntent = null

        let answer: AssistantResponse

        if (this.currentConversation && this.currentConversation.canHandle(request.intent)) {
            // console.log('found for current')
            matchedHandler = this.currentConversation
        }
        
        if ((!matchedHandler) && this.openConversationHandlers.length > 0) {
            matchedHandler = this.openConversationHandlers.find(e => e.canHandle(request.intent))
        }

        if ((!matchedHandler) && this.conversationHandlers.length > 0) {
            matchedHandler = this.conversationHandlers.find(e => e.canHandle(request.intent))
        }

        if (matchedHandler) {
            const previousHander = this.currentConversation
            this.currentConversation = matchedHandler
            let success = await this.currentConversation.handle(request.parameters)
            // console.log('success on matched handler', success)
            if (success) {
                // console.log('state on matched handler', matchedHandler.getState())
                if (this.currentConversation.getState() != CompositeIntentState.OutOfScope) {
                    answer = this.currentConversation.nextInquiry()

                    if(this.currentConversation.getState() == CompositeIntentState.OutOfScope && !answer.isInquiry) {
                        answer.append('Kann ich noch etwas für dich tun?')
                        answer.isInquiry = true
                        this.currentConversation = null
                    }
                } else {
                    answer = new AssistantResponse('Ok, darüber muss ich nachdenken (2)')
                    this.currentConversation = previousHander
                }
            }
        } else {
            // console.log("Completely Off")
            answer = new AssistantResponse('Ok, darüber muss ich nachdenken (3)')
        }

        return new Promise<AssistantResponse>(res => {
            res(answer)
        })
    }

    switchToIntent(type: UserIntentType, payload?: any): boolean {
        let success = false

        const matchedHandler = this.conversationHandlers.find(e => e.canProceed(type))

        if (matchedHandler) {
            this.currentConversation = matchedHandler
            if (payload) {
                matchedHandler.prepare(payload)
            }
            success = true
        }

        return success
    }
}

abstract class ResolveHelper {
    static getConversations(persistenceHandler: IntentPersistenceHandler, nameProvider: PlayerNameProvider, switcher: IntentSwitcher): ICompositeIntent[] {
        let handlers: ICompositeIntent[] = []

        handlers.push(this.provideSearchGamesIntent(persistenceHandler, nameProvider, switcher))
        handlers.push(this.provideStartNewGameIntent(persistenceHandler, nameProvider, switcher))
        //console.log(`(3) ${handlers.length}`)
        return handlers
    }

    static provideSearchGamesIntent(persistenceHandler: IntentPersistenceHandler, nameProvider: PlayerNameProvider, switcher: IntentSwitcher): SearchGameIntent {
        const intent = new SearchGameIntent(persistenceHandler, nameProvider, switcher)

        return intent
    }

    static provideStartNewGameIntent(persistenceHandler: IntentPersistenceHandler, nameProvider: PlayerNameProvider, switcher: IntentSwitcher): NewMatchIntent {
        const intent = new NewMatchIntent(persistenceHandler, nameProvider, switcher)

        return intent
    }
}