

import * as express from 'express'
import * as bodyParser from 'body-parser'
import * as uuidv4 from 'uuid'

import { dialogflow, Permission} from 'actions-on-google'
import { ContextHandling, KickerContext } from './Context/KickerContext'
import { User } from './Models/user'
import { Game } from './Models/game'
import { Player, PositionType } from './Models/player'
import { UserRequest } from './Context/UserRequest'
import { AssistantResponse, AssistantResponseType } from './Context/AssistantResponse'
import { convert, Suggestions, Confirmation } from 'actions-on-google/dist/service/actionssdk'

let isUserKnown: boolean = true

const app = dialogflow({debug: true})

const context: ContextHandling = new KickerContext()
let currentUser: User
let retrievedGames: Game[]


app.middleware(conv =>  {
    //isUserKnown = ('userId' in conv.user.storage)
    isUserKnown = true
    
    new Promise(() => {
        return context.retrieverUserForActiomId(null)
    })
})

app.intent('Default Welcome Intent', conv => {
    if (isUserKnown) {
        conv.ask('Hallo. Wie geht es Dir heute?')
    } else {
        console.log(conv.intent)
        conv.ask('Hallo. Wer bist Du denn?')
        conv.ask(new Permission({
            context: "Wie ist Dein Name?",
            permissions: ["NAME"]
        }))
    }
})

app.intent('GetMatchesIntent', (conv) => {
    // Request to GamePersistenceHandler
   const request = userRequestFromConv(conv)

   return context.handleUserRequest(request).then(res => {
        prepareResponse(res, conv)
        return conv
   })
})

app.intent('PositionIntent', (conv) => {
    // Request to GamePersistenceHandler
    const request = userRequestFromConv(conv)

   return context.handleUserRequest(request).then(res => {
        prepareResponse(res, conv)
        return conv
   })
})

app.intent('UserConfirmationIntent', (conv, params, confirmationGranted) => {
    // Request to GamePersistenceHandler
    const request = userRequestFromConv(conv, {granted: confirmationGranted})

   return context.handleUserRequest(request).then(res => {
        prepareResponse(res, conv)
        return conv
   })
})

app.intent('NegativeConfirmationIntent', (conv) => {
    // Request to GamePersistenceHandler
    conv.ask("")
})

app.intent('PositiveConfirmationIntent', (conv) => {
    // Request to GamePersistenceHandler
    conv.ask("")
})

app.intent('UserPermissionIntent', (conv, params, confirmationGranted) => {
    console.log(params)
    const {name} = conv.user
    console.log('user\'s name' + name.given + ', ' + name.family)

    if (confirmationGranted) {
        conv.close('Hallo ' + name.given)
        const newUserId = uuidv4()
        conv.user.storage['userId'] = newUserId
    } else {
        conv.close('Das ist aber Schade!')
    }
})


app.fallback((conv) => {
    const intent = conv.intent

    conv.close('Ok, thanks!');
})

const expressApp = express().use(bodyParser.json())

expressApp.post('/fulfillments', app)

expressApp.listen(3000)


// Helper methods

function userRequestFromConv(conv: any, parameters: any = conv.parameters): UserRequest {
    const request = new UserRequest(conv.intent, parameters, conv.input.raw)
    console.dir(request, {depth:null})
    return request
}

function prepareResponse(response: AssistantResponse, conv: any) {
    console.dir(response, {depth:null})
    if (response.isInquiry) {
        conv.ask(response.phrase)
        switch (response.type) {
            case AssistantResponseType.Suggestions:
                if (conv.screen) {
                    conv.ask(new Suggestions(response.options))
                }
                break
            case AssistantResponseType.Confirmation:
                conv.ask(new Confirmation(response.options[0]))
                break
            default:
                break
        }
    } else {
        conv.close(response.phrase)
    }
}