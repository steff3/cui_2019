import { PlayerPersistenceHandling } from "../PlayerPersistenceHandling"
import { GamePersistenceHandling } from "../GamePersistenceHandling"
import { Player, TeamType } from '../../Models/player'
import { Game, GameState, GameResult } from '../../Models/game'
import { User } from '../../Models/user'
import { PlayerMockHandler } from "./PlayerMockHandler"


export class GameMockHandler implements GamePersistenceHandling {
    private games: Game[] = []
    private playerHandler: PlayerMockHandler
    private numberOfDeleted = 0

    constructor(playerHandler: PlayerMockHandler) {
        this.playerHandler = playerHandler
    }

    async createNewGame(player: Player): Promise<number> {
        const newGame = new Game()
        newGame.id = this.games.length + this.numberOfDeleted + 1
        newGame.players = []
        const copyOfPlayer = await this.playerHandler.playerForId(player.id)
        //console.dir(copyOfPlayer, {depth:null})
        newGame.players.push(copyOfPlayer)
        const now = new Date()
        newGame.createdAt = now
        newGame.lastChanged = now
        this.games.push(newGame)

        return new Promise<number>(res => res(newGame.id))
    }

    async addPlayer(id: number, player: Player, allowsSingle: boolean = false): Promise<boolean> {
        let success = false

        const index = this.games.findIndex(e => e.id == id)

        if (index >= 0) {
            const thatGame = this.games[index]
            
            if (thatGame.players.length < 4 && thatGame.canAddPlayer(player) && thatGame.players.findIndex(e => e.userId == player.userId) == -1) {
                const copyOfPlayer = await this.playerHandler.playerForId(player.id)
                thatGame.players.push(copyOfPlayer)
                thatGame.lastChanged = new Date()
                success = true
            } 
        }

        return new Promise<boolean>(res => res(success))
    }

    async retrieveGames(state: GameState = GameState.All): Promise<Game[]> {
        const retrievedGames: Game[] = []

        switch (state) {
            case GameState.All:
                for (let game of this.games) {
                    const index = this.games.findIndex(e => e.id == game.id)
                    const copyGame = await this.copyOfGame(index)
                    retrievedGames.push(copyGame)
                }
                break
            case GameState.Filled:
                for (let game of this.games) {
                    if (game.players.length == 4) {
                        const index = this.games.findIndex(e => e.id == game.id)
                        const copyGame = await this.copyOfGame(index)
                        retrievedGames.push(copyGame)
                    }
                }
                break
            case GameState.Pending:
                for (let game of this.games) {
                    if (game.players.length < 4) {
                        const index = this.games.findIndex(e => e.id == game.id)
                        const copyGame = await this.copyOfGame(index)
                        retrievedGames.push(copyGame)
                    }
                }
                break
            case GameState.Pending:
                for (let game of this.games) {
                    if (game.result.isComplete()) {
                        const index = this.games.findIndex(e => e.id == game.id)
                        const copyGame = await this.copyOfGame(index)
                        retrievedGames.push(copyGame)
                    }
                }
                break

        }

        return new Promise<Game[]>(res => res(retrievedGames))
    }

    async retrieveGame(id: number): Promise<Game> {
        let retrievedGame: Game

        const index = this.games.findIndex(e => e.id == id)

        if (index >= 0) {
            const copyGame = await this.copyOfGame(index)
            retrievedGame = copyGame
        }

        return new Promise<Game>(res => res(retrievedGame))
    }

    async retrieveGamesWithUser(user: User, state: GameState = GameState.All): Promise<Game[]> {
        const retrievedGames: Game[] = []
        const filteredGames = this.games.filter(element => {
            return (element.players.filter(item => item.userId == user.id).length == 1)
        })
        switch (state) {
            case GameState.All:
                for (let game of filteredGames) {
                    const index = this.games.findIndex(e => e.id == game.id)
                    const copyGame = await this.copyOfGame(index)
                    retrievedGames.push(copyGame)
                }
                break
            case GameState.Filled:
                for (let game of filteredGames) {
                    if (game.players.length == 4) {
                        const index = this.games.findIndex(e => e.id == game.id)
                        const copyGame = await this.copyOfGame(index)
                        retrievedGames.push(copyGame)
                    }
                }
                break
            case GameState.Pending:
                for (let game of filteredGames) {
                    if (game.players.length < 4) {
                        const index = this.games.findIndex(e => e.id == game.id)
                        const copyGame = await this.copyOfGame(index)
                        retrievedGames.push(copyGame)
                    }
                }
                break
            case GameState.Learnable:
                for (let game of filteredGames) {
                    if (game.result.isComplete()) {
                        const index = this.games.findIndex(e => e.id == game.id)
                        const copyGame = await this.copyOfGame(index)
                        retrievedGames.push(copyGame)
                    }
                }
                break

        }

        return new Promise<Game[]>(res => res(retrievedGames))
    }

    async retrieveGamesForUser(user: User): Promise<Game[]> {
        const retrievedGames: Game[] = []
        const filteredGames = this.games.filter(element => {
            return (element.players.length < 4 && element.players.filter(item => item.userId == user.id).length == 0)
        })

        for (let game of filteredGames) {
            const index = this.games.findIndex(e => e.id == game.id)
            const copyGame = await this.copyOfGame(index)
            retrievedGames.push(copyGame)
        }

        return new Promise<Game[]>(res => res(retrievedGames))
    }

    setResult(id: number, goals: GameResult): Promise<boolean> {
        let success = false

        if (goals.isComplete()) {
            const index = this.games.findIndex(e => e.id == id)

            if (index >= 0) {
                this.games[index].result = goals
                success = true
            }
        }

        return new Promise<boolean>(res => res(success))
    }

    deleteExistingGame(id: number): Promise<boolean>  {
        let success = false

        const index = this.games.findIndex(e => e.id == id)

        if (index >= 0) {
            const deletedGames = this.games.splice(index, 1)
            if (deletedGames.length == 1 && deletedGames[0].id == id) {
                this.numberOfDeleted++
                success = true
            }
        }

        return new Promise<boolean>(res => res(success))
    }


    private async copyOfGame(index: number): Promise<Game> {
        const copiedGame = new Game()
        
        copiedGame.id = this.games[index].id
        copiedGame.createdAt = this.games[index].createdAt
        copiedGame.lastChanged = this.games[index].lastChanged
        copiedGame.players = []

        for (let player of this.games[index].players) {
            const copyOfPlayer = await this.playerHandler.playerForId(player.id)
            copiedGame.players.push(copyOfPlayer)
        }

        if (this.games[index].result) {
            const copiedResult = new GameResult()
            copiedResult.setNumberOfGoals(TeamType.Blue, this.games[index].result.goalsForTeam(TeamType.Blue))
            copiedResult.setNumberOfGoals(TeamType.Red, this.games[index].result.goalsForTeam(TeamType.Red))
            copiedGame.result = copiedResult
        }

        return copiedGame
    }
}