import { ICompositeIntent, CompositeIntentState } from './ICompositeIntent'
import { BaseCompositeIntent } from './BaseCompositeIntent'
import { AtomicIntentState } from '../AtomicIntents/IAtomicIntent'
import { IAtomicSlotIntent } from '../AtomicIntents/IAtomicSlotIntent'
import { BaseAtomicSlotIntent } from '../AtomicIntents/BaseAtomicSlotIntent'
import { ListenerType } from '../ListenerIntents/BaseIntentListener'
import { IntentPersistenceHandler, PlayerNameProvider } from '../../Context/KickerContext'
import { PositionIntent } from '../AtomicIntents/PositionIntent'
import { Player } from '../../Models/player'
import { Game } from '../../Models/game'
import { UserConfirmationResultConsumer } from '../ListenerIntents/UserConfirmationIntent'
import { AssistantResponse, AssistantResponseType } from '../../Context/AssistantResponse'
import { UserIntentType, IntentSwitcher } from '../../Context/ConversationResolver'

export class NewMatchIntent extends BaseCompositeIntent implements ICompositeIntent, UserConfirmationResultConsumer {

    private persistenceHandler: IntentPersistenceHandler
    private nameProvider: PlayerNameProvider

    private player: Player

    constructor(persistenceHandler: IntentPersistenceHandler, nameProvider: PlayerNameProvider, intentSwitcher: IntentSwitcher) {
        super('CreateMatchIntent', UserIntentType.StartNewMatch, intentSwitcher)
        this.persistenceHandler = persistenceHandler
        this.nameProvider = nameProvider

        this.player = new Player()

        const action = async (): Promise<boolean> => {
            let success = false
            if (this.intents.filter(e => e.getState() == AtomicIntentState.Open).length == 0 && this.isPreConfirmed) {
                success = false
                if (await this.createNewGame()) {
                    this.state = CompositeIntentState.Confirmed
                    success = true
                }
            }
            return new Promise<boolean>(res => {
                res(success)
            })
        }
        
        this.addListener(ListenerType.UserConfirmation)
        this.intents.push(new PositionIntent(0, action, this, this.player, 'position'))
    }


    //
    //      Implementing UserConfirmationResultConsumer
    //
    async consume(confirmed: boolean): Promise<boolean> {
        let success = false
        switch (confirmed) {
            case true:
                if (this.state == CompositeIntentState.Resolved) {
                    // this is fine .... I will book
                    //success = await this.persistenceHandler.addToGame(this.availableGames[0].id, this.player.position)
                    if (success) {
                        this.state = CompositeIntentState.Confirmed
                    }
                } else if (this.state == CompositeIntentState.Refused) {
                    // switch to New Game Intent
                    success = true
                }
            case false: 
                if (this.state == CompositeIntentState.Resolved) {
                    // ok, ask if user wants to start her own game
                    this.state = CompositeIntentState.Refused
                    success = true
                } else if (this.state == CompositeIntentState.Refused) {
                    // ok, seems I cannot really do something for you right now
                    success = true
                }
                
        }

        return new Promise<boolean>(res => {
            res(success)
        })
    }

    //
    //      Implementing IIntent
    //


    //
    //      Implementing ICompositeIntent
    //
    async prepare(payload: any): Promise<boolean> {
        let success = true
        const others = this.intents.filter(e => ((e instanceof BaseAtomicSlotIntent) && (e.getState() == AtomicIntentState.Open))) as unknown as IAtomicSlotIntent[]

        for (let intent of others) {
            intent.extractSlot(payload)
        }

        return new Promise<boolean>(res => {
            res(success)
        })
    }

    nextInquiry(): AssistantResponse {
        if (this.state == CompositeIntentState.OutOfScope) {
            return new AssistantResponse('')
        } else if (this.state == CompositeIntentState.Confirmed) {
            this.state = CompositeIntentState.OutOfScope
            this.reset()
            let message = 'Ich habe ein neues Spiel für dich anlegt. Ich gebe dir Bescheid, wenn es beginnt.'
            return new AssistantResponse(message, AssistantResponseType.Plain, false)
        } else if (this.state == CompositeIntentState.Refused) {
            //this.state = CompositeIntentState.OutOfScope
            const message = 'Ok.'
            const nextInquiry = new AssistantResponse(message, AssistantResponseType.Confirmation)
            nextInquiry.options = ['Möchtest Du stattdessen lieber ein neues Spiel starten?']
            return nextInquiry
        }

        if (this.state == CompositeIntentState.Resolved) {
            let nextInquiry
            if (this.isPreConfirmed) {
                this.state = CompositeIntentState.OutOfScope
                let message = 'Ich habe ein neues Spiel für dich anlegt. Ich gebe dir Bescheid, wenn es beginnt.'
                nextInquiry = new AssistantResponse(message, AssistantResponseType.Plain, false)
            } else {
                let message = ''
                this.assignListener(ListenerType.UserConfirmation, this)
                nextInquiry = new AssistantResponse(message, AssistantResponseType.Confirmation)
                nextInquiry.options = ['Soll ich dich in eines der Spiele eintragen?']
            }
            
            return nextInquiry
        } else if (this.currentIntent) {
            let confirmation = this.currentIntent.confirmation()

            let nextIntent = this.intents.find(e => e.getState() == AtomicIntentState.Open)
            if (nextIntent) {
                const nextInquiry = nextIntent.inquiry()
                nextInquiry.prepend(confirmation)
                return  nextInquiry
            } else {
                return new AssistantResponse(confirmation)
            }
        } else {
            if (this.state == CompositeIntentState.Conflicting) {
                const nextInquiry = new AssistantResponse('Leider sind im Moment keine Spiele verfügbar!', AssistantResponseType.Confirmation)
                nextInquiry.options = ['Willst Du ein neues Spiel anlegen?']
                return nextInquiry
            } else if (this.state == CompositeIntentState.InScope) {

                const confirmMessage = ''
                const open = this.intents.find(e => e.getState() == AtomicIntentState.Open)
                
                if (open) {
                    const nextInquiry = open.inquiry()
                    nextInquiry.prepend(confirmMessage)
                    return nextInquiry
                } else {
                    return new AssistantResponse(confirmMessage, AssistantResponseType.Plain, false)
                }
                
            }
        }
    }


    reset() {
        super.reset()
        this.player = new Player()
    }

    //
    //      Logic
    //
    async createNewGame(): Promise<boolean> {
        return this.persistenceHandler.createNewGameWithUser(this.player.position)
    }
}