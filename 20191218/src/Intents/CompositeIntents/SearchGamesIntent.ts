import { ICompositeIntent, CompositeIntentState } from './ICompositeIntent'
import { BaseCompositeIntent } from './BaseCompositeIntent'
import { AtomicIntentState } from '../AtomicIntents/IAtomicIntent'
import { IAtomicSlotIntent } from '../AtomicIntents/IAtomicSlotIntent'
import { BaseAtomicSlotIntent } from '../AtomicIntents/BaseAtomicSlotIntent'
import { ListenerType } from '../ListenerIntents/BaseIntentListener'
import { IntentPersistenceHandler, PlayerNameProvider } from '../../Context/KickerContext'
import { PositionIntent } from '../AtomicIntents/PositionIntent'
import { Game } from '../../Models/game'
import { Player, PositionType } from '../../Models/player'
import { UserConfirmationResultConsumer } from '../ListenerIntents/UserConfirmationIntent'
import { AssistantResponse, AssistantResponseType } from '../../Context/AssistantResponse'
import { UserIntentType, IntentSwitcher } from '../../Context/ConversationResolver'


export class SearchGameIntent extends BaseCompositeIntent implements ICompositeIntent, UserConfirmationResultConsumer {
    private availableGames: Game[]


    private persistenceHandler: IntentPersistenceHandler
    private nameProvider: PlayerNameProvider

    private player: Player

    constructor(persistenceHandler: IntentPersistenceHandler, nameProvider: PlayerNameProvider, intentSwitcher: IntentSwitcher) {
        super('GetMatchesIntent', UserIntentType.JoinExistingMatch, intentSwitcher)

        this.persistenceHandler = persistenceHandler
        this.nameProvider = nameProvider

        this.player = new Player()

        const action = (): Promise<boolean> => {
            let success = false
            if (this.availableGames) {
                //console.log(`number of games before filter :: ${this.availableGames.length}`)
                this.availableGames = this.availableGames.filter(e => e.canAddPlayerOnPosition(this.player.position))
                //console.log(`number of games after filter :: ${this.availableGames.length}`)
                success = true
            }
            return new Promise<boolean>(res => {
                res(success)
            })
        }
        this.addListener(ListenerType.UserConfirmation)
        this.intents.push(new PositionIntent(0, action, this, this.player, 'position'))
    }

    //
    //      Implementing UserConfirmationResultConsumer
    //
    async consume(confirmed: boolean): Promise<boolean> {
        let success = false
        switch (confirmed) {
            case true:
                if (this.state == CompositeIntentState.Resolved) {
                    // this is fine .... I will book
                    success = await this.persistenceHandler.addToGame(this.availableGames[0].id, this.player.position)
                    if (success) {
                        this.state = CompositeIntentState.Confirmed
                    }
                } else if (this.state == CompositeIntentState.Refused || this.state == CompositeIntentState.Conflicting) {
                    // switch to New Game Intent
                    this.state = CompositeIntentState.OutOfScope
                    const playerCopy = this.player
                    this.reset()
                    success = this.intentSwitcher.switchToIntent(UserIntentType.StartNewMatch, playerCopy)
                }
            case false: 
                if (this.state == CompositeIntentState.Resolved) {
                    // ok, ask if user wants to start her own game
                    this.state = CompositeIntentState.Refused
                    success = true
                } else if (this.state == CompositeIntentState.Refused || this.state == CompositeIntentState.Conflicting) {
                    // ok, seems I cannot really do something for you right now
                    // might be good to switch to a kind of defaultIntent
                    this.state = CompositeIntentState.OutOfScope
                    this.reset()
                    success = true
                }
                
        }

        return new Promise<boolean>(res => {
            res(success)
        })
    }

    //
    //      Implementing ICompositeIntent
    //
    canHandle(nlpIntent: string): boolean {
        this.currentIntent = null
        this.currentListener = null
        let success: boolean = false
        if (nlpIntent === this.intentName) {
            success = true
            this.state = CompositeIntentState.Inspecting
        } else {
            success = super.canHandle(nlpIntent)
        }
        
        return success
    }

    async handle(parameters: any): Promise<boolean> {
        let success = false
        if (this.state != CompositeIntentState.OutOfScope) {
            if (this.state == CompositeIntentState.Inspecting) {
                this.availableGames = await this.findAvailableGames()

                if (this.availableGames.length == 0) {
                    this.state = CompositeIntentState.Conflicting
                } else {
                    this.state = CompositeIntentState.InScope
                }
                //
                //  Extract additional slot parameters
                //
                const others = this.intents.filter(e => ((e instanceof BaseAtomicSlotIntent) && (e.getState() == AtomicIntentState.Open))) as unknown as IAtomicSlotIntent[]

                for (let intent of others) {
                    intent.extractSlot(parameters)
                }

                success = true
            } else {
                success = await super.handle(parameters)
            }
        }

        return new Promise<boolean>(res => {
            res(success)
        })
    }

    canProceed(type: UserIntentType): boolean {
        let success = false
        if (type == UserIntentType.JoinExistingMatch) {
            success = true
            this.state = CompositeIntentState.Inspecting
        }

        return success
    }

    async prepare(payload: any): Promise<boolean>  {
        const others = this.intents.filter(e => ((e instanceof BaseAtomicSlotIntent) && (e.getState() == AtomicIntentState.Open))) as unknown as IAtomicSlotIntent[]

        for (let intent of others) {
            intent.extractSlot(payload)
        }

        this.availableGames = await this.findAvailableGames()

        if (this.availableGames.length == 0) {
            this.state = CompositeIntentState.Conflicting
        } else {
            this.state = CompositeIntentState.InScope
        }

        return new Promise<boolean>(res => {
            res(true)
        })
    }

    nextInquiry(): AssistantResponse {
        if (this.state == CompositeIntentState.OutOfScope) {
            return new AssistantResponse('')
        } else if (this.state == CompositeIntentState.Confirmed) {
            this.state = CompositeIntentState.OutOfScope
            this.reset()
            const message = 'Super. Ich habe dich für ein Spiel eingetragen. Ich gebe dir Bescheid, wenn es beginnt.'
            return new AssistantResponse(message, AssistantResponseType.Plain, false)
        } else if (this.state == CompositeIntentState.Refused) {
            //this.state = CompositeIntentState.OutOfScope
            this.assignListener(ListenerType.UserConfirmation, this)
            const message = 'Ok.'
            const nextInquiry = new AssistantResponse(message, AssistantResponseType.Confirmation)
            nextInquiry.options = ['Möchtest Du stattdessen lieber ein neues Spiel starten?']
            return nextInquiry
        }

        if (this.state == CompositeIntentState.Resolved) {
            let confirmation = null
            if (this.currentIntent) {
                confirmation = this.currentIntent.confirmation()
            }
            let message = this.gameCountStatement()
            //console.log('HERE HERE HERE', message)
            this.assignListener(ListenerType.UserConfirmation, this)
            const nextInquiry = new AssistantResponse(message, AssistantResponseType.Confirmation)
            if (confirmation) {
                nextInquiry.prepend(confirmation)
            }
            nextInquiry.options = ['Soll ich dich in eines der Spiele eintragen?']
            return nextInquiry
        } else if (this.currentIntent) {
            let confirmation = this.currentIntent.confirmation()

            let nextIntent = this.intents.find(e => e.getState() == AtomicIntentState.Open)
            if (nextIntent) {
                const nextInquiry = nextIntent.inquiry()
                nextInquiry.prepend(confirmation)
                return  nextInquiry
            } else {
                return new AssistantResponse(confirmation)
            }
        } else {
            if (this.state == CompositeIntentState.Conflicting) {
                this.assignListener(ListenerType.UserConfirmation, this)
                const nextInquiry = new AssistantResponse('Leider sind im Moment keine Spiele verfügbar!', AssistantResponseType.Confirmation)
                nextInquiry.options = ['Willst Du ein neues Spiel anlegen?']
                return nextInquiry
            } else if (this.state == CompositeIntentState.InScope) {

                const confirmMessage = this.gameCountStatement()
                const open = this.intents.find(e => e.getState() == AtomicIntentState.Open)
                
                if (open) {
                    const nextInquiry = open.inquiry()
                    nextInquiry.prepend(confirmMessage)
                    //nextInquiry.personalizePhrase(this.nameProvider.playerName(), 45)
                    return nextInquiry
                } else {
                    return new AssistantResponse(confirmMessage, AssistantResponseType.Plain, false)
                }
                
            }
        }
        
    }

    //
    //      Internal Operations
    //
    async findAvailableGames(): Promise<Game[]> {
        return this.persistenceHandler.availableGamesForUser()
    }

    gameCountStatement(): string {
        let message: string 
        if (this.availableGames.length == 0) {
            message = 'Ich konnte leider keine Spiele für dich finden.'
        } else if (this.availableGames.length == 1) {
            message = 'Ich habe ein Spiel gefunden.'
        } else {
            message = `Ich habe ${ this.availableGames.length } Spiele gefunden.`
        }
        return message
    }

    reset() {
        super.reset()
        this.player = new Player()
    }
}