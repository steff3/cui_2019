import { ICompositeIntent, CompositeIntentState } from './ICompositeIntent'
import { IAtomicIntent, AtomicIntentState } from '../AtomicIntents/IAtomicIntent'
import { IAtomicSlotIntent } from '../AtomicIntents/IAtomicSlotIntent'
import { BaseAtomicSlotIntent } from '../AtomicIntents/BaseAtomicSlotIntent'
import { IListenerIntent, ListenerType } from '../ListenerIntents/BaseIntentListener'
import { ListenerCapableIntent } from '../ListenerIntents/ListenerCapableIntent'
import { UserIntentType, IntentSwitcher } from '../../Context/ConversationResolver'


export class BaseCompositeIntent extends ListenerCapableIntent {
    protected readonly intentName: string
    protected readonly intentType: UserIntentType

    protected intents: IAtomicIntent[]

    protected currentIntent: IAtomicIntent
    protected currentListener: IListenerIntent
    protected intentSwitcher: IntentSwitcher
    protected state: CompositeIntentState

    protected isPreConfirmed: boolean

    constructor(name: string, type: UserIntentType, switcher: IntentSwitcher) {
        super()

        this.intentSwitcher = switcher
        this.intentName = name
        this.intentType = type

        this.isPreConfirmed = false
        this.state = CompositeIntentState.OutOfScope

        this.intents = []
    }



    protected reset() {
        const others = this.intents.filter(e => (e instanceof BaseAtomicSlotIntent)) as unknown as IAtomicSlotIntent[]

        for (let intent of others) {
            intent.reset()
        }

        this.state = CompositeIntentState.OutOfScope
        this.isPreConfirmed = false
    }


    //
    //      Implementing IIntent
    //
    canHandle(nlpIntent: string): boolean {
        this.currentIntent = null
        this.currentListener = null
        let success: boolean = false
        if (nlpIntent === this.intentName) {
            success = true
            this.state = CompositeIntentState.InScope
        } else {
            for (let listener of this.listeners) {
                if (listener.canHandle(nlpIntent)) {
                    this.currentListener = listener
                    break
                }
            }
            if (this.currentListener == null) {
                for (let intent of this.intents) {
                    if (intent.canHandle(nlpIntent)) {
                        this.currentIntent = intent
                        break
                    }
                }
            }
            

            if (this.currentListener || this.currentIntent) {
                if (this.currentIntent) {
                    this.state = CompositeIntentState.InScope
                }
                success = true
            }
        }
        
        return success
    }

    async handle(parameters: any): Promise<boolean> {
        let success = false
        if (this.state != CompositeIntentState.OutOfScope) {
            if (this.currentListener) {
                success = await this.currentListener.handle(parameters)
            }

            if (this.currentIntent) {
                success = await this.currentIntent.handle(parameters)
            }

            //
            //  Extract additional slot parameters
            //
            const others = this.intents.filter(e => ((e instanceof BaseAtomicSlotIntent) && (e.getState() == AtomicIntentState.Open))) as unknown as IAtomicSlotIntent[]

            for (let intent of others) {
                intent.extractSlot(parameters)
            }
        }

        return new Promise<boolean>(res => {
            res(success)
        })
    }

    //
    //      Implementing ICompositeIntent
    //
    canProceed(type: UserIntentType): boolean {
        let success = false
        if (type == this.intentType) {
            success = true
            this.state = CompositeIntentState.InScope
            this.isPreConfirmed = true
        }

        return success
    }

    prepare(payload: any): Promise<boolean>  {
        const others = this.intents.filter(e => ((e instanceof BaseAtomicSlotIntent) && (e.getState() == AtomicIntentState.Open))) as unknown as IAtomicSlotIntent[]

        for (let intent of others) {
            intent.extractSlot(payload)
        }

        return new Promise<boolean>(res => {
            res(true)
        })
    }

    getState(): CompositeIntentState {
        if (this.state == CompositeIntentState.OutOfScope 
            || this.state == CompositeIntentState.Conflicting 
            || this.state == CompositeIntentState.Confirmed
            || this.state == CompositeIntentState.Refused) {
            return this.state
        } else {
            const conflicting = this.intents.find(e => e.getState() == AtomicIntentState.Conflicting)

            if (conflicting) {
                return CompositeIntentState.Conflicting
            }

            const open = this.intents.find(e => e.getState() == AtomicIntentState.Open)
            if (open) {
                return CompositeIntentState.InScope
            }

            this.state = CompositeIntentState.Resolved
            return this.state
        }
    }
}