export interface IIntent {
    canHandle(nlpIntent: string): boolean
    handle(parameters: any): Promise<boolean>
}