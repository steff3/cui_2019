import { IIntent } from '../IIntent'
import { AssistantResponse } from '../../Context/AssistantResponse'

export enum AtomicIntentState {
    Open,
    Conflicting,
    AwaitingConfirmation,
    Resolved
}

export interface IAtomicIntent extends IIntent {
    readonly index: number
    readonly keyword: string

    inquiry(): AssistantResponse
    getState(): AtomicIntentState
    confirmation(): string
}
