import { IAtomicIntent, AtomicIntentState } from './IAtomicIntent'
import { BaseAtomicSlotIntent } from './BaseAtomicSlotIntent'
import { PositionType } from '../../Models/player'
import { ListenerAssigner } from '../ListenerIntents/ListenerCapableIntent'
import { AssistantResponse, AssistantResponseType } from '../../Context/AssistantResponse'

export class PositionIntent extends BaseAtomicSlotIntent implements IAtomicIntent {
    readonly index: number
    readonly inquiryMessage: string
    readonly keyword: string

    private position: PositionType
    private listenerAssigner: ListenerAssigner
    private action: () => Promise<boolean>

    constructor(index: number, action: () => Promise<boolean>, listenerAssigner: ListenerAssigner, target: any, key: string, inquiry?: string) {
        super(key, target)
        this.index = index
        this.listenerAssigner = listenerAssigner
        this.action = action

        if (inquiry) {
            this.inquiryMessage = inquiry
        } else {
            this.inquiryMessage = 'Auf welcher Position möchtest du gerne spielen?'
        }
        
        this.keyword = 'Position'
    }

    descriptionForPositionType(): string {
        switch (this.assignmentTarget[this.keyPath]) { // this.position
            case PositionType.Attack:
                return 'im Sturm'
            case PositionType.Defence:
                return 'in der Abwehr'
        }
    }

    //
    //      INTERFACE IINTENT
    //
    canHandle(nlpIntent: string): boolean {
        if (nlpIntent === 'PositionIntent') {
            return true
        }
        return false
    }

    async handle(parameters: any): Promise<boolean> {
        let success = false

        if (parameters.position) {
            if (parameters.position === 'Attack') {
                this.position = PositionType.Attack
            } else if (parameters.position === 'Defense') {
                this.position = PositionType.Defence
            }
        }
        
        if (this.position != undefined) {
            this.assignmentTarget[this.keyPath] = this.position
            success = true
            this.state = AtomicIntentState.Resolved
            if (this.action) {
                success = await this.action()
            }
        }

        return new Promise<boolean>(res => {
            res(success)
        })
    }

    getState(): AtomicIntentState {
        return this.state
    }

    //
    //      INTERFACE IATOMICSLOTINTENT
    //
    async extractSlot(parameters: any): Promise<void> {
        const positionValue = parameters.position

        if (positionValue != undefined) {
            if (typeof positionValue === 'string') {
                if (positionValue === 'Attack') {
                    this.position = PositionType.Attack
                } else if (positionValue === 'Defense') {
                    this.position = PositionType.Defence
                }
            } else {
                this.position = positionValue
            }
        }

        if (this.position != undefined) {
            this.assignmentTarget[this.keyPath] = this.position
            this.state = AtomicIntentState.Resolved
            if (this.action) {
                await this.action()
            }
        }

        return new Promise<void>(res => {
            res()
        })
    }

    reset() {
        this.position = undefined
        super.reset()
    }

    //
    //      INTERFACE IATOMICINTENT
    //
    confirmation(): string {
        return `Du spielst ${ this.descriptionForPositionType() }.`
    }

    inquiry(): AssistantResponse {
        const generatedInquiry = new AssistantResponse(this.inquiryMessage, AssistantResponseType.Suggestions)
        generatedInquiry.options = ['Sturm', 'Abwehr']

        return generatedInquiry
    }
}