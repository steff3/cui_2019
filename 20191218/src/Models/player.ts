
export enum PositionType {
    Attack,
    Defence,
}

export enum TeamType {
    Blue,
    Red,
}

export class PlayingPosition {
    position: PositionType
    team: TeamType

    constructor(position: PositionType, team: TeamType) {
        this.position = position
        this.team = team
    }
}

export class Player {
    id: number
    name: string
    position: PositionType
    team?: TeamType
    userId: number
}