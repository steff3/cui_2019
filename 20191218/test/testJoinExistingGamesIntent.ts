import { describe } from 'mocha'
import * as chai from 'chai'
import * as chaiAsPromised from 'chai-as-promised'
import * as uuidv4 from 'uuid/v4'
const { expect } = chai

chai.use(chaiAsPromised)

import { GameResult } from '../src/Models/game'
import { TeamType } from '../src/Models/player'
import { SearchGameIntent } from '../src/Intents/CompositeIntents/SearchGamesIntent'
import { KickerContext, MockConfigState } from '../src/Context/KickerContext'
import { UserRequest } from '../src/Context/UserRequest'
import { CompositeIntentState } from '../src/Intents/CompositeIntents/ICompositeIntent'
import { AssistantResponseType } from '../src/Context/AssistantResponse'

let compositeIntent: SearchGameIntent
let context: KickerContext

describe ('test Join Existing Game Intent', function() {
    before('Create Context', async function() {
        // context = new KickerContext(true)
        // await context.setup()
        // await context.getUserForActionId(null)
        // compositeIntent = new SearchGameIntent(context, context, null)
    })

    this.beforeEach('Create Context Before Each', async function() {
        context = new KickerContext(true)
        await context.setup()
        await context.getUserForActionId(null)
        compositeIntent = new SearchGameIntent(context, context, null)
    })

    it ('test canHandle GetGamesIntent', async function() {
        const request = new UserRequest('GetMatchesIntent', {}, 'Gibt es verfügbare Spiele?')

        const success = compositeIntent.canHandle(request.intent)

        expect(success).to.be.true
    })

    it ('test handle GetGamesIntent', async function() {
        const request = new UserRequest('GetMatchesIntent', {}, 'Gibt es verfügbare Spiele?')

        compositeIntent.canHandle(request.intent)

        const success = await compositeIntent.handle(request.parameters)
        const inquiry = compositeIntent.nextInquiry()

        expect(success).to.be.true
        expect(inquiry).to.be.an('object')
        expect(inquiry.phrase).to.be.a('string')
        expect(inquiry.type).to.be.equal(AssistantResponseType.Suggestions)
        expect(inquiry.phrase).to.be.equal('Ich habe 3 Spiele gefunden. Auf welcher Position möchtest du gerne spielen?')
        expect(inquiry.options).to.be.eql(['Sturm', 'Abwehr'])
    })

    it ('test handle GetGamesIntent Extracting Position', async function() {
        const request = new UserRequest('GetMatchesIntent', {position: 'Attack'}, 'Sturm?')
        
        compositeIntent.canHandle(request.intent)

        const success = await compositeIntent.handle(request.parameters)
        const inquiry = compositeIntent.nextInquiry()
        

        expect(success).to.be.true
        expect(inquiry).to.be.an('object')
        expect(inquiry.phrase).to.be.a('string')
        expect(inquiry.phrase).to.be.equal('Ich habe 2 Spiele gefunden.')
    })

    it ('test handle GetGamesIntent 2 Requests', async function() {
        let request = new UserRequest('GetMatchesIntent', {}, 'Gibt es verfügbare Spiele?')

        compositeIntent.canHandle(request.intent)

        await compositeIntent.handle(request.parameters)
        
        request = new UserRequest('PositionIntent', {position: 'Attack'}, 'Sturm')

        let success = compositeIntent.canHandle(request.intent)
        expect(success).to.be.true

        success = await compositeIntent.handle(request.parameters)
        const inquiry = compositeIntent.nextInquiry()

        expect(success).to.be.true
        expect(inquiry).to.be.an('object')
        expect(inquiry.phrase).to.be.a('string')
        expect(inquiry.phrase).to.be.equal('Du spielst im Sturm.')
    })
})

describe ('test Join Existing Game Intent NO Games', function() {
    this.beforeEach('Create Context Before Each', async function() {
        context = new KickerContext(true)
        await context.setup(MockConfigState.NoGames)
        await context.getUserForActionId(null)
        compositeIntent = new SearchGameIntent(context, context, null)
    })

    it ('test canHandle GetGamesIntent', async function() {
        const request = new UserRequest('GetMatchesIntent', {}, 'Gibt es verfügbare Spiele?')

        const success = compositeIntent.canHandle(request.intent)

        expect(success).to.be.true
    })

    it ('test handle GetGamesIntent', async function() {
        const request = new UserRequest('GetMatchesIntent', {}, 'Gibt es verfügbare Spiele?')

        compositeIntent.canHandle(request.intent)

        const success = await compositeIntent.handle(request.parameters)
        const state = compositeIntent.getState()
        const inquiry = compositeIntent.nextInquiry()

        expect(success).to.be.true
        expect(state).to.be.equal(CompositeIntentState.Conflicting)
        expect(inquiry).to.be.an('object')
        expect(inquiry.phrase).to.be.a('string')
        expect(inquiry.type).to.be.equal(AssistantResponseType.Confirmation)
        expect(inquiry.phrase).to.be.equal('Leider sind im Moment keine Spiele verfügbar!')
        expect(inquiry.options).to.be.eql(['Willst Du ein neues Spiel anlegen?'])
    })
})
