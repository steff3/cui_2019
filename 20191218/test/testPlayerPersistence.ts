import { describe } from 'mocha'
import * as chai from 'chai'
import * as chaiAsPromised from 'chai-as-promised'
import * as uuidv4 from 'uuid/v4'
const { expect } = chai
chai.use(chaiAsPromised)


import { PlayerPersistenceHandler } from '../src/Persistence/PlayerPersistenceHandler'
import { UserPersistenceHandler } from '../src/Persistence/UserPersistenceHandler'
import { PositionType } from '../src/Models/player'
import { User } from '../src/Models/user'

let handler: PlayerPersistenceHandler
let userHandler: UserPersistenceHandler
let users: User[] = []
let playerIds: number[] = []
let actionId: string

describe.skip ('Player Tests In Database', function() {
    before('Create User', async function() {
        handler = new PlayerPersistenceHandler()
        userHandler = new UserPersistenceHandler()
        console.log('---- BEFORE SCENARIOS: START CREATE USERS ----')
        await createUser('Mario', 'Maier', 'SuperMario')
        await createUser('Dieter', 'Bauer', 'Didi')
        await createUser('Sara', 'Neuhaus', 'Sasa')
        await createUser('Lena', 'Lukatsch', 'Leni')
        await createUser('Bubu', 'Loreny', 'Bubu')
        console.log('---- BEFORE SCENARIOS: END CREATE USERS ----')
    })

    after('Delete User', async function() {
        console.log('---- AFTER SCENARIOS: START DELETE USERS ----')
        for (let user of users) {
            const success = await userHandler.deleteExistingUser(user.id)
            console.log(`user ${ user.playerName } successfully deleted? ${success}`)
        }
        console.log('---- AFTER SCENARIOS: END DELETE USERS ----')
    })

    it ('Create New Player', async function() {
        playerIds = []
        const playerId = await handler.createNewPlayer(users[0], PositionType.Attack)

        expect(playerId).to.be.a('number').that.is.greaterThan(0)
        playerIds.push(playerId)
    })

    it ('Retrieve Player By Id', async function() {
        playerIds = []
        const playerId = await handler.createNewPlayer(users[0], PositionType.Attack)
        playerIds.push(playerId)

        const player = await handler.playerForId(playerId)
        expect(player).to.have.property('name', users[0].playerName)
        expect(player).to.have.property('position', PositionType.Attack)
        expect(player).to.have.property('userId', users[0].id)
        console.dir(player, {depth:null})
    })

    it ('Retrieve Player Of User', async function() {
        playerIds = []
        const playerId = await handler.createNewPlayer(users[0], PositionType.Attack)
        playerIds.push(playerId)

        const players = await handler.playersOfUser(users[0])
        expect(players.length).to.be.equal(1)
        expect(players[0]).to.have.property('name', users[0].playerName)
        expect(players[0]).to.have.property('position', PositionType.Attack)
        expect(players[0]).to.have.property('userId', users[0].id)
        console.dir(players[0], {depth:null})
    })

    it ('Retrieve Players Of User', async function() {
        playerIds = []
        let playerId = await handler.createNewPlayer(users[0], PositionType.Attack)
        playerIds.push(playerId)
        playerId = await handler.createNewPlayer(users[0], PositionType.Defence)
        playerIds.push(playerId)
        playerId = await handler.createNewPlayer(users[1], PositionType.Defence)
        playerIds.push(playerId)
        playerId = await handler.createNewPlayer(users[4], PositionType.Defence)
        playerIds.push(playerId)

        const players = await handler.playersOfUser(users[0])
        expect(players.length).to.be.equal(2)
        expect(players[0]).to.have.property('name', users[0].playerName)
        expect(players[0]).to.have.property('position', PositionType.Attack)
        expect(players[0]).to.have.property('userId', users[0].id)
        expect(players[1]).to.have.property('name', users[0].playerName)
        expect(players[1]).to.have.property('position', PositionType.Defence)
        expect(players[1]).to.have.property('userId', users[0].id)
        console.dir(players, {depth:null})
    })

    it ('Retrieve NO Player Of User(2)', async function() {
        playerIds = []
        let playerId = await handler.createNewPlayer(users[0], PositionType.Attack)
        playerIds.push(playerId)
        playerId = await handler.createNewPlayer(users[0], PositionType.Defence)
        playerIds.push(playerId)
        playerId = await handler.createNewPlayer(users[1], PositionType.Defence)
        playerIds.push(playerId)
        playerId = await handler.createNewPlayer(users[4], PositionType.Defence)
        playerIds.push(playerId)

        const players = await handler.playersOfUser(users[2])
        expect(players).to.be.an('array')
        expect(players).to.be.empty
    })

    it ('Retrieve Positions Of User(0)', async function() {
        playerIds = []
        let playerId = await handler.createNewPlayer(users[0], PositionType.Attack)
        playerIds.push(playerId)
        playerId = await handler.createNewPlayer(users[0], PositionType.Defence)
        playerIds.push(playerId)
        playerId = await handler.createNewPlayer(users[0], PositionType.Attack)
        playerIds.push(playerId)
        playerId = await handler.createNewPlayer(users[0], PositionType.Attack)
        playerIds.push(playerId)
        playerId = await handler.createNewPlayer(users[0], PositionType.Defence)
        playerIds.push(playerId)
        playerId = await handler.createNewPlayer(users[1], PositionType.Defence)
        playerIds.push(playerId)
        playerId = await handler.createNewPlayer(users[4], PositionType.Defence)
        playerIds.push(playerId)

        const positions = await handler.positionsOfUser(users[0])
        expect(positions).to.be.an('array')
        expect(positions.length).to.be.equal(5)
        expect(positions.filter(element => element == PositionType.Attack).length).to.be.equal(3)
        expect(positions.filter(element => element == PositionType.Defence).length).to.be.equal(2)
    })

    it ('Retrieve Positions Of User(1)', async function() {
        playerIds = []
        let playerId = await handler.createNewPlayer(users[0], PositionType.Attack)
        playerIds.push(playerId)
        playerId = await handler.createNewPlayer(users[0], PositionType.Defence)
        playerIds.push(playerId)
        playerId = await handler.createNewPlayer(users[0], PositionType.Attack)
        playerIds.push(playerId)
        playerId = await handler.createNewPlayer(users[0], PositionType.Attack)
        playerIds.push(playerId)
        playerId = await handler.createNewPlayer(users[0], PositionType.Defence)
        playerIds.push(playerId)
        playerId = await handler.createNewPlayer(users[1], PositionType.Defence)
        playerIds.push(playerId)
        playerId = await handler.createNewPlayer(users[4], PositionType.Defence)
        playerIds.push(playerId)

        const positions = await handler.positionsOfUser(users[1])
        expect(positions).to.be.an('array')
        expect(positions.length).to.be.equal(1)
        expect(positions.filter(element => element == PositionType.Attack)).to.be.empty
        expect(positions.filter(element => element == PositionType.Defence).length).to.be.equal(1)
    })

    it ('Retrieve NO Positions Of User(3)', async function() {
        playerIds = []
        let playerId = await handler.createNewPlayer(users[0], PositionType.Attack)
        playerIds.push(playerId)
        playerId = await handler.createNewPlayer(users[0], PositionType.Defence)
        playerIds.push(playerId)
        playerId = await handler.createNewPlayer(users[0], PositionType.Attack)
        playerIds.push(playerId)
        playerId = await handler.createNewPlayer(users[0], PositionType.Attack)
        playerIds.push(playerId)
        playerId = await handler.createNewPlayer(users[0], PositionType.Defence)
        playerIds.push(playerId)
        playerId = await handler.createNewPlayer(users[1], PositionType.Defence)
        playerIds.push(playerId)
        playerId = await handler.createNewPlayer(users[4], PositionType.Defence)
        playerIds.push(playerId)

        const positions = await handler.positionsOfUser(users[3])
        expect(positions).to.be.an('array')
        expect(positions).to.be.empty
    })



   afterEach('Delete Current Player', async function() {
       for (let id of playerIds) {
            await cleanPlayer(id)
       }
   })
} )

async function cleanPlayer(id: number) {
    const success = await handler.deleteExistingPlayer(id)
    console.log(`player with id ${id} successfully deleted? ${success}`)
}

async function createUser(firstName: string, lastName: string, playerName: string) {
    actionId = uuidv4()
    const userId = await userHandler.createNewUser(firstName, lastName, playerName, actionId)
    const user = await userHandler.retrieveUser(userId)
    console.dir(user, {depth:null})
    users.push(user)
}