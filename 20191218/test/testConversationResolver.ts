import { describe } from 'mocha'
import * as chai from 'chai'
import * as chaiAsPromised from 'chai-as-promised'
import * as uuidv4 from 'uuid/v4'
const { expect } = chai

chai.use(chaiAsPromised)

import { ConversationResolver } from '../src/Context/ConversationResolver'
import { KickerContext, MockConfigState } from '../src/Context/KickerContext'
import { UserRequest } from '../src/Context/UserRequest'
import { AssistantResponseType } from '../src/Context/AssistantResponse'

let resolver: ConversationResolver
let context: KickerContext

describe ('test Conversation Resolver', function() {
    before('Create Context', async function() {
        
    })

    this.beforeEach('Create Search Game Intent', async function() {
        context = new KickerContext(true)
        await context.setup()
        await context.getUserForActionId(null)
        resolver = new ConversationResolver(context, context)
    })

    it ('test handleConversationalTurn Available Games', async function() {
        const request = new UserRequest('GetMatchesIntent', {}, 'Gibt es verfügbare Spiele?')

        const answer = await resolver.handleConversationalTurn(request)

        expect(answer).to.be.an('object')
        expect(answer.isInquiry).to.be.true
        expect(answer.type).to.be.equal(AssistantResponseType.Suggestions)
        expect(answer.options).to.be.eql(['Sturm', 'Abwehr'])
        expect(answer.phrase).to.be.a('string')
        expect(answer.phrase).to.be.equal('Ich habe 3 Spiele gefunden. Auf welcher Position möchtest du gerne spielen?')
    })

    it ('test handleConversationalTurn Choose Position', async function() {
        let request = new UserRequest('GetMatchesIntent', {}, 'Gibt es verfügbare Spiele?')

        let answer = await resolver.handleConversationalTurn(request)
        
        request = new UserRequest('PositionIntent', {position: 'Attack'}, 'Sturm')

        answer = await resolver.handleConversationalTurn(request)

        expect(answer).to.be.an('object')
        expect(answer.isInquiry).to.be.true
        expect(answer.type).to.be.equal(AssistantResponseType.Confirmation)
        expect(answer.phrase).to.be.a('string')
        expect(answer.phrase).to.be.equal('Du spielst im Sturm. Ich habe 2 Spiele gefunden.')
        expect(answer.options).to.be.eql(['Soll ich dich in eines der Spiele eintragen?'])
    })

    it ('test handleConversationalTurn Add To Game', async function() {
        const gamesBefore = await context.pendingGamesOfUser()
        let request = new UserRequest('GetMatchesIntent', {}, 'Gibt es verfügbare Spiele?')

        let answer = await resolver.handleConversationalTurn(request)
        
        request = new UserRequest('PositionIntent', {position: 'Attack'}, 'Sturm')

        answer = await resolver.handleConversationalTurn(request)

        request = new UserRequest('UserConfirmationIntent', {granted: true}, 'Ja')

        answer = await resolver.handleConversationalTurn(request)

        const games = await context.pendingGamesOfUser()
        
        expect(answer).to.be.an('object')
        expect(answer.isInquiry).to.be.true
        expect(answer.type).to.be.equal(AssistantResponseType.Plain)
        expect(answer.phrase).to.be.a('string')
        expect(answer.phrase).to.be.equal('Super. Ich habe dich für ein Spiel eingetragen. Ich gebe dir Bescheid, wenn es beginnt. Kann ich noch etwas für dich tun?')
        expect(gamesBefore.length).to.be.equal(0)
        expect(games.length).to.be.equal(1)
    })

    it ('test handleConversationalTurn Refused Add To Game', async function() {
        const gamesBefore = await context.pendingGamesOfUser()
        let request = new UserRequest('GetMatchesIntent', {}, 'Gibt es verfügbare Spiele?')

        let answer = await resolver.handleConversationalTurn(request)
        
        request = new UserRequest('PositionIntent', {position: 'Attack'}, 'Sturm')

        answer = await resolver.handleConversationalTurn(request)

        request = new UserRequest('UserConfirmationIntent', {granted: false}, 'Nein')

        answer = await resolver.handleConversationalTurn(request)

        const games = await context.pendingGamesOfUser()
        
        expect(answer).to.be.an('object')
        expect(answer.isInquiry).to.be.true
        expect(answer.type).to.be.equal(AssistantResponseType.Confirmation)
        expect(answer.phrase).to.be.a('string')
        expect(answer.phrase).to.be.equal('Ok.')
        expect(answer.options).to.be.eql(['Möchtest Du stattdessen lieber ein neues Spiel starten?'])
        expect(gamesBefore.length).to.be.equal(0)
        expect(games.length).to.be.equal(0)
    })

    it ('test handleConversationalTurn Switch to StartNewMatchIntent', async function() {
        const gamesBefore = await context.pendingGamesOfUser()
        let request = new UserRequest('GetMatchesIntent', {}, 'Gibt es verfügbare Spiele?')

        let answer = await resolver.handleConversationalTurn(request)
        
        request = new UserRequest('PositionIntent', {position: 'Attack'}, 'Sturm')

        answer = await resolver.handleConversationalTurn(request)

        request = new UserRequest('UserConfirmationIntent', {granted: false}, 'Nein')

        answer = await resolver.handleConversationalTurn(request)

        request = new UserRequest('UserConfirmationIntent', {granted: true}, 'Ja')

        answer = await resolver.handleConversationalTurn(request)

        const games = await context.pendingGamesOfUser()
        
        expect(answer).to.be.an('object')
        expect(answer.isInquiry).to.be.true
        expect(answer.type).to.be.equal(AssistantResponseType.Plain)
        expect(answer.phrase).to.be.a('string')
        expect(answer.phrase).to.be.equal('Ich habe ein neues Spiel für dich anlegt. Ich gebe dir Bescheid, wenn es beginnt. Kann ich noch etwas für dich tun?')
        expect(gamesBefore.length).to.be.equal(0)
        expect(games.length).to.be.equal(1)
    })

    // it ('test handleConversationalTurn Refused Add To Game', async function() {
    //     const gamesBefore = await context.availableGamesOfUser()
    //     let request = new UserRequest('GetMatchesIntent', {}, 'Gibt es verfügbare Spiele?')

    //     let answer = await resolver.handleConversationalTurn(request)
        
    //     request = new UserRequest('PositionIntent', {position: 'Attack'}, 'Sturm')

    //     answer = await resolver.handleConversationalTurn(request)

    //     request = new UserRequest('UserConfirmationIntent', {granted: false}, 'Nein')

    //     answer = await resolver.handleConversationalTurn(request)

    //     request = new UserRequest('UserConfirmationIntent', {granted: false}, 'Nein')

    //     answer = await resolver.handleConversationalTurn(request)

    //     const games = await context.availableGamesOfUser()
        
    //     expect(answer).to.be.an('object')
    //     expect(answer.isInquiry).to.be.true
    //     expect(answer.type).to.be.equal(AssistantResponseType.Plain)
    //     expect(answer.phrase).to.be.a('string')
    //     expect(answer.phrase).to.be.equal('Ok.')
    //     expect(answer.options).to.be.eql(['Möchtest Du stattdessen lieber ein neues Spiel starten?'])
    //     expect(gamesBefore.length).to.be.equal(0)
    //     expect(games.length).to.be.equal(0)
    // })
})

describe ('test Conversation Resolver NO Games', function() {
    before('Create Context', async function() {
        
    })

    this.beforeEach('Create Search Game Intent', async function() {
        context = new KickerContext(true)
        await context.setup(MockConfigState.NoGames)
        await context.getUserForActionId(null)
        resolver = new ConversationResolver(context, context)
    })

    it ('test handleConversationalTurn Available Games', async function() {
        const request = new UserRequest('GetMatchesIntent', {}, 'Gibt es verfügbare Spiele?')

        const answer = await resolver.handleConversationalTurn(request)

        expect(answer).to.be.an('object')
        expect(answer.isInquiry).to.be.true
        expect(answer.type).to.be.equal(AssistantResponseType.Confirmation)
        expect(answer.phrase).to.be.a('string')
        expect(answer.phrase).to.be.equal('Leider sind im Moment keine Spiele verfügbar!')
        expect(answer.options).to.be.eql(['Willst Du ein neues Spiel anlegen?'])
    })

    it ('test handleConversationalTurn Switch To CreateNewGame', async function() {
        let request = new UserRequest('GetMatchesIntent', {}, 'Gibt es verfügbare Spiele?')

        let answer = await resolver.handleConversationalTurn(request)

        request = new UserRequest('UserConfirmationIntent', {granted: true}, 'Ja')

        answer = await resolver.handleConversationalTurn(request)

        expect(answer).to.be.an('object')
        expect(answer.isInquiry).to.be.true
        expect(answer.type).to.be.equal(AssistantResponseType.Suggestions)
        expect(answer.phrase).to.be.a('string')
        expect(answer.options).to.be.eql(['Sturm', 'Abwehr'])
        expect(answer.phrase).to.be.equal('Auf welcher Position möchtest du gerne spielen?')
        //console.dir(answer, [{depth:null}])
    })

    it ('test handleConversationalTurn Switch To CreateNewGame and Enter Position', async function() {
        const gamesBefore = await context.pendingGamesOfUser()
        let request = new UserRequest('GetMatchesIntent', {}, 'Gibt es verfügbare Spiele?')

        let answer = await resolver.handleConversationalTurn(request)

        request = new UserRequest('UserConfirmationIntent', {granted: true}, 'Ja')

        answer = await resolver.handleConversationalTurn(request)

        request = new UserRequest('PositionIntent', {position: 'Defense'}, 'Abwehr')

        answer = await resolver.handleConversationalTurn(request)

        const games = await context.pendingGamesOfUser()

        expect(answer).to.be.an('object')
        expect(answer.isInquiry).to.be.true
        expect(answer.type).to.be.equal(AssistantResponseType.Plain)
        expect(answer.phrase).to.be.a('string')
        expect(answer.phrase).to.be.equal('Ich habe ein neues Spiel für dich anlegt. Ich gebe dir Bescheid, wenn es beginnt. Kann ich noch etwas für dich tun?')
        expect(gamesBefore.length).to.be.equal(0)
        expect(games.length).to.be.equal(1)
        //console.dir(answer, [{depth:null}])
    })

})

describe.only ('test Conversation Resolver Existing Games', function() {
    before('Create Context', async function() {
        
    })

    this.beforeEach('Create Search Game Intent', async function() {
        context = new KickerContext(true)
        await context.setup(MockConfigState.ExistingUserGames)
        await context.getUserForActionId(null)
        resolver = new ConversationResolver(context, context)
    })

    it ('test handleConversationalTurn Available Games', async function() {
        const request = new UserRequest('GetMatchesIntent', {}, 'Gibt es verfügbare Spiele?')

        const answer = await resolver.handleConversationalTurn(request)

        expect(answer).to.be.an('object')
        expect(answer.isInquiry).to.be.true
        expect(answer.type).to.be.equal(AssistantResponseType.Confirmation)
        expect(answer.phrase).to.be.a('string')
        expect(answer.phrase).to.be.equal('Leider sind im Moment keine Spiele verfügbar!')
        expect(answer.options).to.be.eql(['Willst Du ein neues Spiel anlegen?'])
    })

    it ('test handleConversationalTurn Switch To CreateNewGame', async function() {
        let request = new UserRequest('GetMatchesIntent', {}, 'Gibt es verfügbare Spiele?')

        let answer = await resolver.handleConversationalTurn(request)

        request = new UserRequest('UserConfirmationIntent', {granted: true}, 'Ja')

        answer = await resolver.handleConversationalTurn(request)

        expect(answer).to.be.an('object')
        expect(answer.isInquiry).to.be.true
        expect(answer.type).to.be.equal(AssistantResponseType.Suggestions)
        expect(answer.phrase).to.be.a('string')
        expect(answer.options).to.be.eql(['Sturm', 'Abwehr'])
        expect(answer.phrase).to.be.equal('Auf welcher Position möchtest du gerne spielen?')
        //console.dir(answer, [{depth:null}])
    })

    it ('test handleConversationalTurn Switch To CreateNewGame and Enter Position', async function() {
        const gamesBefore = await context.pendingGamesOfUser()
        const pastGames = await context.pastGamesOfUser()

        let request = new UserRequest('GetMatchesIntent', {}, 'Gibt es verfügbare Spiele?')

        let answer = await resolver.handleConversationalTurn(request)

        request = new UserRequest('UserConfirmationIntent', {granted: true}, 'Ja')

        answer = await resolver.handleConversationalTurn(request)

        request = new UserRequest('PositionIntent', {position: 'Defense'}, 'Abwehr')

        answer = await resolver.handleConversationalTurn(request)

        const games = await context.pendingGamesOfUser()

        expect(answer).to.be.an('object')
        expect(answer.isInquiry).to.be.true
        expect(answer.type).to.be.equal(AssistantResponseType.Plain)
        expect(answer.phrase).to.be.a('string')
        expect(answer.phrase).to.be.equal('Ich habe ein neues Spiel für dich anlegt. Ich gebe dir Bescheid, wenn es beginnt. Kann ich noch etwas für dich tun?')
        expect(gamesBefore.length).to.be.equal(0)
        expect(games.length).to.be.equal(1)
        expect(pastGames.length).to.be.equal(6)
        //console.dir(answer, [{depth:null}])
    })

})